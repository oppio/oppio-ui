import { useState } from "react";


import {DateSelector,  DateSelectorProps } from "../../../src/atoms/DateSelector";
import { DateTime } from "luxon";
import { Story } from "@storybook/react";

const story = {
  title: "Atom/Fields/DateSelector",
  component: DateSelector,
};

export default story;


export const Default = {
  render: () => {
    const [date, setDate] = useState<DateTime>(DateTime.fromObject({ year: 2023, month: 11, day: 4, hour: 1, minute: 0, second: 0, millisecond: 0 }))
    return <DateSelector
    rangeSelector={false}
    value={date}
    onChange={(_date) => {
      setDate(_date)
    }}
    endYear={2050}
    />
  },
};


export const TimeSelector = {
  render: () => {
    const [date, setDate] = useState<DateTime>(DateTime.fromObject({ year: 2023, month: 11, day: 4, hour: 1, minute: 0, second: 0, millisecond: 0 }))
    return <DateSelector
      rangeSelector={false}
    />
  },
};

export const RangeSelector = {
  render: () => {
    const [datetimes, setDateTimes] = useState<[DateTime, DateTime]>()
    return <DateSelector
      value={datetimes}
      onChange={setDateTimes}
      rangeSelector
    />
  }
};

