import { Story } from "@storybook/react";
import React from "react";
import {Toast,  ToastProps } from "../../../src/atoms/Toast";

const story = {
  title: "Atom/Toast",
  component: Toast,
};

export default story;

const Template: Story<ToastProps> = (args) => {

  return <Toast {...args}/>
};

export const Success = Template.bind({})

Success.args = {
    //opened: true,
    //position: "center",
    children: <>Hello</>,
    //translucent: false,
};
