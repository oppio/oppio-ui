import { Meta, StoryObj } from "@storybook/react"
import {Skeleton, SkeletonPattern} from "../../../src/atoms/Skeleton"





const meta: Meta<typeof Skeleton> = {
  component: Skeleton,
  title: "Atom/Skeleton"
};

export default meta;

type Story = StoryObj<typeof Skeleton>;

export const Input: Story = {
  render: (args) => {
    return <div className="w-24 h-8"><Skeleton className="bg-primary-500" pattern={SkeletonPattern.Input} /></div>
  },
  args: {
   
  }
  
};

export const Card: Story = {
  render: (args) => {
    return <div className="w-96 h-56"><Skeleton className="bg-primary-500" pattern={SkeletonPattern.Card} /></div>
  },
  args: {
   
  }
  
};


export const TextArea: Story = {
  render: (args) => {
    return <div className="w-24 h-8"><Skeleton  className="bg-primary-500" pattern={SkeletonPattern.TextArea} /></div>
  },
};