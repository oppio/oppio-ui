import { Story } from "@storybook/react";
import React from "react";

import {SwitchField,  SwitchProps } from "../../../src/atoms/Switch";

const story = {
  title: "Atom/Fields/Switch Field",
  component: SwitchField,
};


export default story;

const Template: Story<SwitchProps> = (args: SwitchProps) => {
  const [toggle, setToggle] = React.useState(true);
  return (
    <SwitchField
      {...args}
      value={toggle}
      onChange={() => {setToggle(!toggle)}}
    />
  );
};

export const Default = Template.bind({})

Default.args = {
  
};
