import { Story } from "@storybook/react";
import React from "react";


import {Slider,  SliderProps } from "../../../src/atoms/Slider";

const story = {
  title: "Atom/Slider",
  component: Slider,
};

export default story;

const Template: Story<SliderProps> = (args) => {

  return (
    <Slider {...args}/>
  );
};

export const Default = Template.bind({})

Default.args = {
  max: 100,
  min: 0,
  value: 25,
  step: 10
};
