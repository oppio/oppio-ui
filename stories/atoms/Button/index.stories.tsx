import { Story } from "@storybook/react";
import React from "react";
import {Button, ButtonColor, ButtonProps } from "../../../src/atoms/Button";

const story = {
  title: "Atom/Button",
  component: Button,
};

export default story;

const Template: Story<ButtonProps> = (args) => {
  return <Button color={ButtonColor.SOFT} {...args}/>
};

export const Success = Template.bind({})

Success.args = {
  children: "Button text"
};
