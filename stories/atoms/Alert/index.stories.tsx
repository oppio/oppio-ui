import { StoryObj, Meta } from "@storybook/react";
import { Alert } from "../../../src/atoms/Alert";
import { AlertType } from "../../../src/atoms/Alert/types";


const story: Meta = {
  title: "Atom/Alert",
  component: Alert,
};

export default story;

type Story = StoryObj<typeof Alert>;

const Template: Story = {
  render: (args) => (
    <Alert {...args}/>
  )
};

export const Success = {
  ...Template,
  args: {
    alertType: AlertType.SUCCESS,
    children: "success"
  }
}


export const Error = {
  ...Template,
  args: {
    alertType: AlertType.ERROR,
    children: "error"
  }

}



export const Info = {
  ...Template,
  args: {
    alertType: AlertType.INFO,
    children: "info"
  }

}

export const Warning = {
  ...Template,
  args: {
    alertType: AlertType.WARNING,
    children: "warning"
  }

}

