import { FileField, FileFieldProps, FileFieldSize } from "atoms/FileField";
import { Meta, StoryObj } from "@storybook/react";
import { Controller, useForm } from "react-hook-form";


const story: Meta = {
  title: "Atom/Fields/File Field",
  component: FileField,

};

export default story;

const FileStory = (props: FileFieldProps) => {
    const { control, setValue, setError } = useForm({
        defaultValues: { text_field: "" },
      });
      return <div  className="flex gap-8">
        <Controller
      control={control}
      name={"text_field"}
      render={({field}) => <FileField
        onChange={field.onChange}
        label="Small"
        size={FileFieldSize.SMALL}
      />}
    />
    <Controller
      control={control}
      name={"text_field"}
      render={({field}) => <FileField
      onChange={field.onChange}
        label="Medium"
      />}
    />
    <Controller
      control={control}
      name={"text_field"}
      render={({field}) => <FileField
        onChange={field.onChange}
        label="Large"
        size={FileFieldSize.LARGE}
      />}
    />
      </div>

}


export const Default: StoryObj<FileFieldProps>  =  {
  render: (props) => <FileStory {...props}/>
};
