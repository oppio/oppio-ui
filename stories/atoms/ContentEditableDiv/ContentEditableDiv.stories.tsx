import { Story } from "@storybook/react";
import React, { useState } from "react";
import { ContentEditableDiv, ContentEditableDivProps } from "../../../src/atoms/ContentEditableDiv";


const story = {
  title: "Atom/ContentEditableDiv",
  component: ContentEditableDiv,
};

export default story;

const Template: Story<ContentEditableDivProps> = (args) => {
  const [value, setValue] = useState("")
  return <ContentEditableDiv
    value={value}
    onChange={setValue}
    placeholder=""
  />
};

export const Success = Template.bind({})

