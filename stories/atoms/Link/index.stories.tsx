import { Meta, StoryObj } from "@storybook/react"
import {Link} from "../../../src/atoms/Link"
import { LinkProps } from "react-aria-components"




const meta: Meta<typeof Link> = {
  component: Link,
  title: "Atom/Link"
};

export default meta;

type Story = StoryObj<typeof Link>;

export const Primary: Story = {
  render: (args) => {
    return <Link {...args} />
  },
  args: {
   
  }
  
};