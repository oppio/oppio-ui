import { SelectField } from "../../../src/atoms/SelectField";
import { TableProps } from "../../../src/atoms/Table/types";

export type TableMockType = {id: string, title: string, slug: string, path: string, state: "active" | "inactive"};

export const header: TableProps<TableMockType>["columns"] = [
  {
    label: "ID", 
    hidden: {},
    sortable: true,
    renderCell: ({row, rowIndex}) => <div className="text-gray-900">{row.id}</div>,
    getValue: ({row, label}) => row["id"],
    
  },
  {
    label: "Title",
    hidden: {},
    sortable: true,
    renderCell: ({row, rowIndex}) => <div className="text-gray-900">{row.title}</div>,
    getValue: ({row, label}) => row["title"],
  },
  {
    label: "Slug",
    sortable: true,
    renderCell: ({row, rowIndex}) => <div className="text-gray-900">{row.slug}</div>,
    getValue: ({row, label}) => row["slug"],
    hidden: {},
  },
  {
    label: "Path",
    sortable: true,
    renderCell: ({row, rowIndex}) => <div className="text-gray-900">{row.path}</div>,
    getValue: ({row, label}) => row["path"],
    hidden: {},
  },
  {
    label: "State",
    sortable: true,
    renderCell: ({row, label}) => {
    return <div 
      onMouseDown={e => {e.stopPropagation()}}
      onPointerDown={e => {e.stopPropagation()}}


    >
    <SelectField
      value={["active"]}
      options={[
        {label: "Active", value: "active", search: "active"},
        {label: "Inactive", value: "inactive", search: "inactive"}
      ]}
      creatable={false}
      isMulti={true}
      onChange={() => console.log("test")}
    
    /></div>},
    getValue: ({row, label}) => row.state,
    hidden: {},
  }
];


export const body: TableProps<TableMockType>["rows"] = [
  {state: "inactive", id: "1", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "2", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "3", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "4", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "5", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "6", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "inactive", id: "7", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "8", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "9", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "10", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "11", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "12", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "inactive", id: "13", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "14", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "15", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "16", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "17", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "18", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "inactive", id: "19", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "20", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "21", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "22", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "23", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "inactive", id: "24", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "inactive", id: "25", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "inactive", id: "26", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "27", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "28", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "29", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "30", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "31", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "32", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "33", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "34", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "35", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "36", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {state: "active", id: "37", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {state: "active", id: "38", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {state: "active", id: "39", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  
];
