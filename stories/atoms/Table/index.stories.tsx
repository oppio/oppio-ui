import { FilterWidget, TableProps, TableState } from "../../../src/atoms/Table/types";
import {Table} from "../../../src/atoms/Table";
import { TableMockType, body, header } from "./mocks";
import { StoryObj } from "@storybook/react";
import { AddRow } from "../../../src/atoms/Table/AddRow";
import { v4 as uuid4} from "uuid"
import { Context, createContext, useState } from "react";
import { UseFormReturn, useForm } from "react-hook-form";
import { CollisionDetection, DndContext, MouseSensor, pointerWithin, rectIntersection, TouchSensor, useSensor, useSensors } from "@dnd-kit/core";
import { Spinner } from "atoms/Spinner";


const story = {
  title: "Atom/Table",
  component: Table,
};

export default story;


type Story = StoryObj<typeof Table<TableMockType>>;


const TableToolbar = () => <div className="sm:flex sm:items-center p-8">
<div className="sm:flex-auto">
  <h1 className="text-base font-semibold leading-6 text-gray-900">Users</h1>
  <p className="mt-2 text-sm text-gray-700">
    A list of all the users in your account including their name, title, email and role.
  </p>
</div>
<div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
  <button
    type="button"
    className="block rounded-md bg-primary-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-primary-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-primary-600"
  >
    Add user
  </button>
</div>
<AddRow<TableMockType>
  addColumnTemplate={() => {
    return {
    id: uuid4(),
    title: "New",
    state: "active",
    slug: "new",
    path: "/neww"
  }}}
/>
</div>;

const customCollisionDetectionAlgorithm: CollisionDetection = (args) => {
  // First, let's see if there are any collisions with the pointer
  const pointerCollisions = pointerWithin(args);
  // Collision detection algorithms return an array of collisions
  return pointerCollisions;

};

export const FilterClose: Story = {
  render: (args: TableProps<TableMockType>) => {
    const ret = useForm<TableState<TableMockType>>()
    const sensors = useSensors(
      useSensor(MouseSensor, {
        activationConstraint: {
          delay: 200, // Delay in milliseconds before drag starts
          tolerance: 10, // Tolerance in pixels before drag starts
        },
      }),
      useSensor(TouchSensor, {
        activationConstraint: {
          delay: 200, // Delay in milliseconds before drag starts
          tolerance: 10, // Tolerance in pixels before drag starts
        },
      })
    );

    return <div className="h-[500px]"><DndContext sensors={sensors} collisionDetection={customCollisionDetectionAlgorithm}><Table<TableMockType>
      {...args}
      showRemoveColumn
      onRowDoubleClick={() => console.log("detected")}
      components={{
        tableToolbar: ({form}) => <TableToolbar
        />
      }}
      endOfTable={<div className="w-full flex items-center justify-center">
        <Spinner />
    </div>}
    /></DndContext><div id="portal"/></div>
  },
  args: {
    columns: header,
    rows: body,
    ariaLabel: "Storybook",
    selectionMode: "multiple",
    getRowId: (row: TableMockType) => row.id,
    filterModelOpen: "",
    hiddenColumns: ["ID", "Slug"],
    onScrollEnd: () => console.log("onScrollEnd"),
    setFilterModalOpen: () => console.log("setFilterModalOpen"),
    selectedRows: ["2"],
    virtualize: true,
    pagination: {
      pageSize: 100,
      totalCount: body.length,
      page: 0,
      serverSide: false
    },
    handleLongRowPress: (e, row) => console.log("handleLongRowPress", row ),
    rowLongPressOptions: {
      onStart: () => console.log("onStart"),
      onFinish: () => console.log("onEnd"),
      onCancel: () => console.log("onCancel"),
    },
    sortState: {
      Title: {
        sortHandler: (obj: TableMockType) => obj.title
      },
      Path: {
        sortHandler: (obj: TableMockType) => obj.path
      },
      State: {
        sortHandler: (obj: TableMockType) => obj.state
      }
    },
    filterConfig: {
      Title: {
        modifiers: ["contains", "eq"],
        widget: FilterWidget.InputField
      },
      State: {
        modifiers: ["in", "eq"],
        widget: FilterWidget.SelectField,
        choices: [{value:"active", label: "Active", search: "active"}, {value:"inactive", label: "Inactive", search: "inactive"}]
      }
    },
    rowHeight: () => 50,
    getDndBehavior: (row: TableMockType) => {
      return ["draggable", "droppable"]
    },
    onDragEnd: (result) => console.log("dragend", result)
  }
};

function ServerSideTable(args: TableProps<TableMockType>) {
  const [paginationState, setPaginationState] = useState({
    page: 0,
    pageSize: 20,
    totalCount: args.rows.length,
    serverSide: true
  })
  return <div className="h-[500px]">
    <Table<TableMockType>
      {...args}
      showRemoveColumn
      pagination={paginationState}
      onFilterStateChange={(data) => console.log("A", data)}
      onSortStateChange={(data) => console.log("B", data)}
      rowHeight={() => 50}
      components={{
        tableToolbar: ({form}) => <TableToolbar
        />
      }}
    />
  </div>
}

export const ServerSide: Story = {
  render: (args: TableProps<TableMockType>) => {
    return <ServerSideTable
      {...args}
    />
  },
  args: {
    columns: header,
    rows: body,
    ariaLabel: "Storybook",
    selectionMode: "none",
    getRowId: (row: TableMockType) => row.id,
    filterModelOpen: "",
    hiddenColumns: ["ID", "Slug"],
    filterState: [
      {
        topology: "and",
        field: "Title",
        modifier: "contains",
        value: "Pizza"
      }
    ],
    setFilterModalOpen: () => console.log("setFilterModalOpen"),
    sortState: {
      Title: {
        sortHandler: (obj: TableMockType) => obj.title
      },
      Path: {
        sortHandler: (obj: TableMockType) => obj.path
      },
      State: {
        sortHandler: (obj: TableMockType) => obj.state
      }
    },
    filterConfig: {
      Title: {
        modifiers: ["contains", "eq"],
        widget: FilterWidget.InputField
      },
      State: {
        modifiers: ["contains", "eq"],
        widget: FilterWidget.SelectField,
        choices: [{value:"active", label: "Active", search: "active"}, {value:"inactive", label: "Inactive", search: "inactive"}]
      }
    }
  }
};