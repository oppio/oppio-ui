import { Story } from "@storybook/react";
import {RadioButtons,  RadioButtonsProps, RadioOption } from "../../../src/atoms/RadioButtons";

const story = {
  title: "Atom/Fields/RadioButtons",
  component: RadioButtons,
};

export default story;

const Template: Story<RadioButtonsProps> = (args) => {
  return <RadioButtons {...args}/>
};

export const Success = Template.bind({})

const settings: RadioOption[] = [
    {value: "1", name: 'Public access', description: 'This project would be available to anyone who has the link' },
    {value: "2", name: 'Private to Project Members', description: 'Only members of this project would be able to access' },
    {value: "3", name: 'Private to you', description: 'You are the only one able to access this project' },
  ]

Success.args = {
  label: "Time",
  options: settings
};
