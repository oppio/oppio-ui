import { Story } from "@storybook/react";
import React from "react";
import { Tooltip, TooltipProps } from "../../../src/atoms/Tooltip";

const story = {
  title: "Atom/Tooltip",
  component: Tooltip,
};

export default story;

const Template: Story<TooltipProps> = (args) => {

  return <Tooltip {...args}/>
};

export const Bottom = Template.bind({})

Bottom.args = {
    //opened: true,
    //position: "center",
    children: <div onClick={() => console.log("go some where")}>Hello</div>,
    tooltipText: "Check this out",
    //translucent: false,
    placement: "bottom"
};

export const Top = Template.bind({})

Top.args = {
    //opened: true,
    //position: "center",
    children: <div onClick={() => console.log("go some where")}>Hello</div>,
    tooltipText: "Check this out",
    //translucent: false,
    placement: "top"
};

export const LEFT = Template.bind({})

LEFT.args = {
    //opened: true,
    //position: "center",
    children: <div onClick={() => console.log("go some where")}>Hello</div>,
    tooltipText: "Check this out",
    //translucent: false,
    placement: "left"
};


export const Right = Template.bind({})

Right.args = {
    //opened: true,
    //position: "center",
    children: <div onClick={() => console.log("go some where")}>Hello</div>,
    tooltipText: "Check this out",
    //translucent: false,
    placement: "right"
};

