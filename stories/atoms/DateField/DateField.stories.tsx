import { useState } from "react";


import  {DateField, DateFieldProps } from "../../../src/atoms/DateField";
import { DateTime } from "luxon";
import { Story } from "@storybook/react";

const story = {
  title: "Atom/Fields/DateField",
  component: DateField,
};

export default story;


export const Default = {
  render: () => {
    const [date, setDate] = useState<DateTime>(DateTime.fromObject({ year: 2023, month: 11, day: 4, hour: 1, minute: 0, second: 0, millisecond: 0 }))
    return <DateField
    label="Date Selector"
    rangeSelector={false}
    value={date}
    onChange={(_date) => {
      _date && setDate(_date)
    }}
    endYear={2050}
    />
  },
};
