import { Story } from "@storybook/react";
import React from "react";
import { Controller, useForm } from "react-hook-form";

import {InputField, InputFieldProps} from "../../../src/atoms/InputField";
import { Spinner } from "atoms/Spinner";

const story = {
  title: "Atom/Fields/Input Field",
  component: InputField,
};

export default story;

export const Template: Story<InputFieldProps>  = (args: InputFieldProps) => {
  const { control, setValue, setError } = useForm({
    defaultValues: { text_field: "" },
  });
  return <Controller
  control={control}
  name={"text_field"}
  render={({field}) => <InputField
    {...field}
    label="test"
    placeholder="Search"
    leadingIcon={<Spinner size="sm"/>}
  />}
/>
};

export const Text = Template.bind({})

Text.args = {
  name: "text_field",
  label: "Text Field",
  maxLength: 24,
  readOnly: false,
  required: true,
  type: "text",
  value: "Hello World!",
  placeholder: "test"
};
