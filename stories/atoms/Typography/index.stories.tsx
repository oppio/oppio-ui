import { Meta, Story } from "@storybook/react"
import {Typography,  TypographyProps } from "../../../src/atoms/Typography"


const config: Meta =  {
  title: "Atom/Typography",
  component: Typography
}

export default config

const Template: Story<TypographyProps> = (args) => <Typography {...args}>test</Typography>

export const Default = Template.bind({})
