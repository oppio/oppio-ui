import { useState } from "react";


import {DropMenu,  DropMenuProps } from "../../../src/atoms/DropMenu";
import { DateTime } from "luxon";
import { Story } from "@storybook/react";

const story = {
  title: "Atom/DropMenu",
  component: DropMenu,
};

export default story;


export const Default = {
  render: () => {
    return <DropMenu
    ariaLabel="storybook dropdown"
    buttonText=""
    menuItems={[{
       key: "a",
       children: <>Hello</> ,
       isDisabled: true
    },
    {
        key: "b",
        children: <>World</>,
        disabled: true
     }]}
    />
  },
};
