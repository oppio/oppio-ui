import { Story } from "@storybook/react";
import React from "react";
import {TimeField,  TimeFieldProps } from "../../../src/atoms/TimeField";

const story = {
  title: "Atom/Fields/TimeField",
  component: TimeField,
};

export default story;

const Template: Story<TimeFieldProps> = (args) => {
  return <TimeField {...args}/>
};

export const Success = Template.bind({})

Success.args = {
  label: "Time"
};
