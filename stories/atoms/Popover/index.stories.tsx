import { StoryObj } from "@storybook/react";
import React from "react";

import {Popover,  PopoverProps } from "../../../src/atoms/Popover";

const story = {
  title: "Atom/Popover",
  component: Popover,
};

export default story;



const Template = (args: PopoverProps) => {

  return <Popover
  {...args}
  />;
};

type Story = StoryObj<typeof Template>;


export const Default: Story = {
  args: {
    popoverContent: () => <div className="p-4">I am the dog</div>,
    children: () => <div>Hello Family</div>
  }
}
