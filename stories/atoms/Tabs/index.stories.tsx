import React from "react";

import {Tabs,  TabsProps } from "../../../src/atoms/Tabs";
import { props } from "./__mock__";
import { Button } from "../../../src/atoms/Button";

const story = {
  title: "Atom/Base/Tabs",
  component: Tabs,
};

export default story;

export const Primary = (args: TabsProps) => {
const [activeTab, setActiveTab] = React.useState<number>(0);
return <div className="w-[100px]">
  <Button onClick={() => setActiveTab(activeTab - 1)}>
    Back
  </Button>
  <Button onClick={() => setActiveTab(activeTab + 1)}>
    Forward
  </Button>
  <Tabs {...args} currentTab={activeTab} keepMounted />

</div>
}

Primary.args = {
  ...props,
};
