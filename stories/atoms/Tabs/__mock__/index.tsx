import React from "react";
import { TabsProps } from "../../../../src/atoms/Tabs";

export const props: TabsProps = {
  name: "storybook",
  tabs: [{label:"Tab 1"}, {label: "Tab 2"}, {label: "Tab 3"}],
  children: [<>Tab Content 1</>, <>Tab Content 2</>, <>Tab Content 3</>],
  initialTab: 1,
};
