import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import {Tabs} from "../../../src/atoms/Tabs";
import { props } from "./__mock__";

test("default tabs", () => {
  const { container } = render(<Tabs {...props} />);
  const tabList = container.querySelector("[role=tablist]");
  const tabs = container.querySelectorAll("[role=tabpanel]");

  expect(tabList).toBeTruthy();
  expect(tabs.length).toBe(3);
  const tab1 = screen.getByText("Tab 1");
  const tab2 = screen.getByText("Tab 2");
  const tab3 = screen.getByText("Tab 3");
  const tabContent1 = screen.getByText("Tab Content 1");
  const tabContent2 = screen.getByText("Tab Content 2");
  const tabContent3 = screen.getByText("Tab Content 3");
  fireEvent.click(tab1);
  expect(tabContent1.classList.contains("hidden")).toBeFalsy();
  expect(tabContent2.classList.contains("hidden")).toBeTruthy();
  expect(tabContent3.classList.contains("hidden")).toBeTruthy();
  fireEvent.click(tab2);
  expect(tabContent2.classList.contains("hidden")).toBeFalsy();
  expect(tabContent1.classList.contains("hidden")).toBeTruthy();
  expect(tabContent3.classList.contains("hidden")).toBeTruthy();
  fireEvent.click(tab3);
  expect(tabContent3.classList.contains("hidden")).toBeFalsy();
  expect(tabContent2.classList.contains("hidden")).toBeTruthy();
  expect(tabContent1.classList.contains("hidden")).toBeTruthy();
});

test("test one tab", () => {
  const { container } = render(
    <Tabs {...props} tabs={props.tabs}>
      {props.children}
    </Tabs>
  );
  const tabList = container.querySelector("[role=tablist]");
  const tabs = container.querySelectorAll("[role=tabpanel]");
  expect(tabList).toBeTruthy();
  expect(tabs.length).toBe(1);
  const tab1 = screen.getByText("Tab 1");
  const tabContent1 = screen.getByText("Tab Content 1");
  fireEvent.click(tab1);
  expect(tabContent1.classList.contains("hidden")).toBeFalsy();
});
