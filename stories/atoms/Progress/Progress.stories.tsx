import { Story } from "@storybook/react";
import React from "react";
import {Progress,  ProgressProps } from "../../../src/atoms/Progress";

const story = {
  title: "Atom/Progress",
  component: Progress,
};

export default story;

const Template: Story<ProgressProps> = (args) => {
  return <Progress {...args}/>
};

export const Success = Template.bind({})

Success.args = {
  children: "Progress text"
};
