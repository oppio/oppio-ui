import { ImageField, ImageFieldProps, ImageFieldSize } from "atoms/ImageField";
import { Meta, StoryObj } from "@storybook/react";
import { Controller, useForm } from "react-hook-form";


const story: Meta = {
  title: "Atom/Fields/Image Field",
  component: ImageField,

};

export default story;

const ImageStory = (props: ImageFieldProps) => {
    const { control, setValue, setError } = useForm({
        defaultValues: { text_field: "" },
      });
      return <div  className="flex gap-8">
        <Controller
      control={control}
      name={"text_field"}
      render={({field}) => <ImageField
        {...field}
        label="Small"
        size={ImageFieldSize.SMALL}
      />}
    />
    <Controller
      control={control}
      name={"text_field"}
      render={({field}) => <ImageField
        {...field}
        label="Medium"
      />}
    />
    <Controller
      control={control}
      name={"text_field"}
      render={({field}) => <ImageField
        {...field}
        label="Large"
        size={ImageFieldSize.LARGE}
        onDrop={() =>  null}
      />}
    />
      </div>

}


export const Default: StoryObj<ImageFieldProps>  =  {
  render: (props) => <ImageStory {...props}/>
};
