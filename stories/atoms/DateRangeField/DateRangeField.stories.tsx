import { useState } from "react";


import  {DateRangeField, DateRangeFieldProps } from "../../../src/atoms/DateRangeField";
import { DateTime } from "luxon";


const story = {
  title: "Atom/Fields/DateRangeField",
  component: DateRangeField,
};

export default story;

const MockRangeSelector = (props: DateRangeFieldProps) => {
  const [date, setDate] = useState<[DateTime, DateTime]>([
    DateTime.fromObject({ year: 2023, month: 11, day: 4, hour: 1, minute: 0, second: 0, millisecond: 0 }),
    DateTime.fromObject({ year: 2023, month: 11, day: 5, hour: 1, minute: 0, second: 0, millisecond: 0 })
  ])
  return <DateRangeField
    {...props}
    label="Date Selector"
    rangeSelector
    value={date}
    onChange={(_date) => {
      _date && setDate(_date)
    }}
    endYear={2050}
  
  />
}


export const Default = {
  render: (props: DateRangeFieldProps) => {
    return <MockRangeSelector
      {...props}
    />
  },
};
