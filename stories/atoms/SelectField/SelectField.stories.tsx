import { Meta, StoryObj } from "@storybook/react"
import {SelectField,  SelectFieldProps } from "../../../src/atoms/SelectField"
import { useState } from "react";
import { Select } from "react-aria-components";


const Template = (props: SelectFieldProps<number>) => {
  const [value, setValue] = useState<number | undefined>(undefined)
  return <SelectField
    {...props}
    isMulti={false}
    value={value}
    onChange={(_opts: number | undefined) => setValue(_opts)}
  />
}


const config: Meta =  {
  title: "Atom/Fields/Select Field",
  component: Template
}


export default config


type Story = StoryObj<typeof Template>;


export const Creatable: Story = {
  
  args: {
    creatable: true,
    isMulti: true,
    label: "test",
    options: [
      { value: 1, label: 'Wade Cooper', search: 'Wade Cooper' },
      { value: 2, label: 'Arlene Mccoy', search: 'Arlene Mccoy' },
      { value: 3, label: 'Devon Webb', search: 'Devon Webb' },
      { value: 4, label: 'Tom Cook', search: 'Tom Cook' },
      { value: 5, label: 'Tanya Fox', search: 'Tanya Fox' },
      { value: 6, label: 'Hellen Schmidt', search: 'Hellen Schmidt' },
    ],
  }
}
export const Default: Story = {
  args: {
    creatable: false,
    isMulti: false,
    label: "test",
    options: [
      {value: 1, label: "Value 1", search: "Value 1"},
      {value: 2, label: "Value 2", search: "Value 2"}
    ],
  }
}

export const SelectInAreaScoll: StoryObj<SelectFieldProps<number>> = {
  args: {
    creatable: false,
    isMulti: false,
    portal: false,
    useFixed: false,
    label: "test",
    options: [
      {value: 1, label: "Value 1", search: "Value 1"},
      {value: 2, label: "Value 2", search: "Value 2"}
    ],
  },
  render: (args: SelectFieldProps<number>) => {
    return (
      <div style={{height: "3000px", overflow: "auto"}} className="absolute">
        <SelectField
          {...args}
          value={1}
          onChange={(val) => console.log(val)}
          creatable={false}
          isMulti={false}
          label="test"
          className="mt-32"
          clearable={true}
          comboOptionClassName="min-h-[200px]"
          portal={false}
          useFixed={false}
          onScrollEnd={() => console.log("scroll end")}
          options={[
            {value: 1, label: "Value 1 Value 1 Value 1 Value 1 Value 1 Value 1 Value 1 Value 1", search: "Value 1"},
            {value: 2, label: "Value 2", search: "Value 2"},
            {value: 3, label: "Value 3", search: "Value 3"},
            {value: 4, label: "Value 4", search: "Value 4"},
            {value: 5, label: "Value 5", search: "Value 5"},
            {value: 6, label: "Value 6", search: "Value 6"},
            {value: 7, label: "Value 7", search: "Value 7"},
            {value: 8, label: "Value 8", search: "Value 8"},
          ]}
        />
      </div>
      )
  }
}
