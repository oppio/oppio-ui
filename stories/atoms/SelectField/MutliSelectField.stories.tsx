import { Meta, StoryObj } from "@storybook/react"
import {SelectField,  SelectFieldProps } from "../../../src/atoms/SelectField"
import { useState } from "react";


const Template = (props: SelectFieldProps<number>) => {
  const [value, setValue] = useState<number[] | undefined>([])
  return <SelectField<number>
    {...props}
    isMulti={true}
    value={value}
    onChange={(_opts: number[] | undefined) => setValue(_opts)}
    options={[
      { value: 1, label: 'Wade Cooper', search: 'Wade Cooper' },
      { value: 2, label: 'Arlene Mccoy', search: 'Arlene Mccoy' },
      { value: 3, label: 'Devon Webb', search: 'Devon Webb' },
      { value: 4, label: 'Tom Cook', search: 'Tom Cook' },
      { value: 5, label: 'Tanya Fox', search: 'Tanya Fox' },
      { value: 6, label: 'Hellen Schmidt', search: 'Hellen Schmidt' },
    ]}
    onBlur={() => console.log("here")}
  />
}

const config: Meta =  {
  title: "Atom/Fields/Multi Select Field",
  component: Template
}

export default config

type Story = StoryObj<typeof Template>;

export const Creatable: Story = {
  args: {
    creatable: true,
    isMulti: true,
    label: "test",
    
  }
}


