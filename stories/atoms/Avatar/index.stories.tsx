import React from "react";

import {Avatar, AvatarProps, AvatarSize} from "../../../src/atoms/Avatar";

const story = {
  title: "Atom/Avatar",
  component: Avatar,
};

export const Default = (args: AvatarProps) => {
  return <div>
      <Avatar {...args} size={AvatarSize.SMALL} />
      <Avatar {...args} size={AvatarSize.MEDIUM}/>
      <Avatar {...args} size={AvatarSize.LARGE}/>
      <Avatar {...args} size={AvatarSize.SMALL} src="https://picsum.photos/100"/>
      <Avatar {...args} size={AvatarSize.MEDIUM} src="https://picsum.photos/100"/>
      <Avatar {...args} size={AvatarSize.LARGE} src="https://picsum.photos/100"/>
    </div>
};

Default.args = {
  firstName: "Wes",
  lastName: "Garlock",
  onClick: () => console.log("hello")
};

export default story;
