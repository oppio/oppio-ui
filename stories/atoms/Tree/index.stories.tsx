import { NodeModel } from "@minoru/react-dnd-treeview";
import { Meta, StoryObj } from "@storybook/react";
import Tree from "atoms/Tree";
import { useState } from "react";

const meta = {
    title: "Atom/Tree",
    component: Tree,
}

export default meta;

const initialState: NodeModel<{name: string}>[] = [
    {
        id: "1",
        parent: 0,
        text: "",
        droppable: true,
        data: {
            name: "TEST 1"
        }
    },
    {
        id: "2",
        parent: "1",
        text: "",
        droppable: true,
        data: {
            name: "TEST 2"
        }
    },
    {
        id: "3",
        parent: "1",
        text: "",
        droppable: true,
        data: {
            name: "TEST 3"
        }
    },
    {
        id: "4",
        parent: "2",
        text: "",
        droppable: true,
        data: {
            name: "TEST 4"
        }
    }
    ,
    {
        id: "5",
        parent: "3",
        text: "",
        droppable: true,
        data: {
            name: "TEST 5"
        }
    }
]

export const Template: StoryObj<typeof Tree> = {
    render: () => {
        const [nodes, setNodes] = useState<NodeModel<{name: string}>[]>(initialState);
        return <Tree
            nodes={nodes}
            setNodes={setNodes}
        />
    }
    
}