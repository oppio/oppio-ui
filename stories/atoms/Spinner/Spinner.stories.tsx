import { Story } from "@storybook/react";
import React from "react";


import {Spinner,  SpinnerProps } from "../../../src/atoms/Spinner";

const story = {
  title: "Atom/Spinner",
  component: Spinner,
};

export default story;

const Template: Story<SpinnerProps> = (args) => {

  return (
    <Spinner {...args}/>
  );
};

export const Default = Template.bind({})
