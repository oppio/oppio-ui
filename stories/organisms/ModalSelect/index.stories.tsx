import { Meta, StoryObj } from "@storybook/react"
import {ModalSelect,  ModalSelectProps } from "../../../src/organisms/ModalSelect/ModalSelect"
import { useState } from "react";


const Template = (props: ModalSelectProps<number>) => {
  const [value, setValue] = useState<number | null>(null)
  return <ModalSelect
    {...props}
    isMulti={false}
    value={value}
    onChange={(_opts: number) => setValue(_opts)}
  />
}


const config: Meta =  {
  title: "Organisms/ModalSelect",
  component: Template
}


export default config


type Story = StoryObj<typeof Template>;


export const Creatable: Story = {
  
  args: {
    isMulti: false,
    label: "test",
    options: [
      { value: 1, label: <>Wade Cooper</>, search: 'Wade Cooper' },
      { value: 2, label: <>Arlene Mccoy</>, search: 'Arlene Mccoy' },
      { value: 3, label: <>Devon Webb</>, search: 'Devon Webb' },
      { value: 4, label: <>Tom Cook</>, search: 'Tom Cook' },
      { value: 5, label: <>Tanya Fox</>, search: 'Tanya Fox' },
      { value: 6, label: <>Hellen Schmidt</>, search: 'Hellen Schmidt' },
    ],
  }
}
export const Default: Story = {
  args: {
    isMulti: false,
    label: "test",
    options: [
      {value: 1, label: <>Value 1</>, search: "Value 1"},
      {value: 2, label: <>Value 2</>, search: "Value 2"}
    ],
  }
}


