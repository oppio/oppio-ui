// Pagination.stories.ts|tsx

import type { Meta } from '@storybook/react';

import {SlideOver} from '../../../src/organisms/SlideOver';

const meta: Meta<typeof SlideOver> = {
  title: "Organisms/SlideOver",
  component: SlideOver,
};

export default meta;

const Template = () => {
  return <SlideOver
    open={true}
    setOpen={() => console.log("open")}
    panelTitle="Panel Title"
  
  />
}

export const Default = Template.bind({})