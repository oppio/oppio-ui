import type { Meta, StoryObj } from '@storybook/react';

import Chat, { ELLIPSIS_PLACEHOLDER } from '../../../src/organisms/Chat';


const meta: Meta<typeof Chat> = {
  component: Chat,
  title: "Organisms/Chat"
};

export default meta;

type Story = StoryObj<typeof Chat>;

export const Primary: Story = {
  render: (args) => {
    return <Chat {...args} />
  },
  args: {
    chatHistory: [
      {
        isMe: true,
        message: `
# Hello World!
&nbsp;
1. this 



2. is



3. a
<br>
4. test  



        `.trim(),
        id: "A"
      },
      {
        isMe: false,
        message: "Hello Back",
        id: "B"
      },
      {
        isMe: true,
        message: "Hello World!",
        id: "C"
      },
      {
        isMe: false,
        message: "Hello Back",
        id: "D"
      },
      {
        isMe: true,
        message: "Hello World!",
        id: "E"
      },
      {
        isMe: false,
        message: "Hello Back",
        id: "F"
      },
      {
        isMe: true,
        message: "Hello World!",
        id: "G"
      },
      {
        isMe: false,
        message: ELLIPSIS_PLACEHOLDER,
        id: "H"
      }
    ],
    onChange: (message) => {
      console.log(message)
    },
    className: "w-96 h-96"
  }
};