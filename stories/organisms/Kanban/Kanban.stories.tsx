import type { Meta, StoryObj } from '@storybook/react';

import {Kanban, KanbanProps} from '../../../src/organisms/Kanban/Kanban';
import { useEffect, useState } from 'react';
import { BaseItem } from 'organisms/Kanban/types';


type ItemType = {
    swimlane: string
    title: string
    description: string
    order: number
} & BaseItem




const meta: Meta<typeof Kanban> = {
  component: Kanban,
  title: "Organisms/Kanban"
};

export default meta;

type Story = StoryObj<KanbanProps<ItemType>>;


function StatefulKanban(props: Story['args']) {
    const [items, setItems] = useState<ItemType[]>(props?.items || [])
    const [swimlanes, setSwimlanes] = useState<string[]>(props?.swimlanes || [])
    useEffect(() => {
        setItems(props?.items || [])
    }, [props?.items])

    const handleSet = (data: ItemType) => {
        setItems(items.map((item, index) => {
            if (item.id === data.id) {
                return data
            }
            return item
        }))
    }

    return <Kanban<ItemType>
        setItem={handleSet}
        items={items}
        swimlaneProp={"swimlane"}
        swimlanes={swimlanes}
        renderItem={({item, style, transform, transition, ref}) => {
            return <div>{item.title}</div>
        }}
    />

}

export const Primary: Story = {
  render: (args) => {
    return <StatefulKanban {...args}/>
  },
  args: {
    items: [
        {id: "1", title: "Card Title1", description: "Card Description1", swimlane: "To Do",order: 2},
        {id: "2", title: "Card Title2", description: "Card Description2", swimlane: "In Progress", order: 3},
        {id: "3", title: "Card Title3", description: "Card Description3", swimlane: "Done", order: 1},
    ],
    swimlaneProp: "swimlane",
    //Header: KanbanHeader,
    //Card: KanbanCard,
    swimlanes: [
        "To Do",
        "In Progress",
        "Done"
    ],
  }
};

