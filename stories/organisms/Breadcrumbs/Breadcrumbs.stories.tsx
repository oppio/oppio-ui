import type { Meta, StoryObj } from '@storybook/react';

import {Breadcrumbs} from '../../../src/organisms/Breadcrumbs/Breadcrumbs';


const meta: Meta<typeof Breadcrumbs> = {
  component: Breadcrumbs,
  title: "Organisms/Breadcrumbs"
};

export default meta;

type Story = StoryObj<typeof Breadcrumbs>;

export const Primary: Story = {
  render: (args) => {
    return <Breadcrumbs {...args} />
  },
  args: {
    ariaLabel: "Storybook Breadcrumbs",
    className: "w-full",
    fullWidth: true,
    root: {
      name: "Home",
      href: "",
      onClick: () => console.log("home"),
      current: false,
    },
    links: [
      {
        name: "test",
        href: "test",
        onClick: () => "click",
        current: false
      }
    ]
  }
  
};