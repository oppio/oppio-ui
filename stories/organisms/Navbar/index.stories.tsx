import React, { act } from "react"
import {Story, Meta, StoryObj} from "@storybook/react"


import {Navbar, NavbarProps } from "../../../src/organisms/Navbar"
import { CloudArrowUpIcon } from "@heroicons/react/20/solid"
import { NavbarSize } from "../../../src/organisms/Navbar/types"
import { Breakpoint } from "../../../src/hooks"


const config: Meta = {
  title: "Organisms/Navbar",
  component: Navbar
}

export default config

const NavbarMock = (props: NavbarProps) => {
  const [navbarSize, setNavbarSize] = React.useState<NavbarSize>(NavbarSize.LARGE)
  const [activeIndex, setActiveIndex] = React.useState<number>(0)
  return <Navbar {...props} activeIndex={activeIndex} setActiveIndex={setActiveIndex} showNavbar={navbarSize !== NavbarSize.HIDDEN} setNavbarSize={(val) => {console.log("Test", val); setNavbarSize(val)}} navbarSize={navbarSize}/>
}



export const Active: StoryObj<NavbarProps> =  {
  render: (args) => <NavbarMock {...args}/>,
  args: {
    onMenuItemClick: (value?: string) => null,
    breakpoint: Breakpoint.MD,
    showNavbar: true,
    menuItems: [
        {
        label: "World",
        as: "div",
        Icon: CloudArrowUpIcon,
        onClick: (href) => null,
        active: true
        }
        ,
        {
        label: "World",
        as: "div",
        Icon: CloudArrowUpIcon,
        onClick: (href) => null,
        active: true
        }
        ,
        {
        label: "World",
        as: "div",
        Icon: CloudArrowUpIcon,
        onClick: (href) => null,
        active: true
        }
        ,
        {
        label: "World",
        as: "div",
        Icon: CloudArrowUpIcon,
        onClick: (href) => null,
        active: true
        }
    ],
    footer: <div>Footer</div>
  }
}







