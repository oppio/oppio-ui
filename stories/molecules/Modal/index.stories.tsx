import {Story, Meta} from "@storybook/react"
import React from "react"
import {Modal,  ModalProps } from "../../../src/molecules/Modal"

const config: Meta = {
  title: "Molecules/Modal",
  component: Modal
}

export default config

const Template: Story<ModalProps> = (args) => {
  const [open, setOpen] = React.useState(true)
  
  return <Modal 
    {...args}
    opened={open}
    setOpened={setOpen}
    >
      {args.children}
    </Modal>
}

export const Default = Template.bind({})

Default.args = {
  children: <>Hello</>,
  title: "test title",
}
