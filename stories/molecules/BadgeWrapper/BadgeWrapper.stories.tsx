import { Story } from "@storybook/react";
import React from "react";

import {BadgeWrapper,  BadgeWrapperColor,  BadgeWrapperProps,  } from "../../../src/molecules/BadgeWrapper/BadgeWrapper";
import { Badge, BadgeColor, BadgeStyle } from "../../../src/atoms/Badge";

const story = {
  title: "Atom/BadgeWrapper",
  component: BadgeWrapper,
};

export default story;

const Template: Story<BadgeWrapperProps> = (args) => {

  return (
    <BadgeWrapper {...args}/>
  );
};

export const Success = Template.bind({})

Success.args = {
  color: BadgeWrapperColor.GREY,
  children: <div>test</div>,
  badge: <Badge small badgeStyle={BadgeStyle.PILL} color={BadgeColor.RED}>1</Badge>
};
