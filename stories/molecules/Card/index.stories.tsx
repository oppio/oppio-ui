import {Story, Meta} from "@storybook/react"
import {Card,  CardProps } from "../../../src/molecules/Card"

const config: Meta = {
  title: "Molecules/Card",
  component: Card
}

export default config

const Template: Story<CardProps> = (args) => {
  return <Card {...args} />
}

export const Default = Template.bind({})

Default.args = {
  children: <div>Hello</div>,
  header:<>Hello Header</>,
  footer:<>Hello Footer</>,
}
