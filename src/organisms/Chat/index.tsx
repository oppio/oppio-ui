import { PaperAirplaneIcon, StopCircleIcon } from "@heroicons/react/20/solid"
import { AnimatedEllipse } from "atoms/AnimatedEllipse"
import { memo,  useEffect, useRef, useState } from "react"
import { classNames } from "server-side"
import {v4 as uuid4} from "uuid"
import Markdown from 'react-markdown'

export const ELLIPSIS_PLACEHOLDER = "//////////"

const ChatLine = ({ id, children, isMe, chatItemClassName }: {id: string, children: string, isMe: boolean, chatItemClassName?: (item: ChatItem) => string}) => {
    return <div className={classNames("flex w-full", isMe && "justify-end", chatItemClassName && chatItemClassName({
        id: id,
        isMe: isMe,
        message: children
    }))}>
        <div id={id} className={classNames(isMe ? "bg-primary-500 dark:bg-gray-600" : "bg-primary-600  dark:bg-gray-900", "text-white p-3 px-3 rounded-md max-w-[80%]")}>
        {children === ELLIPSIS_PLACEHOLDER ? <AnimatedEllipse /> : <Markdown>{children}</Markdown>}
        </div>
    </div>
}

const ChatLineMemo = memo(ChatLine, (prev, next) => {
    return prev.children === next.children
})

export type ChatItem = {
    id: string
    isMe: boolean,
    message: string
}

export type ChatProps = {
    className?: string,
    inputClassName?: string
    chatItemClassName?: (item: ChatItem) => string
    chatHistory: ChatItem[],
    onChange: (message: ChatItem) => void,
    generateNewId?: () => string
    cancelMessage?: () => void
    placeholder?: string
}

export default function Chat({inputClassName, placeholder, chatItemClassName, chatHistory, onChange, className, generateNewId = uuid4, cancelMessage}:ChatProps ) {
    const [ref, setRef] = useState<HTMLTextAreaElement | null>(null)
    const chatWindowRef = useRef<HTMLDivElement | null>(null)
    const [lastLine, setLastLine] = useState<ChatItem | null>(null) 

    const handleOnChange = () => {
        if (ref) {
            onChange({
                id: generateNewId(),
                message: ref.value,
                isMe: true
            })
            ref.value = ""
        }
    }

    useEffect(() => {
        if (lastLine && chatWindowRef.current) {
            const targetElement = document.getElementById(lastLine.id)
            if (!targetElement || !chatWindowRef.current) return
            const targetPosition = targetElement.offsetTop - chatWindowRef.current.offsetTop;
            chatWindowRef.current.scrollTo({
                top: targetPosition,
                behavior: 'smooth'
            });
        }
    }, [lastLine])

    useEffect(() => {
        if (chatHistory.length > 0) {
            setLastLine(chatHistory[chatHistory.length - 1])
        }
    }, [chatHistory])

    useEffect(() => {
        if (ref) {
            ref.focus()
        }
    }, [ref])


    return <div className={classNames("min-w-[300px] h-64 ring-1 ring-gray-200 shadow-lg rounded-lg flex flex-col justify-between dark:bg-gray-800", className)}>
        <div ref={chatWindowRef} className="p-2 overflow-y-scroll flex flex-col gap-2">
            {chatHistory.map((item) => <ChatLineMemo id={item.id} key={item.id} chatItemClassName={chatItemClassName} isMe={item.isMe}>{item.message}</ChatLineMemo>)}
        </div>
        <div className="p-2 border-t flex gap-1 items-center">
            <textarea
                placeholder={placeholder}
                ref={setRef}
                onKeyDown={(e) => {
                    if (e.key === "Enter" && !e.shiftKey) {
                        e.preventDefault()
                        handleOnChange()
                    }
                }}
                className={classNames("w-full p-2 rounded-md h-20 resize-none bg-white dark:bg-gray-700 text-gray-900 dark:text-white", inputClassName)}
            />
            {cancelMessage ? <StopCircleIcon onClick={cancelMessage} className="w-8 h-8 text-primary-700 p-1 rounded-full cursor-pointer" /> : <PaperAirplaneIcon
                onClick={handleOnChange}
                className="w-8 h-8 text-primary-700 p-1 rounded-full cursor-pointer"
            /> }
        </div>
    </div>
}