import React, {Dispatch, SetStateAction, act, useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {createPortal, unstable_batchedUpdates} from 'react-dom';
import {
  CancelDrop,
  closestCenter,
  pointerWithin,
  rectIntersection,
  CollisionDetection,
  DndContext,
  DragOverlay,
  DropAnimation,
  getFirstCollision,
  KeyboardSensor,
  MouseSensor,
  TouchSensor,
  Modifiers,
  useDroppable,
  UniqueIdentifier,
  useSensors,
  useSensor,
  MeasuringStrategy,
  KeyboardCoordinateGetter,
  defaultDropAnimationSideEffects,
} from '@dnd-kit/core';
import {
  AnimateLayoutChanges,
  SortableContext,
  useSortable,
  arrayMove,
  defaultAnimateLayoutChanges,
  verticalListSortingStrategy,
  SortingStrategy,
  horizontalListSortingStrategy,
} from '@dnd-kit/sortable';
import {CSS} from '@dnd-kit/utilities';
import {coordinateGetter as multipleContainersCoordinateGetter} from './multipleContainersKeyboardCoordinates';


import {createRange} from './utils';
import { Container, ContainerProps } from './Container';
import { Item, ItemProps } from './Item';
import { BaseItem } from './types';
import _ from 'lodash';

export default {
  title: 'Presets/Sortable/Multiple Containers',
};

const animateLayoutChanges: AnimateLayoutChanges = (args) =>
  defaultAnimateLayoutChanges({...args, wasDragging: true});

function DroppableContainer({
  children,
  columns = 1,
  disabled,
  id,
  items,
  style,
  ...props
}: ContainerProps & {
  disabled?: boolean;
  id: UniqueIdentifier;
  items: UniqueIdentifier[];
  style?: React.CSSProperties;
}) {

  const {
    active,
    attributes,
    isDragging,
    listeners,
    over,
    setNodeRef,
    transition,
    transform,
  } = useSortable({
    id,
    data: {
      type: 'container',
      children: items,
    },
    animateLayoutChanges,
  });
  const isOverContainer = over
    ? (id === over.id && active?.data.current?.type !== 'container') ||
      items?.includes(over.id)
    : false;

  return (
    <Container
      ref={disabled ? undefined : setNodeRef}
      style={{
        ...style,
        transition,
        transform: CSS.Translate.toString(transform),
        opacity: isDragging ? 0.5 : undefined,
      }}
      hover={isOverContainer}
      handleProps={{
        ...attributes,
        ...listeners,
      }}
      columns={columns}
      {...props}
    >
      
        {children}
    </Container>
  );
}

const dropAnimation: DropAnimation = {
  sideEffects: defaultDropAnimationSideEffects({
    styles: {
      active: {
        opacity: '0.5',
      },
    },
  }),
};

type Items = Record<UniqueIdentifier, UniqueIdentifier[]>;




export interface KanbanProps<T extends BaseItem> {
  adjustScale?: boolean;
  cancelDrop?: CancelDrop;
  columns?: number;
  containerStyle?: React.CSSProperties;
  coordinateGetter?: KeyboardCoordinateGetter;
  getItemStyles?(args: {
    value: UniqueIdentifier;
    index: number;
    overIndex: number;
    isDragging: boolean;
    containerId: UniqueIdentifier;
    isSorting: boolean;
    isDragOverlay: boolean;
  }): React.CSSProperties;
  wrapperStyle?(args: {index: number}): React.CSSProperties;
  itemCount?: number;
  items: T[];
  setItem: (data: T) => void 
  handle?: boolean;
  renderItem(props: ItemProps<T>): React.ReactElement;
  strategy?: SortingStrategy;
  modifiers?: Modifiers;
  minimal?: boolean;
  trashable?: boolean;
  scrollable?: boolean;
  vertical?: boolean;
  swimlaneProp: keyof T;
  swimlanes: string[];
  setSwimLanes?: Dispatch<SetStateAction<string[]>>
}


export const TRASH_ID = 'void';
const PLACEHOLDER_ID = 'placeholder';
const empty: UniqueIdentifier[] = [];

export function Kanban<T extends BaseItem>({
  adjustScale = false,
  cancelDrop,
  columns,
  handle = false,
  items,
  containerStyle,
  coordinateGetter = multipleContainersCoordinateGetter,
  getItemStyles = () => ({}),
  wrapperStyle = () => ({}),
  minimal = false,
  modifiers,
  setItem,
  renderItem,
  strategy = verticalListSortingStrategy,
  trashable = false,
  vertical = false,
  scrollable,
  swimlaneProp,
  swimlanes
}: KanbanProps<T>) {

  const orderedItems = useMemo(() => _.sortBy(items, ["order"]), [items])

  const [itemsData, setItemData] = useState<Items>(() => {
    return orderedItems.reduce((acc, item) => {
      const swimlane = item[swimlaneProp] as string;
      if (!acc[swimlane]) {
        acc[swimlane] = []
      }
      acc[swimlane].push(item.id)
      return acc
    }, swimlanes.reduce((acc, lane) => ({...acc, [lane]: []}), {}) as Items)
  });

  const itemMap = useMemo(() => {
    return items.reduce((acc, item) => {
      acc[item.id] = item
      return acc
    }, {} as Record<UniqueIdentifier, T>)
  }, [orderedItems])


  const [containers, setContainers] = useState(
    Object.keys(itemsData) as UniqueIdentifier[]
  );
  const [activeId, setActiveId] = useState<UniqueIdentifier | null>(null);
  const lastOverId = useRef<UniqueIdentifier | null>(null);
  const recentlyMovedToNewContainer = useRef(false);
  const isSortingContainer = activeId ? containers.includes(activeId) : false;

  /**
   * Custom collision detection strategy optimized for multiple containers
   *
   * - First, find any droppable containers intersecting with the pointer.
   * - If there are none, find intersecting containers with the active draggable.
   * - If there are no intersecting containers, return the last matched intersection
   *
   */
  const collisionDetectionStrategy: CollisionDetection = useCallback(
    (args) => {
      if (activeId && activeId in itemsData) {
        return closestCenter({
          ...args,
          droppableContainers: args.droppableContainers.filter(
            (container) => container.id in itemsData
          ),
        });
      }

      // Start by finding any intersecting droppable
      const pointerIntersections = pointerWithin(args);
      const intersections =
        pointerIntersections.length > 0
          ? // If there are droppables intersecting with the pointer, return those
            pointerIntersections
          : rectIntersection(args);
      let overId = getFirstCollision(intersections, 'id');

      if (overId != null) {
        if (overId === TRASH_ID) {
          // If the intersecting droppable is the trash, return early
          // Remove this if you're not using trashable functionality in your app
          return intersections;
        }

        if (overId in itemsData) {
          const containerItems = itemsData[overId];

          // If a container is matched and it contains itemsData (columns 'A', 'B', 'C')
          if (containerItems.length > 0) {
            // Return the closest droppable within that container
            overId = closestCenter({
              ...args,
              droppableContainers: args.droppableContainers.filter(
                (container) =>
                  container.id !== overId &&
                  containerItems.includes(container.id)
              ),
            })[0]?.id;
          }
        }

        lastOverId.current = overId;

        return [{id: overId}];
      }

      // When a draggable item moves to a new container, the layout may shift
      // and the `overId` may become `null`. We manually set the cached `lastOverId`
      // to the id of the draggable item that was moved to the new container, otherwise
      // the previous `overId` will be returned which can cause itemsData to incorrectly shift positions
      if (recentlyMovedToNewContainer.current) {
        lastOverId.current = activeId;
      }

      // If no droppable is matched, return the last match
      return lastOverId.current ? [{id: lastOverId.current}] : [];
    },
    [activeId, itemsData]
  );
  const [clonedItems, setClonedItems] = useState<Items | null>(null);
  const sensors = useSensors(
    useSensor(MouseSensor),
    useSensor(TouchSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter,
    })
  );

  const findContainer = (id: UniqueIdentifier) => {
    if (id in itemsData) {
      return id;
    }

    return Object.keys(itemsData).find((key) => itemsData[key].includes(id));
  };

  const getIndex = (id: UniqueIdentifier) => {
    const container = findContainer(id);

    if (!container) {
      return -1;
    }

    const index = itemsData[container].indexOf(id);

    return index;
  };

  const onDragCancel = () => {
    if (clonedItems) {
      // Reset itemsData to their original state in case itemsData have been
      // Dragged across containers
      setItemData(clonedItems);
    }

    setActiveId(null);
    setClonedItems(null);
  };

  useEffect(() => {
    requestAnimationFrame(() => {
      recentlyMovedToNewContainer.current = false;
    });
  }, [itemsData]);


  return (
    <DndContext
      sensors={sensors}
      collisionDetection={collisionDetectionStrategy}
      measuring={{
        droppable: {
          strategy: MeasuringStrategy.Always,
        },
      }}
      onDragStart={({active}) => {
        setActiveId(active.id);
        setClonedItems(itemsData);
      }}
      onDragOver={({active, over}) => {
        const overId = over?.id;

        if (overId == null || overId === TRASH_ID || active.id in itemsData) {
          return;
        }

        const overContainer = findContainer(overId);
        const activeContainer = findContainer(active.id);

        if (!overContainer || !activeContainer) {
          return;
        }
        if (activeContainer !== overContainer) {
          setItemData((itemsArg) => {
            const activeItems = itemsData[activeContainer];
            const overItems = itemsData[overContainer];
            const overIndex = overItems.indexOf(overId);
            const activeIndex = activeItems.indexOf(active.id);

            let newIndex: number;

            if (overId in itemsData) {
              newIndex = overItems.length + 1;
            } else {
              const isBelowOverItem =
                over &&
                active.rect.current.translated &&
                active.rect.current.translated.top >
                  over.rect.top + over.rect.height;

              const modifier = isBelowOverItem ? 1 : 0;

              newIndex =
                overIndex >= 0 ? overIndex + modifier : overItems.length + 1;
            }

            recentlyMovedToNewContainer.current = true;
            // original logic
            const data = {
              ...itemsData,
              [activeContainer]: itemsData[activeContainer].filter(
                (item) => item !== active.id
              ),
              [overContainer]: [
                ...itemsData[overContainer].slice(0, newIndex),
                itemsData[activeContainer][activeIndex],
                ...itemsData[overContainer].slice(
                  newIndex,
                  itemsData[overContainer].length
                ),
              ],
            };
            return data
          });
        }
      }}
      onDragEnd={({active, over}) => {
        if (active.id in itemsData && over?.id) {
          setContainers((containers) => {
            const activeIndex = containers.indexOf(active.id);
            const overIndex = containers.indexOf(over.id);
            return arrayMove(containers, activeIndex, overIndex);
          });
        }

        const activeContainer = findContainer(active.id);
        
        if (!activeContainer) {
          setActiveId(null);
          return;
        }

        const overId = over?.id;
        
        if (overId == null) {
          setActiveId(null);
          return;
        }

        if (overId === TRASH_ID) {
          setItemData((itemsArg) => ({
            ...itemsArg,
            [activeContainer]: itemsData[activeContainer].filter(
              (id) => id !== activeId
            ),
          }))
          setActiveId(null);
          return;
        }

        if (overId === PLACEHOLDER_ID) {
          const newContainerId = getNextContainerId();

          unstable_batchedUpdates(() => {
            setContainers((containers) => [...containers, newContainerId]);
            setItemData( (itemsArg) => ({
              ...itemsArg,
              [activeContainer]: itemsData[activeContainer].filter(
                (id) => id !== activeId
              ),
              [newContainerId]: [active.id],
            }));
            setActiveId(null);
            setItem(
              {
                ...itemMap[active.id],
                [swimlaneProp]: newContainerId
              }
            )
          });
          return;
        }

        const overContainer = findContainer(overId);

        if (overContainer) {
          
          
          const activeIndex = itemsData[activeContainer].indexOf(active.id);
          const overIndex = itemsData[overContainer].indexOf(overId);

          const activeItem = {...itemMap[active.id]}
          activeItem[swimlaneProp] = overContainer.toString() as unknown as T[keyof T]
          if (orderedItems.length > 0) {
            
            if (activeIndex !== overIndex) {
              updateActiveItem(activeItem, itemMap, orderedItems, overId, swimlaneProp)
              setItemData((itemsArg) => ({
                ...itemsArg,
                [overContainer]: arrayMove(
                  itemsData[overContainer],
                  activeIndex,
                  overIndex
                ),
              }));
            } else {
              if (over?.data.current?.sortable.index === 0) {
                updateActiveItem(activeItem, itemMap, orderedItems, overId, swimlaneProp)

              } else if (over?.data.current?.sortable.index === itemsData[overContainer].length - 1) {
                const index = orderedItems.findIndex((item) => item.id === overId)
                if (index === -1) {
                  throw Error("Item not in list")
                }
                if (index + 1 === orderedItems.length) {
                  updateActiveItem(activeItem, itemMap, orderedItems, overId, swimlaneProp)
                } else {
                  updateActiveItem(activeItem, itemMap, orderedItems, orderedItems[index + 1].id, swimlaneProp)
                }
              }
            }
          }
          setItem(activeItem) 
        }
        setActiveId(null);
      }}
      cancelDrop={cancelDrop}
      onDragCancel={onDragCancel}
      modifiers={modifiers}
    >
      <div
        className='grid grid-flow-col grid-rows-1 auto-cols-auto gap-1 w-full overflow-x-scroll py-4'
        style={{
          display: 'grid gap-2',
          boxSizing: 'border-box',
          padding: 20,
          gridAutoFlow: vertical ? 'row' : 'column',
        }}
      >
        <SortableContext
          items={[...containers, PLACEHOLDER_ID]}
          strategy={
            vertical
              ? verticalListSortingStrategy
              : horizontalListSortingStrategy
          }
        >
          {swimlanes.map((lane) => (
            <DroppableContainer
              key={lane}
              id={lane}
              label={lane}
              columns={columns}
              items={itemsData[lane] ?? []}
              scrollable={scrollable}
              style={containerStyle}
              unstyled={minimal}
              onRemove={() => handleRemove(lane)}
            >
              <SortableContext items={itemsData[lane] || []} strategy={strategy}>
                {itemsData[lane]?.map((value, index) => {
                  return (
                    <SortableItem
                      disabled={isSortingContainer}
                      key={value}
                      id={value}
                      index={index}
                      handle={handle}
                      style={getItemStyles}
                      wrapperStyle={wrapperStyle}
                      renderItem={renderItem}
                      containerId={lane}
                      getIndex={getIndex}
                      itemMap={itemMap}
                    />
                  );
                })}
              </SortableContext>
            </DroppableContainer>
          ))}
          {/*minimal ? undefined : (
            <DroppableContainer
              id={PLACEHOLDER_ID}
              disabled={isSortingContainer}
              items={empty}
              onClick={handleAddColumn}
              placeholder
            >
              + Add column
            </DroppableContainer>
          )*/}
        </SortableContext>
      </div>
      {createPortal(
        <DragOverlay adjustScale={adjustScale} dropAnimation={dropAnimation}>
          {activeId
            ? containers.includes(activeId)
              ? renderContainerDragOverlay(activeId)
              : renderSortableItemDragOverlay(activeId)
            : null}
        </DragOverlay>,
        document.body
      )}
      {trashable && activeId && !containers.includes(activeId) ? (
        <Trash id={TRASH_ID} />
      ) : null}
    </DndContext>
  );

  function renderSortableItemDragOverlay(id: UniqueIdentifier) {
    return (
      <Item
        value={id}
        handle={handle}
        style={getItemStyles({
          containerId: findContainer(id) as UniqueIdentifier,
          overIndex: -1,
          index: getIndex(id),
          value: id,
          isSorting: true,
          isDragging: true,
          isDragOverlay: true,
        })}
        item={itemMap[id]}
        color={getColor(id)}
        wrapperStyle={wrapperStyle({index: 0})}
        renderItem={renderItem}
        dragOverlay
      />
    );
  }

  function renderContainerDragOverlay(containerId: UniqueIdentifier) {
    return (
      <Container
        label={containerId.toString()}
        columns={columns}
        style={{
          height: '100%',
        }}
        shadow
        unstyled={false}
      >
        <div >
        {itemsData[containerId]?.map((item, index) => (
          <Item
            key={item}
            value={item}
            handle={handle}
            item={itemMap[item]}
            style={getItemStyles({
              containerId,
              overIndex: -1,
              index: getIndex(item),
              value: item,
              isDragging: false,
              isSorting: false,
              isDragOverlay: false,
            })}
            color={getColor(item)}
            wrapperStyle={wrapperStyle({index})}
            renderItem={renderItem}
          />
        ))}
        </div>
      </Container>
    );
  }

  function handleRemove(containerID: UniqueIdentifier) {
    setContainers((containers) =>
      containers.filter((id) => id !== containerID)
    );
  }

  function handleAddColumn() {
    const newContainerId = getNextContainerId();

    unstable_batchedUpdates(() => {
      setContainers((containers) => [...containers, newContainerId]);
      setItemData((itemsArg) => ({
        ...itemsArg,
        [newContainerId]: [],
      }));
    });
  }

  function getNextContainerId() {
    const containerIds = Object.keys(itemsData);
    const lastContainerId = containerIds[containerIds.length - 1];

    return String.fromCharCode(lastContainerId.charCodeAt(0) + 1);
  }
}

function getColor(id: UniqueIdentifier) {
  switch (String(id)[0]) {
    case 'A':
      return '#7193f1';
    case 'B':
      return '#ffda6c';
    case 'C':
      return '#00bcd4';
    case 'D':
      return '#ef769f';
  }

  return undefined;
}

function Trash({id}: {id: UniqueIdentifier}) {
  const {setNodeRef, isOver} = useDroppable({
    id,
  });

  return (
    <div
      ref={setNodeRef}
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'fixed',
        left: '50%',
        marginLeft: -150,
        bottom: 20,
        width: 300,
        height: 60,
        borderRadius: 5,
        border: '1px solid',
        borderColor: isOver ? 'red' : '#DDD',
      }}
    >
      Drop here to delete
    </div>
  );
}

interface SortableItemProps<T extends BaseItem> {
  containerId: UniqueIdentifier;
  id: UniqueIdentifier;
  index: number;
  handle: boolean;
  disabled?: boolean;
  style(args: any): React.CSSProperties;
  getIndex(id: UniqueIdentifier): number;
  renderItem(props: ItemProps<T>): React.ReactElement;
  wrapperStyle({index}: {index: number}): React.CSSProperties;
  itemMap: Record<UniqueIdentifier, T>,
}

function SortableItem<T extends BaseItem>({
  disabled,
  id,
  index,
  handle,
  renderItem,
  style,
  containerId,
  getIndex,
  wrapperStyle,
  itemMap
}: SortableItemProps<T>) {
  const {
    setNodeRef,
    setActivatorNodeRef,
    listeners,
    isDragging,
    isSorting,
    over,
    overIndex,
    transform,
    transition,
  } = useSortable({
    id,
  });
  const mounted = useMountStatus();
  const mountedWhileDragging = isDragging && !mounted;

  return (
    <Item
      ref={disabled ? undefined : setNodeRef}
      value={id}
      dragging={isDragging}
      sorting={isSorting}
      handle={handle}
      handleProps={handle ? {ref: setActivatorNodeRef} : undefined}
      index={index}
      item={itemMap[id]}
      wrapperStyle={wrapperStyle({index})}
      style={style({
        index,
        value: id,
        isDragging,
        isSorting,
        overIndex: over ? getIndex(over.id) : overIndex,
        containerId,
      })}
      color={getColor(id)}
      transition={transition}
      transform={transform}
      fadeIn={mountedWhileDragging}
      listeners={listeners}
      renderItem={renderItem}
    />
  );
}

function useMountStatus() {
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    const timeout = setTimeout(() => setIsMounted(true), 500);

    return () => clearTimeout(timeout);
  }, []);

  return isMounted;
}

function updateActiveItem<T extends BaseItem>(activeItem: T, itemMap: Record<UniqueIdentifier, T>, orderedItems: T[], targetId: UniqueIdentifier, swimlaneProp: keyof T) {
  // The very first item in the list
  const overItem = itemMap[targetId]
  if (orderedItems[0].id === targetId) {
                
    activeItem.order = overItem.order / 2
  }
  // The very last item in the list
  else if (orderedItems[orderedItems.length - 1].id === overItem.id) {
    // easy to find bisecting point
    
    activeItem.order = (orderedItems[orderedItems.length - 1].order + 1)
  } else {
    
    // find the index of the item that is before the overItem
    const overItemIndex = orderedItems.findIndex((item) => item.id === overItem.id)
    if (overItemIndex === -1) {
      throw Error("Item not in list")
    }
    
    
    // bisecting point
    activeItem.order = (orderedItems[overItemIndex].order + orderedItems[overItemIndex - 1].order) / 2

  }
  return activeItem
}