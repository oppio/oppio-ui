import { Squares2X2Icon, TrashIcon } from '@heroicons/react/20/solid';
import React, {forwardRef} from 'react';
import { classNames } from '../../server-side';
import lodash from 'lodash';
import { Typography } from '../../atoms/Typography';


const Handle = ({...props}) => {
    return <Squares2X2Icon {...props} className={classNames(props.className, "text-primary-400 w-4 h-4")}/>
}

const Remove = ({...props}) => {
    return <TrashIcon {...props} className={classNames(props.className, "text-red-400 w-4 h-4")}/>
}





export interface ContainerProps {
  children: React.ReactNode;
  columns?: number;
  label?: string;
  style?: React.CSSProperties;
  horizontal?: boolean;
  hover?: boolean;
  handleProps?: React.HTMLAttributes<any>;
  scrollable?: boolean;
  shadow?: boolean;
  placeholder?: boolean;
  unstyled?: boolean;
  onClick?(): void;
  onRemove?(): void;
  allowEditSwimLanes?: boolean
}

export const Container = forwardRef<HTMLDivElement, ContainerProps>(
  (
    {
      children,
      columns = 1,
      handleProps,
      horizontal,
      hover,
      onClick,
      onRemove,
      label,
      placeholder,
      style,
      scrollable,
      shadow,
      unstyled,
      allowEditSwimLanes,
      ...props
    }: ContainerProps,
    ref
  ) => {
    const Component = 'div';
    const styles = {
        Container: '',
        Header: 'bg-primary-300 dark:bg-gray-700 p-2 rounded-t-lg flex justify-between',
        Actions: 'flex gap-2 items-center',
        horizontal: 'KanbanContainer--horizontal',
        hover: 'KanbanContainer--hover',
        placeholder: 'KanbanContainer--placeholder',
        scrollable: 'KanbanContainer--scrollable',
        shadow: 'KanbanContainer--shadow',
        unstyled: 'KanbanContainer--unstyled'
    }
    return (
      <Component
        {...props}
        ref={ref}
        style={
          {
            ...style,
            '--columns': columns,
          } as React.CSSProperties
        }
        className={classNames(
          styles.Container,
          unstyled && styles.unstyled,
          horizontal && styles.horizontal,
          hover && styles.hover,
          placeholder && styles.placeholder,
          scrollable && styles.scrollable,
          shadow && styles.shadow
        )}
        onClick={onClick}
        tabIndex={onClick ? 0 : undefined}
      >
        {label ? (
          <div className={styles.Header}>
            <Typography className='font-semibold'>
                {lodash.startCase(label.toString().toLowerCase())}
            </Typography>
            {allowEditSwimLanes &&
                <div className={styles.Actions}>
                {onRemove ? <Remove onClick={onRemove} /> : undefined}
                <Handle {...handleProps} />
                </div>
            }
          </div>
        ) : null}
        <div className='bg-gray-100 dark:bg-gray-500 rounded-b-lg p-2'>
            {placeholder ? children : <ul>{children}</ul>}
        </div>
      </Component>
    );
  }
);