import { UniqueIdentifier } from "@dnd-kit/core";


export type BaseItem = {
    id: UniqueIdentifier;
    order: number
}