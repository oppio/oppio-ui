import React, {useEffect, ForwardRefRenderFunction, ForwardedRef} from 'react';
import type {DraggableSyntheticListeners} from '@dnd-kit/core';
import type {Transform} from '@dnd-kit/utilities';
import { classNames } from '../../server-side';
import { BaseItem } from './types';
const Handle = ({...props}) => {
    return <div>
        {`[]`}
    </div>
}

const Remove = ({...props}) => {
    return <div>
        {`X`}
    </div>
}





export interface ItemProps<T extends BaseItem> {
  dragOverlay?: boolean;
  color?: string;
  disabled?: boolean;
  dragging?: boolean;
  handle?: boolean;
  handleProps?: any;
  height?: number;
  index?: number;
  fadeIn?: boolean;
  transform?: Transform | null;
  listeners?: DraggableSyntheticListeners;
  sorting?: boolean;
  style?: React.CSSProperties;
  transition?: string | null;
  wrapperStyle?: React.CSSProperties;
  value: React.ReactNode;
  item: T,
  ref: ForwardedRef<HTMLLIElement>;
  onRemove?(): void;
}

type DProps<T extends BaseItem> = Parameters<ForwardRefRenderFunction<HTMLLIElement, ItemProps<T>>>

function ItemCMP<T extends BaseItem>(
    {
      color,
      dragOverlay,
      dragging,
      disabled,
      fadeIn,
      handle,
      handleProps,
      height,
      index,
      listeners,
      onRemove,
      renderItem,
      sorting,
      style,
      transition,
      transform,
      value,
      wrapperStyle,
      ...props
    }: Omit<DProps<T>[0], "ref"> & {renderItem(props: ItemProps<T>): React.ReactElement;},
    ref: DProps<T>[1]
  )  {
    useEffect(() => {
      if (!dragOverlay) {
        return;
      }

      document.body.style.cursor = 'grabbing';

      return () => {
        document.body.style.cursor = '';
      };
    }, [dragOverlay]);

    const styles = {
      Wrapper: 'KanbanItem list-none p-1',
      Item: 'KanbanItemContent',
      dragging: 'list-none p-1 rounded-md',
      dragOverlay: 'list-none p-1 rounded-md',
      disabled: 'KanbanItem--disabled',
      fadeIn: 'KanbanItem--fadeIn',
      withHandle: 'KanbanItem--withHandle',
      color: 'KanbanItem--color',
      Actions: 'KanbanItemActions',
      Remove: 'KanbanItemRemove',
      sorting: 'KanbanItem--sorting',

    }

    return <li
        className={classNames(
          styles.Wrapper,
          fadeIn && styles.fadeIn,
          sorting && styles.sorting,
          dragOverlay && styles.dragOverlay,
        )}

      >
        <div
          className={classNames(
            styles.Item,
            dragging && styles.dragging,
            handle && styles.withHandle,
            dragOverlay && styles.dragOverlay,
            disabled && styles.disabled,
            color && styles.color
          )}
          style={style}
          data-cypress="draggable-item"
          tabIndex={!handle ? 0 : undefined}
        >
          {renderItem ? renderItem({
            color,
            disabled,
            dragging,
            fadeIn,
            handle,
            handleProps,
            height,
            index,
            listeners,
            onRemove,
            sorting,
            style,
            transition,
            transform,
            value,
            item: props.item,
            ref
          }) : value}
          <span className={styles.Actions}>
            {onRemove ? (
              <Remove className={styles.Remove} onClick={onRemove} />
            ) : null}
            {handle ? <Handle {...handleProps} {...listeners} /> : null}
          </span>
        </div>
      </li>
    
  }

const ForwardedRefCMP = React.forwardRef(ItemCMP)

export const Item = React.memo(ForwardedRefCMP) as typeof ForwardedRefCMP;