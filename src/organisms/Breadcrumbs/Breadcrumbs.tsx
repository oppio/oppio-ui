import { ButtonColor } from "../../atoms/Button";
import { Link, LinkProps } from "../../atoms/Link";

import { HomeIcon } from "@heroicons/react/24/solid";
import { classNames } from "../../server-side"


export type BreadcrumbsProps = {
    ariaLabel: string
    className?: string
    fullWidth?: boolean
    root: {
        name: string
        href: string
        current: boolean
        onClick?: () => void
    },
    links: {
        name: string
        href: string
        current: boolean
        onClick?: () => void
        disabled?: boolean
    }[],
    as?: "button" | LinkProps<Record<string, unknown>>["as"]
    
}

export function Breadcrumbs({ links, as = "a", root, className, ariaLabel, fullWidth }: BreadcrumbsProps) {



    return <nav className={classNames("flex", className)} aria-label={ariaLabel}>
        <ol role="list" className={classNames("flex space-x-4 rounded-md bg-white dark:bg-gray-800 px-6 shadow", fullWidth ? "w-full" : undefined)}>
            <li className="flex">
                <div className="flex items-center">
                    {as !== "button" ?
                    <Link as={as} color={ButtonColor.NONE} href={root.href} className="text-gray-400 hover:text-gray-500">
                        <HomeIcon className={classNames("h-5 w-5 flex-shrink-0", root.current ? "text-primary-500" : "")} aria-hidden="true" />
                        <span className="sr-only">{root.name}</span>
                    </Link>
                    :
                    <button onClick={root.onClick} className="text-gray-400 hover:text-gray-500">
                        <HomeIcon className={classNames("h-5 w-5 flex-shrink-0", root.current ? "text-primary-500" : "")} aria-hidden="true" />
                        <span className="sr-only">{root.name}</span>
                    </button>
                    }
                </div>
            </li>
            {links.map((page) => (
                <li key={page.name} className="flex">
                    <div className="flex items-center">
                        <svg
                            className="h-full w-6 flex-shrink-0 text-gray-200 dark:text-gray-600"
                            viewBox="0 0 24 44"
                            preserveAspectRatio="none"
                            fill="currentColor"
                            aria-hidden="true"
                        >
                            <path d="M.293 0l22 22-22 22h1.414l22-22-22-22H.293z" />
                        </svg>
                        {as !== "button" ?
                            <Link
                                as={as}
                                color={ButtonColor.NONE}
                                href={page.href}
                                className={classNames("ml-4 text-sm font-medium", page.current ? "text-primary-500 hover:text-primary-700 hover:dark:text-primary-200 font-semibold" : (page.disabled ? "text-gray-300 cursor-not-allowed" : "text-gray-500 hover:text-gray-700"))}
                                aria-current={page.current ? 'page' : undefined}
                            >
                                {page.name}
                            </Link>
                            :
                            <button
                                onClick={() => page.disabled ? undefined : page.onClick && page.onClick()}
                                className={classNames("ml-4 text-sm font-medium", page.current ? "text-primary-500 hover:text-primary-700 hover:dark:text-primary-200 font-semibold" : (page.disabled ? "text-gray-300 cursor-not-allowed" : "text-gray-500 hover:text-gray-700"))}
                                aria-current={page.current ? 'page' : undefined}
                            >
                                {page.name}
                            </button>
                        }
                    </div>
                </li>
            ))}
        </ol>
    </nav>
}