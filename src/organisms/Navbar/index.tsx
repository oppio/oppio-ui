import { Transition, TransitionChild } from "@headlessui/react"
import { Fragment, ReactElement } from "react"
import {
  Bars3Icon,
  XMarkIcon,
} from '@heroicons/react/24/outline'
import { Breakpoint } from "../../hooks"
import { NavbarSize } from "./types"
import { NavbarItem, NavbarItemProps } from "./NavbarItem"
import { isMobile } from "../../utils"
import { classNames } from "../../server-side"

export type NavbarProps = {
  navbarSize: NavbarSize
  onMenuItemClick: (value?: string) => void
  setNavbarSize: (navbarSize: NavbarSize) => void
  breakpoint: Breakpoint
  showNavbar: boolean
  menuItems: Omit<NavbarItemProps, "navbarSize">[]
  logo?: ReactElement
  activeIndex: number
  setActiveIndex: (index: number) => void
  footer?: ReactElement
}



export function Navbar({
  navbarSize, onMenuItemClick, setNavbarSize, breakpoint,
  showNavbar, menuItems, logo, activeIndex, setActiveIndex,
  footer
}: NavbarProps) {


  return <>
  <div>
    
    <Transition show={showNavbar} as={Fragment}>
      <div>

        <TransitionChild
          as={Fragment}
          enter="transition-opacity ease-linear duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition-opacity ease-linear duration-300"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div onClick={() => {setNavbarSize(NavbarSize.HIDDEN)}} className="bg-gray-900/80 lg:hidden fixed top-0 w-screen h-dvh z-40">
          <div className="z-20 absolute right-0 top-0 flex justify-center lg:hidden">
                <button type="button" className="-m-2.5 p-2.5" onClick={() => setNavbarSize(NavbarSize.HIDDEN)}>
                  <span className="sr-only">Close sidebar</span>
                  <XMarkIcon className="h-6 w-6 text-white" aria-hidden="true" />
                </button>
              </div>
          </div>
        </TransitionChild>
        <TransitionChild
            as={Fragment}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
          >
        <div className="absolute w-[275px] h-dvh top-0 left-0 flex z-40">
            <div className="relative flex w-full max-w-[275px] flex-1">
              <div className="flex grow flex-col gap-y-5 overflow-y-auto bg-primary-600 dark:bg-gray-800 px-6 pb-4">
                <div className="flex h-16 shrink-0 items-center">
                  {logo ? logo : <img
                    className="h-8 w-auto"
                    src="https://tailwindui.com/img/logos/mark.svg?color=white"
                    alt="Your Company"
                  />}
                </div>
                <nav className="flex flex-1 flex-col">
                  <ul role="list" className="flex flex-1 flex-col gap-y-7">
                    <div className="flex flex-col gap-2 relative">
                      <div className={classNames("bg-primary-700 dark:bg-gray-700 w-full h-10 rounded-md absolute  z-0 transition-transform")} style={{transform: `translateY(${Math.max(activeIndex, 0) * 48}px)`}} />
                      {menuItems.map((props, idx) => (
                        <NavbarItem
                          {...props}
                          key={props.href}
                          active={activeIndex === idx}
                          onClick={(href?: string) => {
                            setActiveIndex(idx)
                            onMenuItemClick(href)
                            isMobile(breakpoint) ? setNavbarSize(NavbarSize.HIDDEN) : undefined
                          }}
                          navbarSize={navbarSize}
                        />
                      ))}
                    </div>
                  </ul>
                </nav>
                {footer && <div className="flex shrink-0 items-center">
                  {footer}
                </div>}
              </div>
            </div>
        </div>
        </TransitionChild>
      </div>
    </Transition>
  </div></>
}
