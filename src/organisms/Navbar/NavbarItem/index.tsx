import { classNames } from "../../../server-side"
import { NavbarSize } from "../types"
import {Link, LinkProps} from "../../../atoms/Link"



export type NavbarItemProps = {
  onClick?: (href?: string) => void
  href?: string
  navbarSize: NavbarSize
  active: boolean
  label: string
  Icon: React.FC<{className?: string}>
  as?: LinkProps<Record<string, any>>["as"]
}

export const NavbarItem = ({active, onClick, as = "a", href, navbarSize, label, Icon, ...props}: NavbarItemProps ) => {

  return <Link
    as={as}
    onClick={() => {
      onClick && onClick(href)
    }}
    href={href || "/"}
    className={classNames(
      active && "text-white",
      'bg-transparent text-primary-200 dark:text-gray-400 hover:text-white hover:bg-primary-700/10 dark:hover:bg-gray-700/10',
      'group flex gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold z-10 relative',
    )}
    {...props}
  >
    <>
    <Icon
      className={classNames(
        'text-primary-200 dark:text-current group-hover:text-white',
        'h-6 w-6 shrink-0'
      )}
      aria-hidden="true"
    />
    {label}
    </>
  </Link>

}
