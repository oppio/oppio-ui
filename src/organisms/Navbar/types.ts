export enum NavbarSize {
    LARGE = "LARGE",
    SMALL = "SMALL",
    HIDDEN = "HIDDEN"
  }
  