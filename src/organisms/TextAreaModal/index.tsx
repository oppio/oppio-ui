import { forwardRef, ForwardedRef, useState, RefObject } from "react";
import { Typography } from "../../atoms/Typography";
import { } from "react-aria-components";
import { Modal } from "../../molecules/Modal";
import { TextAreaField, TextAreaFieldProps } from "../../atoms/TextAreaField";

export type TextAreaModalProps = TextAreaFieldProps & {
    textAreaRef?: RefObject<HTMLTextAreaElement>
}

export const TextAreaModal = forwardRef<HTMLTextAreaElement, TextAreaModalProps>(({textAreaRef, ...props}: TextAreaModalProps, ref) => {
    const [open, setOpen] = useState(false)
    return <>
    <Typography onClick={() => {setOpen(true)}} className={`cursor-pointer italic text-sm truncate overflow-hidden ${props.value ? 'text-primary-600 dark:text-primary-400' : 'text-gray-600 dark:text-gray-400'}`}>
        <>{props.value || props.placeholder}</>
    </Typography>
    <Modal opened={open} setOpened={() => setOpen(false)}>
        <TextAreaField
            {...props}
            ref={ref}
        />
    </Modal>
    </>
})