import {  useMemo, useState,  FormEvent, ForwardedRef, forwardRef, useEffect,  ReactElement, useCallback } from "react";

import _ from "lodash";

import { ChevronUpDownIcon } from "@heroicons/react/24/outline";

import { ExclamationCircleIcon } from "@heroicons/react/20/solid";
import { Modal } from "../../molecules/Modal";
import { InputField } from "../../atoms/InputField";



export type Option<T> = { value: T, label:  (args: {selected: boolean}) => JSX.Element, stringLabel?: string, search: string }




export type ModalSelectProps<T> = ({
  value?: T | null
  onChange: (option: T | null) => void
  isMulti?: false
} | {
  value?: T[] | null
  onChange: (option: T[]) => void
  isMulti: true
}) & {
  options?: Option<T>[]
  label?: string
  className?: string
  openOnHover?: boolean
  onTextInputChange?: (value: string | null) => void
  error?: string
  disabled?: boolean
  hideLabel?: boolean
  useFixed?: boolean
  modalClassName?: string
}


function ModalSelectCMP<T extends string | number>({ 
  value, onChange, isMulti,  options = [], label, error,
  disabled = false, hideLabel = false, className, useFixed = true,
  modalClassName
}: ModalSelectProps<T>, ref: ForwardedRef<HTMLInputElement>) {
  const [optionsRef, setOptionsRef] = useState<HTMLDivElement | null>(null)
  const [inputRef, setInputRef] = useState<HTMLElement | null>(null)
  const [open, setOpen] = useState(false)
  const [filteredOptions, setFilteredOptions] = useState(options)
  const [query, setQuery] = useState("")

  const optionMap = useMemo(() => Object.fromEntries<Option<T>>(options.map((_opt) => [typeof _opt.value === "string" ? _opt.value : JSON.stringify(_opt.value), _opt])), [options])
  
  const handleOnChange = (args: Option<T>) => {
    if (isMulti) {
      if (value?.includes(args.value)) {
        onChange(value.filter((v) => v !== args.value))
      } else {
        onChange([...(value || []), args.value])
      }
      
    } else if (!Array.isArray(args)) {
      if (args.value === value) {
        onChange(null)
      } else {
        onChange(args.value)
        setOpen(false)
      }
    }
  }

  const handleOnBlur = (e: FormEvent<HTMLInputElement>) => {
    if (e.currentTarget.value && optionMap[e.currentTarget.value]) {
      if (isMulti) {
        onChange([e.currentTarget.value] as T[])
      } else {
        onChange(e.currentTarget.value as T)
        setOpen(false)
      }
    }
  }

  useEffect(() => {
    const onScroll = () => {
      const inputStyle = inputRef?.getBoundingClientRect() ?? null
      if (optionsRef) {
        optionsRef.children[0]?.setAttribute("style", `width: ${inputStyle?.width}px; top: ${inputStyle?.bottom}px; left: ${inputStyle?.left}px;`)
      }
    }
    if (open && useFixed) {
      
      document?.addEventListener("scroll", onScroll, true)
    }
    
      
    
    return () => {
      if (open && useFixed) {
        document?.removeEventListener("scroll", onScroll)
      }
    }
  }, [inputRef, open, optionsRef])
  
  const getStringRep = (val: T) => {
    return optionMap[val]?.stringLabel || optionMap[val]?.search || ""
  }

  const filterOptionsCallback = useCallback(_.debounce((query: string) => {
    setFilteredOptions(options.filter((_opt) => _opt.search.toLowerCase().includes(query.toLowerCase())))
  }, 500), [options])

  const inputValue = isMulti ? value?.map(v => (v ? getStringRep(v) : "")).join(", ") : (value ? getStringRep(value) : "")

  return (<div className={className}>
      <div className="relative opacity-100">
        <div ref={setInputRef} className="relative" >
          <InputField
            ref={ref}
            onBlur={handleOnBlur}
            hideLabel={hideLabel}
            label={label || ""}
            onClick={() => !disabled && setOpen(true)}
            value={inputValue}
            readOnly
            disabled={disabled}
            trailingIcon={
              <span className="flex">
                {error &&
  
                    <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
  
                }
                <ChevronUpDownIcon
                  className="h-5 w-5 text-gray-400"
                  aria-hidden="true"
                  onClick={() => setOpen(!open)}
                />
              </span>
           }
          />
          
          <Modal opened={open} setOpened={() => setOpen(false)} modalContainerClassName={modalClassName}>
            <div>
              <InputField
                value={query}
                onChange={(e) => {
                  setQuery(e.currentTarget.value)
                  filterOptionsCallback(e.currentTarget.value)
                }}
                placeholder="Search"
                label="Search Options"

              />
            </div>
            <div 
              className="grid grid-cols-1 md:grid-cols-3 gap-2 pt-2 mt-4"
            >
              {filteredOptions.map((_opt) => {
                const selected = isMulti ? (value || []).includes(_opt.value) : value === _opt.value
                return <div className="cursor-pointer" onClick={() => handleOnChange(_opt)} key={_opt.search}>
                {_opt.label({selected})}
              </div>})}
            </div>
          </Modal>
          
        </div>
        { error &&
            <p className="mt-2 text-sm text-red-600 text-left" id={`${name}-error`}>
            {error}
            </p>
          }
        
      </div>
    
    </div>)
}

export const ModalSelect = forwardRef(ModalSelectCMP) as typeof ModalSelectCMP
