import { useEffect, createContext, useContext, useCallback, useState } from "react"



export enum Breakpoint {
  NONE = "",
  XS = "xs",
  SM = "sm",
  MD = "md",
  LG = "lg",
  XL = "xl",
  XXL = "xxl"
}

export const mobileBreakpoints = [Breakpoint.XS, Breakpoint.SM]


export const tabletBreakpoints = [Breakpoint.MD, Breakpoint.LG]

export function useBreakpoint(
  breakpoints: { [key: string]: string },
  target?: HTMLElement,
): {width: number | null, height: number | null, breakpoint: Breakpoint} {

  const [breakpoint, setBreakpoint] = useState<Breakpoint>(Breakpoint.XL);
  const [screenSize, setScreenSize] = useState<{width: null | number, height: null | number}>({width:null, height: null})

  if (!target && typeof document !== "undefined" && document.firstElementChild) {
    target = document.firstElementChild as HTMLElement
  }

  const resizeCallback: ResizeObserverCallback = useCallback((entries) => {
    const entryTarget = entries[0].target as HTMLElement;
    const pixelValue = entryTarget.offsetWidth
    const newHeight = entryTarget.clientHeight
    const newWidth = entryTarget.clientWidth
    const breakpointValues = Object.entries(breakpoints || {});
    let newBreak = breakpoint;

    for (let i = 0; i < breakpointValues.length; i++) {
      if (pixelValue < parseInt(breakpointValues[i][1], 10)) {
        newBreak = breakpointValues[i][0] as Breakpoint;
        break
      }
    }
    
    if (!newBreak) {
      newBreak = breakpoint;
    }
    
    if (newBreak !== breakpoint) {
      setBreakpoint(newBreak);
    }
    

    if (newWidth !== screenSize.width || newHeight !== screenSize.height) {
      setScreenSize({width: newWidth, height: newHeight})
    }

  }, [breakpoint, breakpoints, screenSize.height, screenSize.width]);

  

  useEffect(() => {
    const localObserver = new ResizeObserver(resizeCallback); 
    if (target){
      localObserver.observe(target);
    }
    return () => {
      localObserver.disconnect()
    }
  }, [resizeCallback, target])

  return {...screenSize, breakpoint}
}



export const ResizeContext = createContext<{width: number | null, height: number | null}>({width: null, height: null})


export const BreakpointProvider = ({children, action}: {children: React.ReactElement | React.ReactNode, action?: (breakpoint: Breakpoint) => void}) => {
  

  const {breakpoint, width, height} = useBreakpoint({
    "sm": "640px",
    "md": "768px",
    "lg": "1024px",
    "xl": "1280px",
    "xxl": "1536px"
  })

  useEffect(() => {
    action && action(breakpoint)
  }, [breakpoint, action])
  
  return <ResizeContext.Provider value={{width, height}}>
    {children}
  </ResizeContext.Provider>
}

export const useResizeObserver = () => {
  return useContext(ResizeContext)
}
