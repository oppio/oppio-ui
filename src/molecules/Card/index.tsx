import { classNames } from "../../server-side"
import React, { ForwardedRef, HTMLProps, ReactElement, forwardRef } from "react"

export type CardProps = {
  header?: ReactElement | ReactElement[]
  footer?: ReactElement | ReactElement[]
  children: HTMLProps<HTMLDivElement>["children"]
  bodyClassName?: string
  dividers?: boolean
  className?: string
  onClick?: HTMLProps<HTMLDivElement>["onClick"]
  style?: HTMLProps<HTMLDivElement>["style"]
}

function CardCMP({onClick, children, bodyClassName = "", header, footer, dividers = false, className, ...props}: CardProps, ref: ForwardedRef<HTMLElement>){
  return <div {...props} className={classNames(
    dividers ? "divide-y divide-gray-200" : "" ,
    "overflow-hidden rounded-lg bg-white dark:bg-gray-800 dark:ring-2 p-2",
    className
  )} onClick={onClick}>
    {header &&
      <div className="px-4 py-5 sm:px-6">
        {header}
      </div>
    }
    <div className={classNames(`px-4 py-2 sm:p-6`, bodyClassName)}>
      {children}
    </div>
    {footer &&
      <div className="px-4 py-5 sm:px-6">
        {footer}
      </div>
    }
</div>
}

export const Card = forwardRef(CardCMP)
