import { Fragment, MutableRefObject, ReactElement } from "react"
import { Dialog, DialogPanel, Transition, TransitionChild } from "@headlessui/react"
import { classNames } from "../../server-side"
import { XMarkIcon } from "@heroicons/react/20/solid"
import { Typography } from "../../atoms/Typography"



export type ModalProps = {
  setOpened?: (value: boolean) => void
  opened: boolean
  initialFocus?: MutableRefObject<HTMLElement | null> | undefined
  children: ReactElement | ReactElement[] | boolean
  title?: string | ReactElement
  fullscreen?: boolean
  showBackdrop?: boolean
  modalContainerClassName?: string
}


export function Modal({ modalContainerClassName, opened, setOpened, initialFocus, children, fullscreen = false, showBackdrop = true, title, ...props }: ModalProps) {
  return <Transition show={opened} as={Fragment}>
    <Dialog as="div" className="relative z-10" initialFocus={initialFocus} onClose={() => null}>
      {showBackdrop &&
        <TransitionChild
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </TransitionChild>
      }

      <div className={classNames("fixed inset-0 z-10 w-screen overflow-y-auto", modalContainerClassName)}>
        <div className="flex min-h-full items-center justify-center text-center items-center">
          <TransitionChild
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <DialogPanel className={classNames("relative overlow-y-visible bg-white dark:bg-gray-800 px-4 py-4 text-left shadow-xl transition-all sm:w-full sm:p-6", fullscreen ? "w-screen h-dvh z-60 lg:!pl-[299px]" : "sm:max-w-4xl sm:my-8 rounded-lg")}>
              <>
                <Typography className="w-full dark:text-white">{title ? title : ""}</Typography>
                <div className="w-full flex justify-end absolute right-1 top-1">
                  <XMarkIcon className="w-6 w-6 text-gray-700 dark:text-white cursor-pointer" onClick={setOpened ? () => setOpened(false) : undefined} />
                </div>
              </>
              {children}
            </DialogPanel>
          </TransitionChild>
        </div>
      </div>
    </Dialog>
  </Transition>
}