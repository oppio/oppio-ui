import { classNames } from "../../server-side"
import { ReactElement } from "react"

export enum BadgeWrapperColor {
    GREY = "GREY",
    GREEN = "GREEN",
    RED = "RED"
}

export type BadgeWrapperProps = {
    children: ReactElement
    color?: BadgeWrapperColor
    badge?: boolean | ReactElement
    bottom? : boolean
}


const indicatorStyle = (hasBadge: boolean, color: BadgeWrapperColor, bottom: boolean) => {
    let style = "absolute right-0  block rounded-full"
    if (!hasBadge) {
        switch (color) {
            case BadgeWrapperColor.GREY:
                style += " bg-gray-300"
                break
            case BadgeWrapperColor.GREEN:
                style += " bg-green-300"
                break
            case BadgeWrapperColor.RED:
                style += " bg-red-300"
                break
        }
    }
    return classNames(style, bottom ? "bottom-0" : "top-0", hasBadge ? "" : "h-1.5 w-1.5")
}

export function BadgeWrapper({children, color = BadgeWrapperColor.GREY, badge, bottom = false}: BadgeWrapperProps) {
    return <span className="relative inline-block">
        {children}
        {(badge && typeof badge !== "boolean") &&
        <span 
            className={indicatorStyle(!!badge, color, bottom)}
        >
            
                <span className="absolute translate-x-[-50%] translate-y-[-50%]">
                    {badge}        
                </span>
            
            
        </span>
        }
    </span>
}