import { DateTime } from 'luxon'
import {InputField, InputFieldProps } from '../InputField'
import {Popover, PopoverProps } from '../Popover'
import {DateSelector, DateSelectorProps} from '../DateSelector'
import { monthDayYear } from '../../utils'
import { CalendarDaysIcon } from '@heroicons/react/24/outline'
import { Button, ButtonColor } from 'atoms/Button'

const buttonOptions = () => {



  return [
  {
    label: 'Last Week',
    start: DateTime.local()
    .minus({ weeks: 1 })
    .set({ weekday: 1 }),
    end: DateTime.local()
    .minus({ weeks: 1 })
    .set({ weekday: 7 })
  },
  {
    label: 'Last Month',
    start: DateTime.local()
    .minus({ months: 1 })
    .startOf('month'),
    end: DateTime.local()
    .minus({ months: 1 })
    .endOf('month'),
  },
  {
    label: 'Last Quarter',
    start: DateTime.local().minus({ months: 3 }).startOf('quarter'),
    end: DateTime.local().minus({ months: 3 }).endOf('quarter'),
  },
  {
    label: 'Last Year',
    start: DateTime.local().minus({ years: 1 }).startOf('year'),
    end: DateTime.local().minus({ years: 1 }).endOf('year'),
  },
  {
    label: 'This Week',
    start: DateTime.local().set({ weekday: 1 }),
    end: DateTime.local().set({ weekday: 7 }),
  },
  {
    label: 'This Month',
    start: DateTime.local().startOf('month'),
    end: DateTime.local().endOf('month'),
  },
  {
    label: 'This Quarter',
    start: DateTime.local().startOf('quarter'),
    end: DateTime.local().endOf('quarter'),
  },
  {
    label: 'This Year',
    start: DateTime.local().startOf('year'),
    end: DateTime.local().endOf('year'),
  },
]
}

const DateRangePopper = ({
  endYear,
  ...props
}: DateSelectorProps & {rangeSelector: true}) =><div className='flex'>
  <div className='flex flex-col gap-1 mx-2 mb-2 justify-end hidden md:flex'>
    {buttonOptions().map(btn => {
      return <Button 
        key={btn.label}
        color={ButtonColor.SOFT}
        className='text-nowrap'
        onClick={() => {
          props.onChange && props.onChange([btn.start, btn.end])
        }}
      >
        {btn.label}
      </Button>
    })}
  </div>
  <DateSelector
    {...props}
    className="min-w-[275px]"
    endYear={endYear}
   />
</div>

export type DateRangeFieldProps = {
  value?: [DateTime | undefined, DateTime | undefined]
  onChange?: (value: [DateTime, DateTime]) => void
  rangeSelector?: true
  startYear?: number
  endYear?: number
  selectTime?: boolean
  numberOfMonths?: number
} & Pick<InputFieldProps, "label" | "error" | "name" | "className"> & Pick<PopoverProps, "placement">

export function DateRangeField({ label, placement, error, name, className, endYear, ...props }: DateRangeFieldProps) {

  return <Popover
  className={className}
    placement={placement}
    popoverContent={({open, close}) =><DateRangePopper
    {...props}
    rangeSelector={true}
    className="min-w-[275px]"
    endYear={endYear}
    onChange={(date: [DateTime | undefined, DateTime | undefined]) => {
      props.onChange && props.onChange(date as DateTime & [DateTime, DateTime])
    }}
  />}
  >
    {({ close }) => {
      return <InputField
        label={label}
        value={Array.isArray(props.value) ? `${props.value[0] ? monthDayYear(props.value[0]): ""}- ${props.value[1] ? monthDayYear(props.value[1]) : ""}` : (props.value ? monthDayYear(props.value) : undefined)}
        trailingIcon={<CalendarDaysIcon className='w-4 h-4 dark:text-white'/>}
        error={error}
        name={name}
        onChange={() => null}
        className={className}
      />
    }}

  </Popover>
}