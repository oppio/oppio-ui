import { HTMLProps, ReactElement } from "react"
import React, {  ForwardedRef,  } from "react";
import { ExclamationCircleIcon } from '@heroicons/react/20/solid'
import { classNames } from "../../server-side";




export type InputFieldProps<T extends (string | number) = string> = HTMLProps<HTMLInputElement> & {
  label: string
  hideLabel?: boolean
  error?: string
  media?: ReactElement
  helpText?: string
  trailingIcon?: ReactElement
  leadingIcon?: ReactElement
  inputClassName?: string
}


function _InputField<T extends (string | number) = string>({ 
  label, error, media, name, helpText, hideLabel, trailingIcon, leadingIcon, className, inputClassName,  ...props 
}: InputFieldProps<T>, ref: ForwardedRef<HTMLInputElement>) {

  return (
    <div className={className}>
      <label htmlFor={name} className={hideLabel ? "sr-only" : "block text-sm font-medium leading-6 text-gray-900 dark:text-white text-left"}>
        {label}
      </label>
      <div className={`${(error || trailingIcon || leadingIcon) ? " relative rounded-md shadow-sm": ""}`}>
        {leadingIcon &&
          <div className={`pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3`}>
            {leadingIcon}
          </div>
        }
        <input
          {...props}
          ref={ref}
          name={name}
          className={classNames(`bg-white px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-primary-600 dark:bg-gray-700 dark:text-white disabled:cursor-not-allowed disabled:bg-gray-50 disabled:text-gray-500 disabled:ring-gray-200 sm:text-sm sm:leading-6 ${(trailingIcon || error) ? "pr-10" : ((trailingIcon && error) ? "pr-20" : "")} ${(leadingIcon) ? "pl-10" : ""}`, inputClassName)}
        />
        {trailingIcon &&
          <div className={`pointer-events-none absolute inset-y-0 ${(trailingIcon && error) ? "right-4" : "right-0"} flex items-center pr-3`}>
            {trailingIcon}
          </div>
        }
        {error &&
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
            <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
          </div>
        }
      </div>
      {(helpText && !error) &&
        <p className="mt-2 text-sm text-gray-500" id={`${name}-description`}>
          {helpText}
        </p>
      }
      { error &&
        <p className="mt-2 text-sm text-red-600 text-left" id={`${name}-error`}>
         {error}
        </p>
      }
    </div>
  )
}

export const InputField = React.forwardRef(_InputField)
