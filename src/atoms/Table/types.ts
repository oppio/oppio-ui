
import { ReactElement, MouseEvent, CSSProperties } from "react";
import { FieldValues, UseFormReturn } from "react-hook-form";
import { TableProps as AriaTableProps } from "react-aria-components";
import { ModalProps } from "molecules/Modal";
import { SelectFieldProps } from "atoms/SelectField";
import { LongPressOptions } from "@uidotdev/usehooks";

type BaseTableRecord = object

type FilterModifier = "eq" | "gte" | "lte" | "contains" | "in"

export enum FilterWidget {
  InputField,
  DateField,
  SelectField,
  SwitchField
}

type Filter<T> = {
  topology: "or" | "and"
  field: string
  modifier: FilterModifier
  value: T[keyof T] | undefined
}

export type Sort<T extends BaseTableRecord> = {
  direction?: "asc" | "desc",
  sortHandler?: (c: T) => unknown,
  order?: number
}

type TableState<T extends BaseTableRecord> = {
    rows: T[],
    filterModalOpen?: string
    selectedRows?: string[]
    sortState?: Partial<{
      [key: string]: Sort<T>
    }>,
    filterConfig?: {
      [key: string]: {
        widget: FilterWidget
        modifiers: FilterModifier[]
        choices?: SelectFieldProps<string | number>["options"]
      }
    }
    hiddenColumns?: string[]
    columns: TableHeader<T>[]
    filterState?: Filter<T>[]
    pagination: {
      pageSize: number
      page?: number
      totalCount: number
      serverSide?: boolean
    }
    onFilterStateChange?: (data: TableState<T>["filterState"]) => void
    onSortStateChange?: (data: TableState<T>["sortState"]) => void
    onSelect?: (row: T | T[]) => void
    headerStyle?: string
    dragOverId?: string
  } & FieldValues

export type RenderCellProps<T extends BaseTableRecord> = {rowIndex: number, row: T, label: string, onSelect?: (row: T | T[]) => void, form: UseFormReturn<TableState<T>>}
export type GetValuesProps<T extends BaseTableRecord> = {row: T, label: string, rowIndex: number}

type TableHeader<T extends BaseTableRecord> = {
    label: string,
    hidden?: {
      xs?: boolean
      sm?: boolean
      md?: boolean
      lg?: boolean
      xl?: boolean
    },
    hideLabel?: boolean
    visible?: boolean
    minWidth?: number
    maxWidth?: number
    width?: number
    hideDropdown?: boolean
    sortable?: boolean
    getValue?: (props: GetValuesProps<T>) => string | boolean | number | undefined | null
    renderCell: (props: RenderCellProps<T>) => ReactElement
  }

  export type PresentationRowProps<T extends BaseTableRecord> = {
    columns: TableHeader<T>[]
    selectionsEnabled: boolean
    onSelect: TableState<T>["onSelect"]
    isSelected: boolean,
    form: UseFormReturn<TableState<T>, any, undefined>
    index: number
    row?: T
    spanRef?: (element: HTMLElement | null) => void
    className?: string
    style?: CSSProperties
    onRowClick?: (e: MouseEvent) => void
    onRowDoubleClick: TableProps<T>["onRowDoubleClick"],
    handleLongRowPress?: (e: Event, row: T) => void
    rowLongPressOptions?: LongPressOptions
    isDragging?: boolean
  }

type TableProps<T extends BaseTableRecord> = {
    ariaLabel: string
    columns: TableHeader<T>[]
    components?: { tableToolbar?:  ({form}: {form: UseFormReturn<TableState<T>>}) => ReactElement }
    endOfTable?: ReactElement
    filterModalOpen?: string
    filterModalProps?: ModalProps
    getDndBehavior?: (row: T) => ("draggable" | "droppable")[]
    getRowId: (row: T) => string
    handleLongRowPress?: PresentationRowProps<T>["handleLongRowPress"]
    headerStyle?: string
    hiddenColumns?: string[]
    nextPage?: () => void
    onDragEnd?: (result: {source: string[], destination: string}) => void
    onFilterStateChange?: (data: TableState<T>["filterState"]) => void
    onRowClick?: (e: MouseEvent, row: T, form: UseFormReturn<TableState<T>>) => void
    onRowDoubleClick?: (e: MouseEvent, row: T, form: UseFormReturn<TableState<T>>) => void
    onScrollEnd?: () => void
    onSelect?: (row: T | T[], form: UseFormReturn<TableState<T>>) => void
    onSortStateChange?: (data: TableState<T>["sortState"]) => void
    paginationStyle?: string
    previousPage?: () => void
    rowClassName?: (row: T) => string | undefined
    rowHeight: ((rowIndex: number) => number)
    rowLongPressOptions?: PresentationRowProps<T>["rowLongPressOptions"]
    selectColumn?: boolean
    selectionMode: AriaTableProps["selectionMode"]
    setFilterModalOpen?: (value: string) => void
    showRemoveColumn?: boolean
    virtualize?: boolean
    DragComponent?: (props: {selectedProps: PresentationRowProps<T>[]}) => ReactElement | undefined | false
  } & TableState<T>;

type SortState<T extends BaseTableRecord> = TableState<T>["sortState"]
type FilterState<T extends BaseTableRecord> = TableState<T>["filterState"]
type FilterStateMap<T extends BaseTableRecord> = {[key: string]: Filter<T>[]} 

export type { 
  BaseTableRecord,
  FilterModifier,
  Filter,
  TableState,
  TableHeader,
  TableProps,
  SortState,
  FilterState,
  FilterStateMap
}
