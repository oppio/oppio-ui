import { Button, ButtonColor, ButtonProps } from "../../Button";
import { useFormContext, useWatch } from "react-hook-form";
import { CSVLink } from "react-csv";
import { CloudArrowDownIcon } from "@heroicons/react/24/outline";
import { BaseTableRecord, TableState } from "../types";

export type CSVExportProps = {
    filename: string
    leadingIcon?: React.ReactElement
    children?: string | React.ReactElement
    color? : ButtonProps["color"]
}


export function CSVExport<T extends BaseTableRecord>({
    filename,
    color = ButtonColor.SOFT,
    leadingIcon = <CloudArrowDownIcon className="w-6 h-6" />,
    children = "Download CSV"
}:  CSVExportProps) {

    const {watch, getValues} = useFormContext<TableState<T>>()
    const rows = watch("rows")
    const hiddenColumns = watch("hiddenColumns")
    const visibleColumns = getValues && getValues().columns?.filter((column) => !hiddenColumns?.includes(column.label) && !!column.getValue)
    const csvExportData = rows?.map((row, idx) => {
        return visibleColumns?.map((column) => column.getValue && column.getValue({row, label: column.label, rowIndex: idx})) || []
    }) || []
    return <CSVLink
    headers={visibleColumns?.map((column) => column.label)}
    filename={filename}
    data={csvExportData}
  ><Button leadingIcon={leadingIcon} color={color}>{children}</Button></CSVLink>
}

