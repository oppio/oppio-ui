import { Button, ButtonColor, ButtonProps } from "../../Button";
import { BaseTableRecord } from "../types";
import { PlusIcon } from "@heroicons/react/20/solid";
import { useFormContext } from "react-hook-form";

export type AddRowProps<T extends BaseTableRecord> = {
    leadingIcon?: React.ReactElement
    children?: string | React.ReactElement
    color? : ButtonProps["color"]
    addColumnTemplate: () => T
}


export function AddRow<T extends BaseTableRecord>({
    color = ButtonColor.SOFT,
    leadingIcon = <PlusIcon className="w-6 h-6" />,
    children = "Add Row",
    addColumnTemplate
}:  AddRowProps<T>) {
    const {getValues, setValue} = useFormContext()
    return <Button leadingIcon={leadingIcon} color={color} onClick={() => setValue("rows", [addColumnTemplate(), ...getValues("rows")], {shouldDirty: true})}>
        {children}
    </Button>
}

