import {memo, MouseEvent} from 'react'
import { EyeSlashIcon, FunnelIcon } from '@heroicons/react/24/outline';
import _ from 'lodash';
import { CSSProperties,  JSXElementConstructor, ReactElement, useCallback, useEffect, useMemo, useRef, useState } from 'react';

import { FormProvider, useForm, useFormContext, UseFormReturn } from 'react-hook-form';
import { classNames } from '../../server-side';
import { Badge, BadgeColor } from '../Badge';
import { ButtonColor } from '../Button';
import { DropMenu } from '../DropMenu';
import { Typography, TypographyElement } from '../Typography';
import { TableFilterModal } from './Filter/Filter';
import { StyledCell, StyledColumn } from './StyledTableComponents';
import { BaseTableRecord, Filter, PresentationRowProps, Sort, TableHeader as TableHeaderType, TableProps, TableState } from './types';
import { ListChildComponentProps, VariableSizeList } from 'react-window';

import AutoSizer from "react-virtualized-auto-sizer";
import { DragEndEvent, DragOverlay, useDndMonitor, useDraggable, useDroppable } from '@dnd-kit/core';
import { createPortal } from 'react-dom';
import { useLongPress } from "@uidotdev/usehooks";

function handleHiddenStyles<T extends BaseTableRecord>(hidden: TableHeaderType<T>["hidden"]) {
  let style = ""
  if (hidden?.xs === true) {
    style += "hidden"
  }
  if (hidden?.sm === true) {
    style += "hidden sm:table-cell"
  } else {
    style += ""
  }
  if (hidden?.md === true) {
    style += "hidden md:table-cell"
  } else {
    style += ""
  }
  if (hidden?.lg === true) {
    style += "hidden lg:table-cell"
  } else {
    style += ""
  }
  if (hidden?.xl === true) {
    style += "hidden xl:table-cell"
  } else {
    style += ""
  }
  return style
}

function Cell<T extends BaseTableRecord>({
  selectionsEnabled,
  onSelect,
  columnIndex,
  isSelected,
  row,
  form,
  rowIndex
}: {
  rowIndex: number
  selectionsEnabled: boolean
  onSelect: TableState<T>["onSelect"]
  columnIndex: number
  isSelected: boolean
  form: UseFormReturn<TableState<T>, any, undefined>
  row: T
}) {

  const col = form.watch(`columns.${columnIndex}`)
  const CellCMP = col.renderCell

  return <StyledCell
    className={classNames("select-none whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 dark:text-gray-100 truncate overflow-hidden")}
    style={{ minWidth: col.minWidth, width: col.width}}
  >
    <CellCMP
      rowIndex={rowIndex}
      row={row}
      label={col.label}
      onSelect={onSelect}
      form={form}
    />
  </StyledCell>
}



function PresentationRow<T extends BaseTableRecord>({
  columns,
  selectionsEnabled,
  onSelect,
  isSelected,
  form,
  index,
  row,
  spanRef,
  onRowClick,
  onRowDoubleClick,
  handleLongRowPress = () => null,
  rowLongPressOptions,
  ...props
}: PresentationRowProps<T>) {
  if (!row) return null


  const {onMouseDown, ...longPressProps} = useLongPress((e) => handleLongRowPress(e, row), rowLongPressOptions)

  const handleMouseDown = (e: MouseEvent) => {
    onMouseDown(e)
    onRowClick && onRowClick(e)
  }

  return <span
    ref={spanRef}
    {...props}
    {...longPressProps}
    onMouseDown={handleMouseDown}
    onDoubleClick={(e) => onRowDoubleClick && onRowDoubleClick(e, row, form)}
    className={classNames(props.className ? props.className : 'flex', isSelected ? "bg-primary-100 dark:bg-gray-600" : "")}
  >
    {columns.map((col, idx) => {
      return <Cell
        key={`${col.label}-${idx}`}
        columnIndex={idx}
        selectionsEnabled={selectionsEnabled}
        onSelect={onSelect}
        isSelected={isSelected}
        form={form}
        rowIndex={index}
        row={row}
      />
    })}
  </span>
}






function NonDNDRow<T extends BaseTableRecord>({
  index, style, data, form, onRowClick, onRowDoubleClick, isOver, isDragging
}: {
    isOver: boolean, onRowDoubleClick: TableProps<T>["onRowDoubleClick"] | undefined,
    onRowClick: (e: MouseEvent) => void, form: UseFormReturn<TableState<T>, any, undefined>
    isDragging: boolean
  } & ListChildComponentProps<{
    getDndBehavior: TableProps<T>["getDndBehavior"],
    getRowId: TableProps<T>["getRowId"],
    currentLength: number;
    endOfTable: ReactElement<any, string | JSXElementConstructor<any>> | undefined;
    onScrollEnd: ((data: T) => void) | undefined; draggable: boolean;
    handleLongRowPress?: PresentationRowProps<T>["handleLongRowPress"]
    rowLongPressOptions?: PresentationRowProps<T>["rowLongPressOptions"]
  }>) {

  const row = form?.watch(`rows.${index}`)

  const throttledOnScrollEnd = useCallback(_.throttle((row: T) => {
    return data?.onScrollEnd && data.onScrollEnd(row)
  }, 2000), [])

  if (!form) return null

  const columns = form.watch("columns")
  const selectionsEnabled = form.watch("selectionMode") !== "none"
  const onSelect = form.watch("onSelect")
  const isSelected = !!form.watch("selectedRows")?.includes((row as unknown as { id: string }).id)

  if (!row) return null

  if (index === (data?.currentLength ?? 0) - 1) {
    throttledOnScrollEnd(row)
  }


  const props = {
    style
  }
  if (data.endOfTable && index === (data.currentLength ?? 0) - 1) {
    return data.endOfTable
  }

  return <PresentationRow
    {...props}
    className={classNames('flex', isOver  ? "bg-primary-200 dark:bg-gray-700" : "")}
    columns={columns}
    selectionsEnabled={selectionsEnabled}
    onSelect={onSelect}
    isSelected={isSelected}
    form={form}
    index={index}
    row={row}
    onRowClick={onRowClick}
    onRowDoubleClick={onRowDoubleClick}
    rowLongPressOptions={data.rowLongPressOptions}
    handleLongRowPress={data.handleLongRowPress}
    isDragging={isDragging}
/>
}

function getItemsBetweenSelections(array: string[], newItem: string, currentSelection: string[]): string[] {
  // If there's no current selection, return array with just the new item
  if (!currentSelection || currentSelection.length === 0) {
      return [newItem];
  }

  // Find indices
  const newItemIndex = array.indexOf(newItem);
  const minSelectedIndex = Math.min(...currentSelection.map(item => array.indexOf(item)));
  const maxSelectedIndex = Math.max(...currentSelection.map(item => array.indexOf(item)));

  // Determine start and end indices for the range
  const startIndex = Math.min(newItemIndex, minSelectedIndex);
  const endIndex = Math.max(newItemIndex, maxSelectedIndex);

  // Return the slice of array between start and end indices (inclusive)
  return array.slice(startIndex, endIndex + 1);
}

function DndRow<T extends BaseTableRecord>({ index, style, data, form }: {
  form: UseFormReturn<TableState<T>, any, undefined>} & ListChildComponentProps<{ 
    getDndBehavior: TableProps<T>["getDndBehavior"],
    onRowClick?: TableProps<T>["onRowClick"]
    getRowId: TableProps<T>["getRowId"]
    currentLength: number;
    endOfTable: ReactElement<any, string | JSXElementConstructor<any>> | undefined;
    onScrollEnd: ((data: T) => void) | undefined;
    draggable: boolean;
    onRowDoubleClick: TableProps<T>["onRowDoubleClick"] | undefined
    handleLongRowPress?: PresentationRowProps<T>["handleLongRowPress"]
    rowLongPressOptions?: PresentationRowProps<T>["rowLongPressOptions"]
  }>) {
  const row = form.watch(`rows.${index}`)
  
  

  const {setNodeRef: droppableNodeRef, ...rest} = useDroppable({
    id: data.getRowId(row) ?? 0,
    data: row
  });

  const {attributes, listeners, setNodeRef: draggableNodeRef, transform, isDragging} = useDraggable({
    id: data.getRowId(row) ?? 0,
    data: row
  });
  const dndBehavior = data.getDndBehavior ? data.getDndBehavior(row) : "none"
  const selectionMode = form.watch("selectionMode")

  const dndProps = {
    ...attributes,
    ...listeners
  }

  const draggableProps = { 
    ...dndProps,
    style: {
      ...style, 
    }
  }
  
  if (!row) return null
  
  
  const onRowClick  = (e: MouseEvent) => {
    const selectedRows = form.getValues("selectedRows") ?? []
    const isSelected = !!selectedRows?.includes(data.getRowId(row))
    const shiftHeld = e.shiftKey
    const isMetaHeld = e.metaKey
    const multipleSelectAllowed = selectionMode === "multiple"
    const rowId = data.getRowId(row)
    const isRightClick = e.button === 2
    
    // Cases

    // Shift Not Held && Multiple Select Allowed

    // 1. Select only one row
    // 2. Row is selected, deselect row

    //Shift Held && Multiple Select Allowed

    // 1. Add row to selection

    // Shift Held && Multiple Select Not Allowed

    // 1. Select only one row
    // 2. Row is selected, deselect row
    if (!isRightClick) {
      if (isMetaHeld) {
        if (isSelected) {
          form.setValue("selectedRows", selectedRows.filter(row => row !== rowId))
        } else {
          form.setValue("selectedRows", [...selectedRows, rowId])
        }
      } else if (!shiftHeld && multipleSelectAllowed) {
        if (isSelected && selectedRows.length === 1) {
          form.setValue("selectedRows", [])
        } else if (isSelected && selectedRows.length > 1) {
          form.setValue("selectedRows", [rowId])
        } else {
          form.setValue("selectedRows", [rowId])
        }
      } else if (shiftHeld && multipleSelectAllowed) {
        const newSelection = getItemsBetweenSelections(form.getValues().rows.map(row => data.getRowId(row)), rowId, selectedRows)
        form.setValue("selectedRows", newSelection)
      } else if (!multipleSelectAllowed) {
        if (isSelected) {
          form.setValue("selectedRows", [])
        } else {
          form.setValue("selectedRows", [rowId])
        }
      }
    }
    data?.onRowClick && data?.onRowClick(e, row, form)
  }
  
  if (isDragging && data.handleLongRowPress) {
    data.handleLongRowPress = undefined
  }
  if (dndBehavior.includes("draggable") && dndBehavior.includes("droppable")) {
    return <span {...draggableProps} ref={draggableNodeRef}>
      <span ref={droppableNodeRef}>
      <NonDNDRow isDragging={isDragging} isOver={rest.isOver} onRowDoubleClick={data.onRowDoubleClick} onRowClick={onRowClick} key={data.getRowId(row)} {...{index, style:{}, data, form}} />
    </span>
    </span>
  } else if (dndBehavior.includes("draggable")) {
    return <span {...draggableProps} ref={draggableNodeRef}>
      <NonDNDRow isDragging={isDragging} isOver={false} onRowDoubleClick={data.onRowDoubleClick} onRowClick={onRowClick} key={data.getRowId(row)} {...{index, data, form, style: {}}} />
    </span>
  } else if (dndBehavior.includes("droppable")) {
    return <span ref={droppableNodeRef} style={style}><NonDNDRow isDragging={isDragging} onRowDoubleClick={data.onRowDoubleClick} isOver={rest.isOver} onRowClick={onRowClick} key={data.getRowId(row)} {...{index, style, data, form}} /></span>
  }
  return <NonDNDRow isDragging={isDragging} isOver={false} onRowDoubleClick={data.onRowDoubleClick} onRowClick={onRowClick} key={data.getRowId(row)} {...{index, style: style, data, form}} />
}

function StatefulRow<T extends BaseTableRecord>({ index, style, data }: ListChildComponentProps<{ 
  getDndBehavior: TableProps<T>["getDndBehavior"],
  onRowClick?: TableProps<T>["onRowClick"]
  onRowDoubleClick: TableProps<T>["onRowDoubleClick"] | undefined
  getRowId: TableProps<T>["getRowId"], currentLength: number; endOfTable: ReactElement<any, string | JSXElementConstructor<any>> | undefined; onScrollEnd: ((data: T) => void) | undefined; draggable: boolean; }>) {
  const form = useFormContext<TableState<T>>()
  if (!form) return null
  return <DndRow {...{index, style, data}} form={form}  />
}

function DragMonitor<T extends BaseTableRecord>({
  rows, onDragEnd, getRowId, rowHeight, DragComponent, onRowDoubleClick
}: {
  rows: T[],
  onDragEnd?: TableProps<T>["onDragEnd"],
  getRowId: (row: T) => string
  rowHeight: ((rowIndex: number) => number);
  DragComponent?: TableProps<T>["DragComponent"]
  onRowDoubleClick: TableProps<T>["onRowDoubleClick"] | undefined
}) {
  const form = useFormContext<TableState<T>>()

  const [showOverLay, setShowOverlay] = useState(false)

  const selectedProps = useMemo(() => {
    const selectedRows = form.getValues("selectedRows")
    return selectedRows?.map((activeId) => {
      const rowIndex = rows.findIndex(row => getRowId(row) === activeId)
      const row = rows[rowIndex]
      if (!row) return undefined
      return {
        key: getRowId(row),
        index: rowIndex,
        style: {
          width: "100%",
          height: rowHeight(rowIndex),
          backgroundColor: 'unset',
        },
        columns: form.getValues("columns"),
        selectionsEnabled: false,
        onSelect: () => null,
        isSelected: false,
        form: form,
        row: row,
        spanRef: () => null,
        onRowDoubleClick: onRowDoubleClick
        }
    }, []).filter(row => row !== undefined) ?? []
  }, [showOverLay])

  useDndMonitor({
    onDragEnd(e: DragEndEvent) {
      setShowOverlay(false)
      form.setValue("dragOverId", undefined)
      if (onDragEnd && e.over && e.over.id && e.over.id !== e.active.id) {
        const selectedRows = form.getValues("selectedRows")
        form.setValue("selectedRows", [])
        onDragEnd({
          source: selectedRows ?? [],
          destination: String(e.over.id)
        })
      }
    },
    onDragMove(e) {
      setShowOverlay(String(e.over?.id) !== String(e.active?.id))
    }
  })

  return  <DragOverlay>
    <div>
  {((selectedProps?.length ?? 0) > 0 && form && showOverLay) ? (
    DragComponent ?
    <DragComponent selectedProps={selectedProps}/> :
    <div className='relative h-full bg-primary-100/70 dark:bg-gray-600/70 w-full'>
      {
        selectedProps.map((props, idx) => {
          return <PresentationRow<T>
            {...props}
            key={props.key}
            onRowDoubleClick={onRowDoubleClick}
            isDragging={true}
          />
        })
      }
    </div>
  ): null}
  </div>
</DragOverlay>
}


function Rows<T extends BaseTableRecord>({
  endOfTable,
  onScrollEnd,
  rowHeight,
  columnsRef,
  rows,
  getRowId,
  getDndBehavior,
  onDragEnd,
  onRowClick,
  DragComponent,
  onRowDoubleClick,
  handleLongRowPress,
  rowLongPressOptions
}: {
  endOfTable: ReactElement<any, string | JSXElementConstructor<any>> | undefined;
  onScrollEnd: ((data: T) => void) | undefined;
  ariaLabel: string; columnsRef: React.MutableRefObject<HTMLDivElement | null>
  rows: T[];
  getRowId: TableProps<T>["getRowId"]
  rowHeight: ((rowIndex: number) => number);
  getDndBehavior?: TableProps<T>["getDndBehavior"]
  onDragEnd?: TableProps<T>["onDragEnd"]
  onRowClick?: TableProps<T>["onRowClick"]
  DragComponent: TableProps<T>["DragComponent"]
  onRowDoubleClick: TableProps<T>["onRowDoubleClick"] | undefined
  handleLongRowPress: PresentationRowProps<T>["handleLongRowPress"] | undefined
  rowLongPressOptions: PresentationRowProps<T>["rowLongPressOptions"] | undefined
}) {
  

  const isDnd = !!getDndBehavior && !!onDragEnd
  const portal = document.getElementById("portal")
  return <>
  {isDnd && portal && createPortal(<DragMonitor onRowDoubleClick={onRowDoubleClick} onDragEnd={onDragEnd} rows={rows} getRowId={getRowId} rowHeight={rowHeight} DragComponent={DragComponent} />, portal)}
  <AutoSizer className='overflow-visible'>
    {({ width, height }) => {
      return <VariableSizeList<{ 
        onRowDoubleClick: TableProps<T>["onRowDoubleClick"] | undefined,
        onRowClick?: TableProps<T>["onRowClick"],
        getDndBehavior: TableProps<T>["getDndBehavior"],
        getRowId: TableProps<T>["getRowId"],
        currentLength: number;
        endOfTable: ReactElement<any, string | JSXElementConstructor<any>> | undefined;
        onScrollEnd: ((data: T) => void) | undefined; draggable: boolean;
        handleLongRowPress?: PresentationRowProps<T>["handleLongRowPress"]
        rowLongPressOptions?: PresentationRowProps<T>["rowLongPressOptions"]
      }>
        height={height - 44}
        itemCount={rows.length}
        itemSize={rowHeight}
        width={columnsRef.current?.clientWidth || width}
        itemData={{
          currentLength: rows.length,
          endOfTable,
          onScrollEnd,
          draggable: true,
          getRowId: getRowId,
          getDndBehavior,
          onRowClick,
          onRowDoubleClick,
          handleLongRowPress,
          rowLongPressOptions
        }}
      >
        {StatefulRow}
      </VariableSizeList>
    }}
  </AutoSizer>
  </>
}


export function Table<T extends BaseTableRecord>({
  rows, columns, filterState, pagination, selectionMode = "none", selectedRows,
  onSortStateChange, onFilterStateChange, nextPage, previousPage, sortState, onSelect,
  filterConfig = {}, filterModalProps, headerStyle, endOfTable, onScrollEnd,
  rowHeight, components, ariaLabel, getRowId, hiddenColumns, showRemoveColumn, onRowClick,
  getDndBehavior, onDragEnd, DragComponent, onRowDoubleClick, rowLongPressOptions, handleLongRowPress
}: TableProps<T>) {

  const columnsRef = useRef<HTMLDivElement | null>(null)

  const tableColumns = useMemo(() => {

    return columns
  }, [columns])

  const form = useForm<TableState<T>>({
    defaultValues: {
      rows: rows,
      columns: tableColumns.map(col => ({ ...col, width: col.width || 250 })),
      filterState: filterState,
      pagination: {
        ...pagination,
        page: pagination?.page ? pagination.page : 0,
      },
      hiddenColumns: [],
      filterModalOpen: "",
      selectedRows: selectedRows ?? [],
      onSelect: onSelect,
      selectionMode: selectionMode,

    }
  })

  const paginationPage = form?.watch("pagination.page")
  const paginationPageSize = form?.watch("pagination.pageSize")
  const paginationPageTotalCount = form?.watch("pagination.totalCount")
  const paginationPageIsServerSide = form?.watch("pagination.serverSide")
  const watchedSortState = form?.watch("sortState")

  const paginationState = {
    page: paginationPage ?? 0,
    pageSize: paginationPageSize,
    totalCount: paginationPageTotalCount,
    serverSide: paginationPageIsServerSide
  }

  const hiddenColumnsState = form?.watch("hiddenColumns") || []
  const currentSort = useRef<string>("[]")


  const valueGetterMap = useMemo(() => {
    return Object.fromEntries(columns?.map((col) => [col.label, col.getValue]) || [])
  }, [columns])


  const filterLogic = useCallback((include: boolean, _filter: Filter<T>, _field: T, rowIndex: number) => {
    if (_filter.value === undefined) {
      return true
    }

    const valueGetter = valueGetterMap[_filter.field]
    const val = valueGetter && valueGetter({ row: _field, label: _filter.field, rowIndex: rowIndex })

    switch (_filter.modifier) {
      case "in":
        if (Array.isArray(_filter.value)) {
          const compareValue = Array.isArray(val) ? val.map(val => String(val).toLowerCase()) : [String(val)?.toLowerCase()]
          include = _.intersection(compareValue, _filter.value).length > 0
        } else {
          include = String(_filter.value).toLowerCase().includes(String(val).toLowerCase())
        }

        break
      case "gte":
        include = Number(_filter.value) <= Number(val)
        break
      case "lte":
        include = Number(_filter.value) >= Number(val)
        break
      case "contains":
        include = String(val).toLowerCase().includes(String(_filter.value).toLowerCase())
        break
      case "eq":
        if (Array.isArray(_filter.value)) {
          const compareValue = Array.isArray(val) ? val.map(val => String(val).toLowerCase()) : [String(val)?.toLowerCase()]
          include = _.difference(_filter.value, compareValue).length === 0
        } else {
          include = String(val).toLowerCase() === String(_filter.value).toLowerCase()
        }
        break
    }
    return include
  }, [valueGetterMap])



  const handleFilterData = useCallback(() => {
    const data = form?.getValues("filterState")
    if (!paginationState?.serverSide) {
      const orFilters = data?.filter(_datum => _datum.topology === "or") || []
      const andFilters = data?.filter(_datum => _datum.topology === "and") || []
      const newFields: T[] = []
      for (let i = 0; i < rows.length; i++) {
        let row = rows[i]
        let include = orFilters.length === 0 && andFilters.length === 0

        for (let j = 0; j < orFilters.length; j++) {
          let _filter = orFilters[j]
          include = (_filter && row)  ? filterLogic(include, _filter, row, i) : false
          if (include) {
            break
          }
        }
        for (let k = 0; k < andFilters.length; k++) {
          let _filter = andFilters[k]
          include = (_filter && row) ? filterLogic(include, _filter, row, i) : false
          if (!include) {
            break
          }
        }
        if (include && row) {
          newFields.push(row)
        }
      }
      form?.resetField("rows", { defaultValue: newFields })
      form?.resetField("pagination.totalCount", { defaultValue: newFields.length })
      form?.resetField("pagination.page", { defaultValue: 0 })

    } else {
      onFilterStateChange && onFilterStateChange(data)
    }
    form?.setValue("filterModalOpen", "")
  }, [form?.getValues, form?.setValue, JSON.stringify(rows), filterLogic])


  const handleSortData = useCallback(() => {
    const data = form?.getValues("sortState") || {}
    const fieldsState = form.getValues("rows")
    if (!pagination?.serverSide && fieldsState?.length > 0) {
      const keys: ("asc" | "desc")[] = []
      const funcs: ((obj: T) => unknown)[] = []
      const cacheState: Partial<Sort<T>>[] = []
      Object.values(data).map((_state) => _state).sort((a, b) => {
        if (a?.order && b?.order) {
          return a?.order - b?.order
        }
        return 0
      }).forEach((record) => {
        if (record && record.direction && record.sortHandler) {
          funcs.push(record.sortHandler)
          keys.push(record.direction)
          cacheState.push(record)
        }
      })
      const cache = JSON.stringify(cacheState)
      if (cache === "[]") {
        form?.setValue("rows", rows, { shouldDirty: false })
        handleFilterData()
      } else if (cache !== currentSort.current) {
        fieldsState.forEach((fi) => funcs[0] && funcs[0](fi))
        const updatesFields = _.orderBy(fieldsState, funcs, keys)
        form?.resetField("rows", { defaultValue: updatesFields })
        form?.setValue("rows", updatesFields, { shouldDirty: false })
      }
      currentSort.current = cache
    } else {
      onSortStateChange && onSortStateChange(data)
    }
  }, [form?.setValue, form?.getValues, form?.resetField, columns, JSON.stringify(rows)])




  useEffect(() => {
    const currentPagination = form?.getValues("pagination")
    if (pagination) {
      if (currentPagination?.page !== pagination.page) {
        form?.setValue("pagination.page", pagination.page)
      }
      if (currentPagination?.pageSize !== pagination.pageSize && !paginationState?.serverSide) {
        form?.setValue("pagination.pageSize", pagination.pageSize)
      }
      if (currentPagination?.totalCount !== pagination.totalCount) {
        form?.setValue("pagination.totalCount", pagination.totalCount)
      }
    }
  }, [JSON.stringify(pagination)])

  useEffect(() => {
    form?.setValue("rows", rows)
    form?.resetField("rows", { defaultValue: rows })
  }, [JSON.stringify(rows)])

  useEffect(() => {
    if (sortState) {
      form?.setValue("sortState", sortState, { shouldDirty: false })
      if (!pagination?.serverSide) {
        handleSortData()
      }

    }
  }, [sortState])

  useEffect(() => {
    if (tableColumns) {
      form?.setValue("columns", tableColumns.map(col => ({ ...col, width: col.width || 250 })), { shouldDirty: false })
    }
  }, [tableColumns])

  useEffect(() => {
    if (filterState?.length !== undefined) {
      form?.resetField("filterState", { defaultValue: filterState })
      if (!pagination?.serverSide) {
        handleFilterData()
      }
    }
  }, [filterState])


  

  const visibleColumns = useMemo(() => {
    const userDefinedColumns = columns?.filter(col => !hiddenColumnsState.includes(col.label)) || []
    return userDefinedColumns
  }, [columns, hiddenColumnsState])


  const handleSort = useCallback((direction: "asc" | "desc" | undefined, key: string, order?: number) => {
    const lowestOrder = Object.values(watchedSortState || {}).reduce((prev, curr) => {
      return (curr?.order || 0) > (prev?.order || 0) ?
        curr : prev
    }, {
      direction: undefined,
      order: 0,
      sortHandler: () => null
    })
    let newValue = undefined
    switch (direction) {
      default:
        newValue = direction
        break
    }
    form?.setValue(`sortState.${key}.direction`, newValue as unknown as TableState<T>["headerState"][string]["direction"], { shouldDirty: false })
    if (!order) {
      form?.setValue(`sortState.${key}.order`, (lowestOrder?.order || 0) + 1 as unknown as TableState<T>["headerState"][string]["order"], { shouldDirty: false })
    }
    handleSortData()
  }, [form?.setValue, watchedSortState, JSON.stringify(rows)])

  const watchedRows = form?.watch("rows") ?? []

  return  <FormProvider {...form} >
      {components?.tableToolbar &&
        components?.tableToolbar({ form })
      }
      <div className="relative overflow-scroll h-full">
      
      {hiddenColumnsState.length > 0 &&
        <div className='flex gap-4 py-4'>
          <Typography className='text-sm font-bold'>Hidden Columns:</Typography> <div className='flex gap-2'>
            {hiddenColumnsState.map((col) => <Badge color={BadgeColor.RED} key={col} onRemove={() => form?.setValue("hiddenColumns", hiddenColumnsState.filter(_col => _col !== col))}>
              {col}
            </Badge>)}
          </div>
        </div>
      }
      <div ref={columnsRef} className='table whitespace-nowrap overflow-auto'>
        {visibleColumns.map((col, idx) => <StyledColumn idx={idx}
          id={`${col.label.toLowerCase().replace(" ", "-")}-col-header`}
          key={col.label as string | number}
          className={classNames(
            handleHiddenStyles(col?.hidden), headerStyle
          )}
        >
          <div className='flex justify-between w-full'>
            <div tabIndex={-1} className='p-2 w-full items-center flex justify-between'>
              <span className='text-sm'>{!col.hideLabel && String(col.label)}</span>
              <span className='flex gap-1'>
                <FilterBadge fieldLabel={col.label} handleFilterData={handleFilterData} />
                {(watchedSortState && watchedSortState[col.label]?.direction) &&
                  <Badge color={BadgeColor.PURPLE} onRemove={() => {
                    form?.setValue(`sortState.${col.label}`, { ...watchedSortState[col.label], direction: undefined, order: undefined })
                    handleSortData()
                  }}>
                    {watchedSortState && watchedSortState[col.label]?.direction === "asc" ? "a-z" : "z-a"}
                  </Badge>
                }
                {!col.hideDropdown && <DropMenu
                  ariaLabel={`control-${col.label}`}
                  color={ButtonColor.NONE}
                  popOverProps={{
                    placement: "bottom right",
                  }}
                  disabledKeys={[
                    "filter",
                  ]}
                  onAction={(key) => {
                    const currentFilterState = form?.getValues().filterState ?? []
                    const hasCurrentField = currentFilterState?.filter(fil => fil.field === col.label).length > 0
                    switch (key) {
                      case `control-${col.label}-asc`:
                        handleSort("asc", col.label as string)
                        break
                      case `control-${col.label}-desc`:
                        handleSort("desc", col.label as string)
                        break
                      case `control-${col.label}-filter`:
                        form?.setValue("filterModalOpen", col.label as string)
                        form?.setValue("filterState", [
                          ...currentFilterState,
                          ...(!hasCurrentField && filterConfig ? [{
                            topology: "or" as "and" | "or",
                            field: col.label,
                            modifier: filterConfig[col.label]?.modifiers[0] || "eq",
                            value: undefined
                          }] : [])])
                        break
                      case `control-${col.label}-hide`:
                        form?.setValue("hiddenColumns", [...hiddenColumnsState, col.label])
                        break
                    }
                  }}
                  menuItems={[
                    {
                      key: "filter",
                      "aria-label": `filter-${col.label}`,
                      disabled: !filterConfig || !filterConfig[col.label as string],
                      children: <Typography
                        element={TypographyElement.p}
                        className={classNames("flex items-center gap-2", !filterConfig || !filterConfig[col.label as string])}
                      >
                        <FunnelIcon className="text-gray-500 w-4 h-4" /> Filter
                      </Typography>
                    },
                    {
                      key: "asc",
                      "aria-label": `sort-asc-${col.label}`,
                      disabled: (!pagination?.serverSide && !(watchedSortState && watchedSortState[col.label]?.sortHandler)) || !col.sortable,
                      children: <Typography
                        element={TypographyElement.p}
                        className={classNames("flex items-center gap-2", watchedSortState && !watchedSortState[col.label])}>
                        <span className="text-xs font-light text-gray-500">a-z</span> Sort Asc
                      </Typography>
                    },
                    {
                      key: "desc",
                      disabled: (!pagination?.serverSide && !(watchedSortState && watchedSortState[col.label]?.sortHandler)) || !col.sortable,
                      "aria-label": `sort-desc-${col.label}`,
                      children: <Typography
                        element={TypographyElement.p}
                        className={classNames("flex items-center gap-2", watchedSortState && !watchedSortState[col.label])}
                      >
                        <span className="text-xs font-light text-gray-500">z-a</span> Sort Desc
                      </Typography>
                    },
                    {
                      key: "hide",
                      "aria-label": `hide-${col.label}`,
                      children: <Typography
                        element={TypographyElement.p}
                        className="flex items-center gap-2"
                      >
                        <EyeSlashIcon className="text-gray-500 w-4 h-4" /> Hide
                      </Typography>
                    }
                  ]}
                />}
              </span>
            </div>
          </div>
        </StyledColumn>)}
      </div>
      
        <Rows
          ariaLabel={ariaLabel}
          columnsRef={columnsRef}
          endOfTable={endOfTable}
          onScrollEnd={onScrollEnd}
          rowHeight={rowHeight}
          rows={watchedRows}
          getRowId={getRowId}
          getDndBehavior={getDndBehavior}
          onDragEnd={onDragEnd}
          onRowClick={onRowClick}
          onRowDoubleClick={onRowDoubleClick}
          DragComponent={DragComponent}
          rowLongPressOptions={rowLongPressOptions}
          handleLongRowPress={handleLongRowPress}
        />
      

      {rows.length >= 0 &&
        <TableFilterModal<T>
          {...filterModalProps}
          control={form?.control}
          columns={columns}
          setOpened={() => form?.setValue("filterModalOpen", "")}
          getValues={form?.getValues}
          handleFilterData={handleFilterData}
          filterConfig={filterConfig}
        />
      }
      </div>
    </FormProvider>
  
}

function FilterBadge<T extends BaseTableRecord>({ fieldLabel, handleFilterData }: { fieldLabel: string, handleFilterData: () => void }) {

  const { watch, control, setValue } = useFormContext<TableState<T>>()
  const watchedFilterState = watch(`filterState`)
  const filterLength = watchedFilterState?.filter(fil => fil.field === fieldLabel).length || 0

  return filterLength > 0 && <Badge color={BadgeColor.PURPLE} onRemove={() => {
    setValue("filterState", watchedFilterState?.filter(fil => fil.field !== fieldLabel) || [])
    handleFilterData()
  }}>
    {String(filterLength)}
  </Badge>
}
