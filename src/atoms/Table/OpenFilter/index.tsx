import { ForwardedRef, forwardRef } from "react";
import { Button, ButtonColor, ButtonProps } from "../../Button";
import { useFormContext, useWatch } from "react-hook-form";
import { FunnelIcon } from "@heroicons/react/24/outline";


export const OpenFilter =  forwardRef(({ color = ButtonColor.SOFT, leadingIcon = <FunnelIcon className="w-6 h-6" />, children = "Filter", ...props}:  ButtonProps, ref: ForwardedRef<HTMLButtonElement>) =>  {
    const {control, setValue} = useFormContext()

    const filterModelOpen = useWatch({control, name: "filterModalOpen"})
    return <Button 
        {...props}
        leadingIcon={leadingIcon}
        color={color}
        children={children}
        onClick={(...args) => {
            props.onClick && props.onClick(...args)
            !!filterModelOpen ? setValue("filterModalOpen", "") : setValue("filterModalOpen", "true")
        }}
        ref={ref}
    />
})