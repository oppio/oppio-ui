import {HTMLAttributes, useEffect, useRef, useState, MouseEvent as ReactMouseEvent, forwardRef} from "react"
import { classNames } from "../../server-side";
import { useFormContext } from "react-hook-form";
import { BaseTableRecord, TableState } from "./types";


export const StyledColumn = ({idx, ...props}: HTMLAttributes<HTMLTableHeaderCellElement> & {idx: number}) => {
    const form = useFormContext<TableState<BaseTableRecord>>()
    const col = form?.watch(`columns.${idx}`) 
    const minWidth = col?.minWidth || 130
    const maxWidth = col?.maxWidth
    const width = col?.width ?? 0
    const setWidth =(width: number) => {
      form?.setValue(`columns.${idx}.width`, width >= minWidth ? width : minWidth)
    }

    const [isDragging, setIsDragging] = useState(false);
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [startPosition, setStartPosition] = useState({ x: 0, y: 0 });
    const elementRef = useRef<HTMLDivElement>(null);
    const [containingEle, setContainingEle] = useState<HTMLDivElement | null>(null);
    const ogWidth = useRef<number>(0);
    
    
    useEffect(() => {
        ogWidth.current = containingEle?.clientWidth || 0;
    }, [containingEle]);

    useEffect(() => {
      const handleMouseMove = (e: MouseEvent) => {
        if (isDragging) {
          const dx = e.clientX - startPosition.x;
          const dy = e.clientY - startPosition.y;
          setPosition({ x: dx, y: dy });
          
          // Log the relative position
          setWidth(ogWidth.current + dx);
        }
      };

      if (isDragging) {
        window.addEventListener('mousemove', handleMouseMove);
        window.addEventListener('mouseup', handleMouseUp);
      }

      return () => {
        window.removeEventListener('mousemove', handleMouseMove);
        window.removeEventListener('mouseup', handleMouseUp);
      };
    }, [isDragging, startPosition]);
    

    const handleMouseUp = () => {
        setIsDragging(false);
    };

    const handleMouseDown = (e: MouseEvent | ReactMouseEvent) => {
      setIsDragging(true);
      setStartPosition({ x: e.clientX - position.x, y: e.clientY - position.y });
    };

    
    return <div
        ref={setContainingEle}
        {...props}
        style={{maxWidth: maxWidth, minWidth: minWidth, width: width}}
        className={classNames("inline-block", typeof props.className === "string" ? props.className : undefined)}
        
    >
        <div className="flex items-center justify-between">
            {props.children}
            <div 
                ref={elementRef}
                className="w-[2px] h-8 hover:bg-primary-500 translate-x-[2px] cursor-col-resize"
                onMouseDown={handleMouseDown}
                onMouseUp={handleMouseUp}
            />
        </div>
    </div>
}

export const StyledCell = forwardRef<HTMLSpanElement, HTMLAttributes<HTMLTableCellElement>>((props, ref) => {
    return <span
        {...props}
        ref={ref}
        className={classNames("border-0 whitespace-nowrap text-sm font-medium text-gray-900", typeof props.className === "string" ? props.className : undefined)}
    />
})

