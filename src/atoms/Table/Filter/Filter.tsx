import { SelectField, Option } from "../../SelectField"
import { Control, Controller, PathValue, UseFormGetValues, useController, useFieldArray, useFormContext, useWatch } from "react-hook-form"
import { BaseTableRecord, Filter, FilterModifier,  FilterWidget, TableProps, TableState } from "../types"
import { DateTime } from "luxon"
import { PopperPlacement } from "../../Popover"
import { InputField } from "../../InputField"
import { FormEvent, useCallback, useEffect, useMemo, useState } from "react"
import { Modal, ModalProps } from "../../../molecules/Modal"

import _ from "lodash"
import { CheckCircleIcon, PlusCircleIcon, TrashIcon } from "@heroicons/react/24/outline"
import { Button, ButtonColor } from "../../Button"
import { DateField } from "../../DateField"



type FilterModalProps<T extends BaseTableRecord> = Omit<ModalProps, "children" | "setOpened" | "opened"> & {
  control: Control<TableState<T>>
  columns: TableProps<T>["columns"]
  getValues: UseFormGetValues<TableState<T>>
  handleFilterData: () => void
  filterConfig: TableProps<T>["filterConfig"]
  setOpened: (val: string) => void
}

export function TableFilterModal<T extends BaseTableRecord>({ control, filterConfig = {}, handleFilterData, columns, getValues, ...props }: FilterModalProps<T>) {
  
  const filterModalOpen = useWatch({ control, name: "filterModalOpen" })

  const { fields, append, remove } = useFieldArray({
    control: control,
    rules: { minLength: 1 },
    name: "filterState",
  })

  const fieldOptions = useMemo(() => {
    return Object.keys(filterConfig).map((_col) => ({
      value: _col,
      label: _.capitalize(_col as string),
      search: `${_.capitalize(_col as string)} ${_col as string}`,
    }))
  }, [JSON.stringify(columns)])

  const addNewRow = useCallback((val?: string) => {
    const firstConfig = (val && filterConfig[val]) ? val : Object.keys(filterConfig)[0]
    const initState = {
      topology: "and",
      field: firstConfig,
      modifier: filterConfig[firstConfig].modifiers[0],
      value: ""
    } as Filter<T>
    append(initState)
  }, [append, fieldOptions])

  const removeRow = useCallback((idx: number) => {
    remove(idx)
  }, [remove])

  const choicesMap = useMemo(() => {
    
    const _choiceMap: { 
      [key in keyof TableState<T>["filterConfig"]]?: Option<string>[] 
    } = Object.fromEntries(Object.entries(filterConfig).map(([field, { widget, choices }]) => {
      return [field, choices || []]
    }))
    return _choiceMap
  }, [getValues, JSON.stringify(filterConfig)])

  return <Modal {...props} opened={!!filterModalOpen} setOpened={() => props.setOpened("")} title="Filter Table Data">
    <div className="w-full mx-auto">
      <div className="h-72 flex flex-col justify-between ">
        <div className="overflow-y-scroll h-full border-b py-1">
          {fields.map((_field, idx) => {
            return <div key={_field.id} className="mt-2 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-11">
              <div className="sm:col-span-2 pt-8">
                {idx !== 0 ? <Controller
                  control={control}
                  name={`filterState.${idx}.topology`}
                  render={({ field: { ref, ...field } }) => {
                    return <SelectField
                      {...field}
                      creatable={false}
                      isMulti={false}
                      label="topology"
                      hideLabel
                      options={[{ value: "or", label: "or", search: "or" }, { value: "and", label: "and", search: "and" }]}
                    />
                  }}
                /> : <div/>}
              </div>
              <div className="sm:col-span-3">
                <div className="mt-2">
                  <Controller
                    control={control}
                    name={`filterState.${idx}.field`}
                    render={({ field: { ref, ...field } }) => {
                      return <SelectField
                        {...field}
                        creatable={false}
                        isMulti={false}
                        label="Field"
                        options={fieldOptions}
                      />
                    }} />
                </div>
              </div>
              <div className="sm:col-span-2">
                <ModifierField
                  idx={idx}
                  filterConfig={filterConfig}
                />
              </div>
              <div className="sm:col-span-3">
                <div className="mt-2">
                  <Controller
                    control={control}
                    name={`filterState.${idx}.value`}
                    render={({ field }) => {
                      return <QueryField
                        choiceMap={choicesMap}
                        label="Query"
                        idx={idx}
                        filterConfig={filterConfig}
                      />
                    }}
                  />
                </div>
              </div>
              <div className="sm:col-span-1 pt-12 cursor-pointer" onClick={() => removeRow(idx)}>
                <TrashIcon className="w-4 h-4 text-red-600 dark:text-red-400" />
              </div>
            </div>
          })}
        </div>
        <div className="flex flex-row-reverse w-full mt-6">
          <Button
            type="button"
            onClick={() => handleFilterData()}
            leadingIcon={<CheckCircleIcon className="w-4 h-4" />}
          >
            Filter Data
          </Button>
          <Button
            type="button"
            color={ButtonColor.SECONDARY}
            className="mr-2"
            onClick={() => addNewRow()}
            leadingIcon={<PlusCircleIcon className="w-4 h-4" />}
          >
            Add Filter
          </Button>

        </div>
      </div>

    </div>
  </Modal>
}

type QueryFieldProps<T extends BaseTableRecord> = {
  label: string,
  choiceMap: {[key:string]: Option<string>[]}
  idx: number
  filterConfig: TableProps<T>["filterConfig"]
}

function QueryField<T extends BaseTableRecord>({ filterConfig = {}, label, choiceMap, idx }: QueryFieldProps<T>) {
  const { control, setValue, register } = useFormContext<TableState<T>>()

  const [localValue, setLocalValue] = useState<string>("")

  const fieldValue = useWatch({control, name: `filterState.${idx}.field`})
  const modifierValue = useWatch({control, name: `filterState.${idx}.modifier`})
  const choices = choiceMap[fieldValue]
  const {
    field: {
      value: filterValue,
      onChange
    }
  } = useController({control, name: `filterState.${idx}.value`})

  const debounceOnChange = useCallback(_.debounce((val: string) => onChange(val), 500), [])

  useEffect(() => {
    debounceOnChange(localValue)
  }, [localValue])

  useEffect(() => {
    setLocalValue(filterValue as string)
  }, [])


  useEffect(() => {
    if (!filterConfig[fieldValue].modifiers.includes(modifierValue)) {
      const filter = filterConfig[fieldValue].modifiers[0]
      setValue(`filterState.${idx}.modifier`, filter as PathValue<TableState<T>, `filterState.${number}.modifier`>, {shouldDirty: true})
    }
    
  }, [fieldValue,  modifierValue])

  switch (filterConfig[fieldValue]?.widget) {
    case FilterWidget.InputField:
      return <InputField
        label={label}
        value={localValue}
        inputClassName=""
        autoComplete="false"
        onChange={(e: FormEvent<HTMLInputElement>) => {
          setLocalValue(e.currentTarget.value)
        }}
      />
    case FilterWidget.SelectField:
      return <SelectField<string>
        label={label}
        value={Array.isArray(filterValue) ? filterValue : [filterValue || ""] as string[]}
        options={choices}
        onChange={onChange}
        creatable={false}
        isMulti={true}
      />

    case FilterWidget.DateField:
      return <DateField
        label={label}
        placement={PopperPlacement.BOTTOM_CENTER}
        rangeSelector={false}
        value={filterValue ? DateTime.fromISO(filterValue as string) : undefined}
        onChange={(date: DateTime | null) => onChange(date ? date.toISO() : "")}
      />
    default:
      return <></>
  }
}

type ModifierFieldProps<T extends BaseTableRecord> = {
  idx: number
  filterConfig: TableProps<T>["filterConfig"]
}

const prettyRepOfBy = (val: string) => {
  switch (val) {
    case "eq":
      return "equals"
    case "neq":
      return "not equals"
    case "lt":
      return "less than"
    case "lte":
      return "less than or equals"
    case "gt":
      return "greater than"
    case "gte":
      return "greater than or equals"
    case "in":
      return "in"
    case "nin":
      return "not in"
    case "like":
      return "like"
    case "nlike":
      return "not like"
    case "isnull":
      return "is null"
    case "notnull":
      return "is not null"
    default:
      return val
  }
}

function ModifierField<T extends BaseTableRecord>({ idx, filterConfig = {} }: ModifierFieldProps<T>) {

  const { control } = useFormContext<TableState<T>>()

  const field = useWatch({ control, name: `filterState.${idx}.field` })
  const choices = filterConfig[field].modifiers.map((_opt) => ({ value: _opt, label: prettyRepOfBy(_opt), search: prettyRepOfBy(_opt) }))
  return <Controller
    control={control}
    name={`filterState.${idx}.modifier`}
    render={({ field }) => {
      return <SelectField<FilterModifier>
        {...field}
        value={field.value}
        className="mt-2"
        creatable={false}
        isMulti={false}
        label="By"
        options={choices || []}
        onChange={(val) => {
          return Array.isArray(val) ? field.onChange(val[0]) : field.onChange(val)
        }}
      />
    }}
  />
}