import { TableProps } from "../types";

export type TableMockType = {id: string, title: string, slug: string, path: string}

export const header: TableProps<TableMockType>["columns"] = [
  {
    attr: "id",
    label: "ID", 
    hidden: {},
    renderCell: ({row, rowIndex, attr}) => <>{row[attr]}</>,
    getValue: ({row, label}) => row["id"],
    
  },
  {
    attr: "title",
    label: "Title",
    hidden: {},
    renderCell: ({row, rowIndex, attr}) => <>{row[attr]}</>,
    getValue: ({row, label}) => row["title"],
  },
  {
    attr: "slug",
    label: "Slug",
    renderCell: ({row, rowIndex, attr}) => <>{row[attr]}</>,
    getValue: ({row, label}) => row["slug"],
    hidden: {},
  },
  {
    attr: "path",
    label: "Path",
    renderCell: ({row, rowIndex, attr}) => <>{row[attr]}</>,
    getValue: ({row, label}) => row["path"],
    hidden: {},
  }
];


export const body: TableProps<TableMockType>["rows"] = [
  {id: "1", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "2", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "3", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "4", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "5", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "6", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "7", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "8", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "9", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "10", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "11", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "12", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "13", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "14", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "15", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "16", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "17", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "18", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "19", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "20", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "21", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "22", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "23", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "24", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "25", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "26", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "27", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "28", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "29", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "30", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "31", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "32", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "33", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "34", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "35", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "36", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  {id: "37", title:"Corndog", slug: "corndog", path:"/path/corndog"},
  {id: "38", title:"Pizza", slug: "pizza", path:"/path/pizza"},
  {id: "39", title:"Hamburger", slug: "hamburger", path:"/path/hamburger"},
  
];
