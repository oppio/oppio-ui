import { Controller, useController, useFormContext, useWatch } from "react-hook-form"
import {Checkbox} from "../../Checkbox"
import { StyledCell } from "../StyledTableComponents"
import { BaseTableRecord, TableHeader, TableProps, TableState } from "../types"
import { classNames } from "../../../server-side";
import { memo } from "react"


type TableRowProps<T extends BaseTableRecord> = {
    index: number
    style?: React.CSSProperties
    columns: TableHeader<T>[]
    getRowId: TableProps<T>["getRowId"]
    onSelect: TableProps<T>["onSelect"]
    selectionsEnabled: boolean
    rowClassName: (row: T) => string | undefined
  }
  
function UnmemoizedTableRow<T extends BaseTableRecord>({ style, index, onSelect, getRowId, selectionsEnabled, rowClassName }: TableRowProps<T>) {
    const form = useFormContext<TableState<T>>()
    const renderingMode = useWatch({control:form.control, name: "pagination.serverSide"})
    const page = useWatch({control:form.control, name: "pagination.page"})
    const pageSize = useWatch({control:form.control, name: "pagination.pageSize"})
    const offset = renderingMode === false ? (page || 0) * pageSize : 0
    const correctedIdx = offset + index

    const {
        field: row
    } = useController({
        control: form.control,
        name: `rows.${correctedIdx}`
    })

    const columns = form.watch("columns")
    const hiddenColumns = form.watch("hiddenColumns")
    return <tr key={getRowId(row.value)} id={getRowId(row.value)} className={rowClassName(row.value)} style={style}>
      {selectionsEnabled && <Controller
        name={"selectedRows"}
        control={form.control}
        render={({field}) => {
        return <StyledCell key={"select"}>
        <Checkbox
            isSelected={field.value?.includes(getRowId(row.value))}
            onChange={(checked) => {
              field.onChange(checked ? [...(field.value || []), getRowId(row.value)] : field.value?.filter((id) => id !== getRowId(row.value)) ?? [])
            }}
        />
      </StyledCell>}} />}
      
      {columns?.filter(col => !hiddenColumns?.includes(col.label))?.map((_col, jdx) => {
          const element = _col.renderCell({rowIndex: index, row: row.value, attr: _col.attr,label: _col.label, onSelect, form})
          return <StyledCell 
            className={classNames("whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 dark:text-gray-100 truncate overflow-hidden")}
          >
            {element}
          </StyledCell>
      })}
    </tr>
  }

export const TableRow  = memo(UnmemoizedTableRow) as typeof UnmemoizedTableRow
  