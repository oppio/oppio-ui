

export const AnimatedEllipse = () => {
    
    return <div className="flex"><div className="animate-bounce text-4xl">.</div><div className="animate-bounce delay-100 text-4xl">.</div><div className="animate-bounce delay-200 text-4xl">.</div></div>
}