import React, { ReactElement} from "react"
import { AlertType } from "../Alert/types";
import { CheckCircleIcon, XMarkIcon, XCircleIcon } from '@heroicons/react/24/solid'
import { ExclamationTriangleIcon, InformationCircleIcon } from '@heroicons/react/20/solid'
import {Button, ButtonColor, ButtonStyle} from "../Button";
import { classNames } from "../../server-side"

export type ToastProps = {
  callback: () => void
  timer?: number 
  alertType?: AlertType
  children: ReactElement | ReactElement[] | null
}

const UnmemoizedToast = ({timer, callback, alertType = AlertType.INFO, children, ...props}: ToastProps) => {

  return <>
  <div
    aria-live="assertive"
  >
    <div className="flex w-full flex-col justify-end items-center space-y-4 sm:items-end">
      <div className="pointer-events-auto w-full max-w-sm overflow-hidden rounded-lg bg-white dark:bg-gray-700 shadow-lg ring-1 ring-black ring-opacity-5">
          <div className="p-4">
            <div className="flex items-center">
              <div className="flex-shrink-0">
                <StatusIcon alertType={alertType} className="h-6 w-6" />
              </div>
              <div className="ml-3 w-0 flex-1 pt-0.5">
                <p className="text-sm font-medium text-gray-900 dark:text-gray-100">{children}</p>
              </div>
              <div className="ml-4 flex flex-shrink-0">
                <Button
                  type="button"
                  buttonStyle={ButtonStyle.NONE}
                  color={ButtonColor.NONE}
                  className="shadow-none inline-flex rounded-md bg-white dark:bg-transparent text-gray-400"
                  onClick={callback}
                >
                  <span className="sr-only">Close</span>
                  <XMarkIcon className="h-5 w-5 text-gray-600 dark:text-gray-400" aria-hidden="true" />
                </Button>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</>
}

export const Toast = React.memo(UnmemoizedToast)

const StatusIcon = ({alertType, className}: {alertType: AlertType, className: string}) => {
  switch (alertType){
    case AlertType.SUCCESS:
      return <CheckCircleIcon className={classNames("text-green-400", className)} aria-hidden="true" />
    case AlertType.INFO:
      return <InformationCircleIcon className={classNames("text-blue-400", className)} aria-hidden="true" />
    case AlertType.WARNING:
      return<ExclamationTriangleIcon className={classNames("text-yellow-400", className)} aria-hidden="true" />
    case AlertType.ERROR:
      return <XCircleIcon className={classNames("text-red-400", className)} aria-hidden="true" />
  }

}