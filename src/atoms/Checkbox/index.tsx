import { classNames } from "../../server-side"
import {Checkbox as CheckboxCMP} from 'react-aria-components';
import type {CheckboxProps as AriaCheckboxProps} from 'react-aria-components';

export type CheckboxProps = AriaCheckboxProps;

export function Checkbox({children, ...props}: CheckboxProps) {
  return (
    <CheckboxCMP {...props}>
      {({isIndeterminate, isSelected}) => <>
        <div className={classNames("border-2  rounded flex justify-center items-center w-4 h-4", (isSelected || isIndeterminate) ? "bg-primary-500 border-primary-500" : undefined)}>
          
          <svg viewBox="0 0 18 18" aria-hidden="true" className={classNames('stroke-white w-4 h-4', isIndeterminate && "fill-white stroke-0", !isIndeterminate && "fill-none stroke-2", !isIndeterminate && !isSelected && "hidden")}>
            {isIndeterminate
              ? <rect x={1} y={7.5} width={15} height={3} />
              : <polyline points="1 9 7 14 15 4" />
            }
          </svg>
        </div>
        {children}
      </>}
    </CheckboxCMP>
  );
}