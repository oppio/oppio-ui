import {TimeField as TimeFieldCMP, Label, DateInput, DateSegment, TimeValue} from 'react-aria-components';
import { InputFieldProps } from '../InputField';
import { DateTime } from 'luxon';
import { parseAbsoluteToLocal } from '@internationalized/date';


export type TimeFieldProps = Pick<InputFieldProps, "label" | "hideLabel"> & {
    value?: DateTime
    onChange: (value: DateTime) => void
}

export function TimeField({label, hideLabel = false, value, onChange}: TimeFieldProps) {
    const valueString = value?.toISO()
    const timeValue = valueString ? parseAbsoluteToLocal(valueString) : undefined
    
    return <TimeFieldCMP
        value={timeValue}
        onChange={(time) => {
            onChange(DateTime.fromJSDate(time.toDate()))
        }}
    >
    {!hideLabel &&<Label className='block text-sm font-medium leading-6 text-gray-900 dark:text-white'>
        {label}
    </Label> }
    <DateInput
        className="block w-full rounded-md border-0 py-1.5 text-gray-900 dark:text-white shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 px-4 gap-1 focus:ring-2 focus:ring-inset focus:ring-primary-600 sm:text-sm sm:leading-6 flex"
    >
      {segment => <DateSegment segment={segment} />}
    </DateInput>
  </TimeFieldCMP>
}