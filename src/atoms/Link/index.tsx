
import React, { ComponentProps, ForwardedRef, HTMLProps, ReactElement, ReactNode, forwardRef } from "react";
import { classNames } from "../../server-side"
import { ButtonColor, ButtonSize, ButtonStyle, buttonClassStyle } from "../Button";


export type LinkProps<T extends ComponentProps<any>> = {
  as?: ((props: ComponentProps<any>, ref?: ForwardedRef<any>) => (ReactElement | ReactNode | Iterable<ReactNode> | Iterable<ReactElement>)) | "a" | ComponentProps<any>
  href?: string
  leadingIcon?: ReactElement
  trailingIcon?: ReactElement
  type?: "submit" | "button"
  buttonSize?: ButtonSize
  color?: ButtonColor
  buttonStyle?: ButtonStyle
  className?: string
  disabled?: boolean
  children: ComponentProps<any>["children"]
} & T
 
 function LinkCMP<T extends ComponentProps<any>>({
  className="",
  href,
  children,
  color = ButtonColor.PRIMARY,
  buttonSize = ButtonSize.LG,
  buttonStyle = ButtonStyle.STANDARD,
  disabled = false,
  as,
  ...props
 }: LinkProps<T>, ref: ForwardedRef<T extends HTMLProps<HTMLAnchorElement> ? HTMLAnchorElement : HTMLElement>) {

  const Component = as ? as : "a"

  return <Component href={href || ""} ref={ref} {...props}  className={classNames(buttonClassStyle(className, color, buttonSize, buttonStyle, disabled))}>
    {children}
  </Component>
}

LinkCMP.displayName = "Link"

export const Link = forwardRef(LinkCMP) as <T extends ComponentProps<any>>(props: LinkProps<T> & { ref?: ForwardedRef<T> }) => JSX.Element