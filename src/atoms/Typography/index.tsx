import React, { HTMLProps } from "react";


export enum TypographyElement {
  "h1" = "h1",
  "h2" = "h2",
  "h3" = "h3",
  "h4" = "h4",
  "h5" = "h5",
  "h6" = "h6",
  "p" = "p",
  "div" = "div"
}

const titleStyle = (className: string, element: TypographyElement) => {
  let style = "transition-all text-neutral-900 dark:text-primary-100"

  switch (element) {
    case TypographyElement.h1:
    style += ` text-5xl`
    break
    case TypographyElement.h2:
    style += ` text-4xl`
    break
    case TypographyElement.h3:
    style += ` text-3xl`
    break
    case TypographyElement.h4:
    style += ` text-2xl`
    break
    case TypographyElement.h5:
    style += ` text-xl`
    break
    case TypographyElement.h6:
    style += ` text-lg`
    break
    case TypographyElement.p:
    style += ` text-md`
    break
    case TypographyElement.div:
    style += ` text-md`
    break
  }
  style += ` ${className}`
  return style
}



export type TypographyProps =  {
  element?: TypographyElement
} & HTMLProps<HTMLHeadingElement | HTMLParagraphElement | HTMLDivElement>


export function Typography({
  element = TypographyElement.p,
  ...props
}: TypographyProps) {

  switch(element) {
    case TypographyElement.h1:
      return <h1 {...props} className={titleStyle(props.className ?? "", element)}/>
    case TypographyElement.h2:
      return <h2 {...props}className={titleStyle(props.className ?? "", element)}/>
    case TypographyElement.h3:
      return <h3 {...props}className={titleStyle(props.className ?? "", element)}/>
    case TypographyElement.h4:
      return <h4 {...props} className={titleStyle(props.className ?? "", element)}/>
    case TypographyElement.h5:
      return <h5 className={titleStyle(props.className ?? "", element)}/>
    case TypographyElement.h6:
      return <h6 {...props} className={titleStyle(props.className ?? "", element)}/>
    case TypographyElement.p:
      return <p {...props} className={titleStyle(props.className ?? "", element)}/>
    case TypographyElement.div:
      return <div {...props} className={titleStyle(props.className ?? "", element)}/>
    default:
      return <div {...props} className={titleStyle(props.className ?? "", TypographyElement.div)}/>
  }
}