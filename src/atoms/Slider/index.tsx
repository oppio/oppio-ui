import { ChangeEvent, useEffect } from "react"
import { useState } from "react"

export type SliderProps = {
  className?: string
  min: number
  onChange: (value: number) => void
  value?: number
  max: number
  step: number
}

export function Slider({value, max, step, min, onChange}: SliderProps) {
    const [sliderLineRef, setSliderLineRef] = useState<HTMLDivElement | null>(null)
    const [sliderButtonRef, setSliderButtonRef] = useState<HTMLDivElement | null>(null)
    const [isSliding, setIsSliding] = useState(false)

    const calculatedleftPosition = Math.floor((((value || min) - min) / (max - min) * 100 / step)) * step
    const leftPosition = 100 >= calculatedleftPosition ? calculatedleftPosition : 100

    return <div 
      className="w-full h-4 flex flex-col items-center py-4"
      onMouseUp={() => setIsSliding(false)}
      onMouseMove={(e) => {
        const {left, width} = sliderLineRef?.getBoundingClientRect() || {left: 0, width: 0}
        const newPos = (e.pageX - left) / width
        if (newPos <= 1 && newPos >=0 && isSliding) {
          sliderButtonRef?.setAttribute("style", `left: ${newPos * 100}%`)
          onChange((newPos * (max - min)) + min)
        }
      }}
      onMouseDown={() => setIsSliding(true)}
      onMouseLeave={() => setIsSliding(false)}
    >
        <div ref={setSliderLineRef} className="w-full relative">
            <div className="w-full h-1 bg-gray-400"/>
            <div className="absolute top-0 left-0 h-1 bg-primary-700" style={{width: `${leftPosition}%`}}/>
            <div
              ref={setSliderButtonRef}
              
              
              className="w-4 h-4 rounded-full bg-primary-700 absolute top-0 translate-x-[-50%] translate-y-[-37.5%]" style={{left: `${leftPosition}%`}}
            />
        </div>
        <div></div>
    </div>
}