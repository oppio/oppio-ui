import {ProgressBar, Label, ProgressBarProps} from 'react-aria-components';

export type ProgressProps = ProgressBarProps & {
    loadingText?: string
}

export function Progress({loadingText = "", value = 0, ...props}: ProgressProps) {
    return <ProgressBar {...props} value={value}>
  {({percentage, valueText}) => <div className='py-4'>
    <div className='flex justify-between text-gray-600'>
    <Label className='w-full'>{value < 100 ? loadingText : ""}</Label>
    <span className="value dark:text-white">{value < 100 ? valueText : "Complete!"}</span>
    </div>
    <div className="w-full h-1 bg-gray-200">
      <div className="bg-primary-400 h-1" style={{width: percentage + '%'}} />
    </div>
  </div>}
</ProgressBar>
}