
import { Popover as PopoverCMP, PopoverButton, Transition, PopoverPanel } from '@headlessui/react';
import { Fragment, ReactElement, useState } from 'react';
import { usePopper } from 'react-popper';
import { classNames } from '../../server-side';

export enum PopperPlacement {
  TOP_LEFT = "TOP_LEFT",
  TOP_CENTER = "TOP_CENTER",
  TOP_RIGHT = "TOP_RIGHT",
  BOTTOM_LEFT = "BOTTOM_LEFT",
  BOTTOM_CENTER = "BOTTOM_CENTER",
  BOTTOM_RIGHT = "BOTTOM_RIGHT",
}

export type PopoverProps = {
  popoverContent: ({open, close}: {open:boolean, close: () => void}) => ReactElement | ReactElement[]
  placement?: PopperPlacement
  children: ({close}: {close: () => void}) => ReactElement | ReactElement[]
  open?: boolean
  className?: string
  buttonClassName?: string
  
}

const popPanelStyle = (placement: PopperPlacement) => {
  let style = "fixed z-30 mt-3 max-w-sm  transform px-4 sm:px-0 lg:max-w-3xl"
  return style
}

export function Popover({
  children,
  open,
  popoverContent,
  placement = PopperPlacement.BOTTOM_LEFT,
  className,
  buttonClassName,
  ...props
}: PopoverProps){
  const [popperButtonRef, setPopperButtonRef] = useState<HTMLButtonElement | null>(null)
  const [popperWrapperRef, setPopperWrapperRef] = useState<HTMLDivElement | null>(null)
  let { styles, attributes } = usePopper(popperButtonRef, popperWrapperRef, {placement: "bottom-end"})


  return <PopoverCMP className={classNames("relative w-fit", className)}>
  {({ open, close }) => (
    <>
      <PopoverButton className={buttonClassName} ref={setPopperButtonRef}>
        <span>{children({close})}</span>
      </PopoverButton>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-200"
        enterFrom="opacity-0 translate-y-1"
        enterTo="opacity-100 translate-y-0"
        leave="transition ease-in duration-150"
        leaveFrom="opacity-100 translate-y-0"
        leaveTo="opacity-0 translate-y-1"
      >
        <PopoverPanel 
          className={popPanelStyle(placement)}
        >
          <div 
            style={styles.popper}
            {...attributes.popper}
            className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black/5 bg-white dark:bg-gray-800" ref={setPopperWrapperRef}
          >
            {popoverContent({open, close})}
          </div>
        </PopoverPanel>
      </Transition>
      
    </>
  )}
</PopoverCMP>
}