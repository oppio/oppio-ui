import { classNames } from "../../server-side"
import { HTMLProps } from "react";

export type ContainerProps = HTMLProps<HTMLDivElement>

export function Container({children, ...props}: ContainerProps ) {
    return (
      <div {...props} className={classNames("mx-auto px-4 sm:px-6 lg:px-8", props.className)}>
        {/* We've used 3xl here, but feel free to try other max-widths based on your needs */}
        <div className="mx-auto">{children}</div>
      </div>
    )
  }