import { DateTime } from 'luxon'
import { InputField, InputFieldProps } from '../InputField'
import { Popover, PopoverProps } from '../Popover'
import { DateSelector } from '../DateSelector'
import { classNames, monthDayYear  } from "../../server-side"

import { CalendarDaysIcon } from '@heroicons/react/24/outline'
import { ForwardedRef, forwardRef } from 'react'
import { XMarkIcon } from '@heroicons/react/20/solid'


export type DateFieldProps = {
  value?: DateTime | null
  initialDate?: DateTime
  onChange?: (value: DateTime | null) => void
  rangeSelector?: false
  startYear?: number
  endYear?: number
  selectTime?: boolean
  numberOfMonths?: number
  clearable?: boolean
} & Pick<InputFieldProps, "label" | "error" | "name" | "className" | "hideLabel"> & Pick<PopoverProps, "placement">


export const DateField = forwardRef(({ 
  label, placement, error, name, className,
  endYear, initialDate, hideLabel, clearable = false, ...props 
}: DateFieldProps, ref: ForwardedRef<HTMLInputElement>) => {

  return <div className='flex items-center w-full'>
    <Popover
      className={className}
      placement={placement}
      popoverContent={({ open, close }) => <DateSelector
        {...props}
        rangeSelector={false}
        value={props.value || undefined}
        className="min-w-[275px] w-full"
        initialDate={initialDate}
        endYear={endYear}
        onChange={(date: DateTime | [DateTime, DateTime]) => {
          props.onChange && date !== props.value && props.onChange(date as DateTime & [DateTime, DateTime])
          if (open) {
            if (!Array.isArray(date) && !Array.isArray(props.value)) {
              (props?.value && !date?.equals(props?.value) || !props?.value) && close()
            } else if (Array.isArray(date) && Array.isArray(props.value)) {
              !date[0]?.equals(props?.value[0]) && !date[1]?.equals(props?.value[1]) && close()
            }
          }
        }}
      />}
    >
      {({ close }) => {
        return <InputField
          ref={ref}
          label={label}
          value={Array.isArray(props.value) ? `${monthDayYear(props.value[0])}-${monthDayYear(props.value[1])}` : (props.value ? monthDayYear(props.value) : "")}
          trailingIcon={<div className='flex'>
            <CalendarDaysIcon className='w-4 h-4 dark:text-white' />
          </div>}
          error={error}
          name={name}
          onChange={() => null}
          className={className}
          hideLabel={hideLabel}
        />
      }}
    </Popover>
    {clearable &&
      <div className={classNames('pl-2', hideLabel ? "" : "pt-6")}>
        <XMarkIcon className='w-4 h-4 dark:text-white cursor-pointer' onClick={() => props.onChange && props.onChange(null)} />
      </div>
    }
  </div>
})