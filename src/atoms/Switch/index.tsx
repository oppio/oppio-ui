import { ForwardedRef, ReactElement, forwardRef, useState } from 'react'
import { Switch as SwitchCMP} from '@headlessui/react'

import { classNames } from "../../server-side";


export type SwitchProps = {
  id?: string
  value: boolean
  onChange: (value: boolean) => void
  labelLeft?: string | ReactElement
  labelRight?: string | ReactElement
  ariaLabel?: string
}

export function SwitchField ({id, ariaLabel, value, onChange, labelLeft, labelRight}: SwitchProps, ref: ForwardedRef<HTMLElement>) {
  return <div id={id} className="flex items-center my-2 gap-2">
  {labelLeft &&
    <span className="flex flex-col text-gray-900 dark:text-white">
      {labelLeft}
    </span>
  }
  <SwitchCMP
    aria-label={ariaLabel}
    checked={value}
    onChange={onChange}
    className={classNames(
      value ? 'bg-primary-600' : 'bg-gray-200 dark:bg-gray-700',
      'relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-primary-600 focus:ring-offset-2'
    )}
  >
    <span
      aria-hidden="true"
      className={classNames(
        value ? 'translate-x-5' : 'translate-x-0',
        'pointer-events-none inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out'
      )}
    />
  </SwitchCMP>
  {labelRight &&
    <span className="flex flex-col text-gray-900 dark:text-white">
      {labelRight}
    </span>
  }
</div>
}

export const Switch = forwardRef(SwitchField);
