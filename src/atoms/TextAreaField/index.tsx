import { classNames } from "../../server-side"
import { ExclamationCircleIcon } from "@heroicons/react/24/solid";
import { HTMLProps } from "react"
import React from "react";




export type TextAreaFieldProps = HTMLProps<HTMLTextAreaElement> & {
  error?: string
  hideLabel?: boolean
  containerClassName?: string
}


export const TextAreaField = React.forwardRef<HTMLTextAreaElement, TextAreaFieldProps>(({label, error, media, hideLabel, containerClassName, ...props }: TextAreaFieldProps, ref) => {

  return <div className={containerClassName}>
  <label htmlFor="comment" className={hideLabel ? "sr-only" : "block text-sm font-medium leading-6 text-gray-900 dark:text-white"}>
    {label}
  </label>
  <div className="relative">
    <textarea
      ref={ref}
      {...props }
      className={classNames("bg-white block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-primary-600 sm:text-sm sm:leading-6 dark:bg-gray-800 dark:text-white p-2", props.className)}
    />
    { error &&
        <p className="mt-2 text-sm text-red-600" id={`${name}-error`}>
         {error}
        </p>
      }
      {error &&
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-start pt-3 pr-3">
            <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
          </div>
        }
  </div>
</div>
})


