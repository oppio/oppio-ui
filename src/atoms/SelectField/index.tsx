import {  useMemo, useState,  Fragment, FormEvent, ForwardedRef, forwardRef, useEffect, ReactElement, FormEventHandler } from "react";

import _ from "lodash";
import { Combobox, ComboboxInput, ComboboxButton, ComboboxOption, ComboboxOptions, Label } from "@headlessui/react";
import { CheckIcon, ChevronUpDownIcon, PlusIcon } from "@heroicons/react/24/outline";


import { classNames } from "../../server-side"
import { ExclamationCircleIcon, XMarkIcon } from "@heroicons/react/20/solid";
import { Spinner } from "atoms/Spinner";



export type Option<T> = { value: T, label: string, search: string, menuItem?: ReactElement }

export type SelectFieldProps<T> = ({
  value?: T | null
  onChange: (option: T | undefined) => void
  isMulti: false
} | {
  value?: T[] | null
  onChange: (option: T[] | undefined) => void
  isMulti: true
}) & {
  creatable: boolean
  options?: Option<T>[]
  label?: string
  className?: string
  openOnHover?: boolean
  onTextInputChange?: (value: string | null) => void
  error?: string
  disabled?: boolean
  onCreate?: (value: string) => void
  hideLabel?: boolean
  onQueryChange?: (val: string) => void
  useFixed?: boolean
  clearable?: boolean
  placeholder?: string
  isLoading?: boolean
  comboInputClassName?: string
  comboOptionClassName?: string
  onBlur?: FormEventHandler<HTMLDivElement>
  id?: string
  onScrollEnd?: () => void
  portal?: boolean
}


function SelectFieldCMP<T>({ 
  value, onChange, isMulti, creatable = false, options = [], label, error, id,
  disabled, onCreate, hideLabel = false, onQueryChange, className, useFixed = true, portal = true,
  clearable, placeholder, isLoading, comboInputClassName, onBlur, comboOptionClassName, onScrollEnd
}: SelectFieldProps<T>, ref: ForwardedRef<HTMLInputElement>) {
  const [query, setQuery] = useState('')
  const [optionsRef, setOptionsRef] = useState<HTMLDivElement | null>(null)
  const [inputRef, setInputRef] = useState<HTMLElement | null>(null)
  const [showOptions, setShowOptions] = useState(false)
  const [onEndRef, setOnEndRef] = useState<HTMLDivElement | null>(null)
 

  const inputStyle = inputRef?.getBoundingClientRect()

  const filteredOptions =
    query === ''
      ? options
      : options?.filter((_opt) =>
        _opt.search
          .toLowerCase()
          .replace(/\s+/g, '')
          .includes(query.toLowerCase().replace(/\s+/g, ''))
      )
  
  const optionMap = useMemo(() => Object.fromEntries<Option<T>>(options.map((_opt) => [typeof _opt.value === "string" ? _opt.value : JSON.stringify(_opt.value), _opt])), [options])
  
  const handleOnChange = (args: Option<T> | Option<T>[] |  undefined) => {
    if (args === undefined) {
      onChange(undefined)
      setQuery("")
    } else if (isMulti) {
      const changeValue = Array.isArray(args) ? args.map((_arg) => _arg.value) : []
      onChange(changeValue)
    } else if (!Array.isArray(args)) {
      onChange(args?.value)
    }
  }

  const comboValue = useMemo(() => {
    let _value: Option<T> | Option<T>[] | undefined = undefined
    if (typeof value === "string") {
      _value = optionMap[value]
    } else if (!Array.isArray(value) && typeof value !== "object"){
      _value = optionMap[String(value)]
    } else if (Array.isArray(value)) {
      _value = value.filter(_v => typeof _v === "string" ? optionMap[_v] : optionMap[JSON.stringify(_v)]).map((_v) => typeof _v === "string" ? optionMap[_v] : optionMap[JSON.stringify(_v)]).filter(_v => _v !== undefined)
    }
    return _value
  }, [optionMap, options, value])

  const handleOnBlur = (e: FormEvent<HTMLInputElement>) => {
    if (e.currentTarget.value && optionMap[e.currentTarget.value]) {
      if (isMulti) {
        onChange([e.currentTarget.value] as T[])
      } else {
        onChange(e.currentTarget.value as T)
      }
    }
    onBlur && onBlur(e)
  }

  useEffect(() => {
    const onScroll = () => {
      const inputStyle = inputRef?.getBoundingClientRect() ?? null
      if (optionsRef) {
        optionsRef.children[0]?.setAttribute("style", `width: ${inputStyle?.width}px; top: ${inputStyle?.bottom}px; left: ${inputStyle?.left}px;`)
      }
    }
    if (showOptions && useFixed) {
      document?.addEventListener("scroll", onScroll, true)
    }

    return () => {
      if (showOptions && useFixed) {
        document?.removeEventListener("scroll", onScroll)
      }
    }
  }, [inputRef, showOptions, optionsRef])

  useEffect(() => {
    const option = options.find(opt => opt.value === value)
    if (!option && ref) {
      setQuery("")
    }
  }, [value, optionMap])

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          onScrollEnd && onScrollEnd()
        }
      })
    }, {
      threshold: 0.5
    })

    if (onEndRef) {
      observer.observe(onEndRef)
    }
    return () => {
      observer.disconnect()
    }
  }, [onEndRef, onScrollEnd])

  return (<div className={classNames(className)} >
    {/*@ts-ignore*/}
    <Combobox id={id} value={comboValue || (isMulti ? [] : null)} onChange={handleOnChange} as="div" multiple={isMulti} disabled={disabled}>
      {!hideLabel && <Label className="block text-sm font-medium leading-6 text-gray-900 dark:text-white">{label}</Label>}
      <div className="relative opacity-100">
        <div ref={setInputRef} className="relative" >
          <ComboboxInput
            ref={ref}
            className={classNames("w-full rounded-md border-0 bg-white dark:bg-gray-700 dark:text-white py-1.5 pl-3 pr-6 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-primary-600 sm:text-sm sm:leading-6 !overflow-hidden !overflow-x-hidden text-ellipsis", clearable ? "pr-14" : "pr-8", comboInputClassName)}
            onBlur={handleOnBlur}
            placeholder={placeholder}
            displayValue={(_opt: Option<T> | Option<T>[] | null) => {
              let displayValue: string = ""
              if (_opt && comboValue) {
                if (Array.isArray(_opt)) {
                  displayValue = _opt.map((_opt) => _opt.label).join(", ") 
                } else if (_opt) {
                  displayValue = _opt.label
                }  else {
                  displayValue = ""
                }
              }
              return displayValue
            }}
            onChange={(event) => {
              const valueArray = event.target.value.split(",")
              const lastValue = valueArray[valueArray.length - 1]?.trimEnd().trimStart() || ""
              setQuery(lastValue)
              onQueryChange && onQueryChange(lastValue)
            }}
            onKeyDown={(e) => {
              if (creatable && onCreate && ["Comma", "Tab"].includes(e.code)) {
                const valueArray = query.split(",")
                const lastValue = valueArray[valueArray.length - 1]?.trimEnd().trimStart() || ""
                onCreate(lastValue)
              }
            }}
          />
          <div className="w-fit absolute inset-y-1 right-[1px] flex items-center">
            {clearable && <XMarkIcon className="w-4 h-8 text-gray-900 dark:text-white cursor-pointer" onClick={() => handleOnChange(undefined)} />}
            <ComboboxButton
              onClick={() => setShowOptions(!showOptions)}
              className="flex items-center rounded-r-md px-2 focus:outline-none"
            >
              <span className="flex">
                {error &&

                    <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />

                }
                <ChevronUpDownIcon
                  className="h-5 w-5 text-gray-400"
                  aria-hidden="true"
                />
              </span>
            </ComboboxButton>
          </div>
        </div>
        { error &&
            <p className="mt-2 text-sm text-red-600 text-left" id={`${name}-error`}>
            {error}
            </p>
          }

          <div ref={setOptionsRef}  >
          <ComboboxOptions
            anchor={portal && "bottom"}
            transition
            portal={portal}
            style={{width: inputStyle?.width || 0}}
            className={classNames("empty:invisible z-40 overflow-scroll mt-1 max-h-[20px] w-full overflow-auto rounded-md bg-white dark:bg-gray-700  py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm origin-top border transition duration-200 ease-out empty:invisible data-[closed]:scale-95 data-[closed]:opacity-0", comboOptionClassName)}
          >
            {filteredOptions.length === 0 && query !== '' ? (
              <div className="relative cursor-default select-none px-4 py-2 text-gray-700" onClick={() => (creatable && onCreate) ? onCreate(query) : undefined}>
                {isLoading && <span className="flex text-xs items-end mr-w fill-gray-500 text-gray-500 hover:text-gray-800"><Spinner size="sm"/> Loading</span>}
                {creatable ? 
                  <span className="flex text-xs items-end mr-w fill-gray-500 text-gray-500 hover:text-gray-800"><PlusIcon className="w-4 h-auto mr-1"/> Create</span> 
                  : 
                  (isLoading ? false : <span className="flex text-xs items-end mr-w fill-gray-500 text-gray-500">Nothing found</span>)
                  }
              </div>
            ) : (
              filteredOptions.map((_opt) => (
                <ComboboxOption
                  key={String(_opt.value)}
                  className={({ active, selected }) => classNames("relative cursor-default select-none py-2 pl-4 pr-4", selected ? 'bg-primary-600 dark:bg-gray-600 text-white' : 'text-gray-900 dark:text-gray-500', selected)
                  }
                  value={_opt}
                >
                  {({ selected, active }) => (
                    <>
                      <span
                        className={`block truncate ${selected ? 'font-medium mr-2' : 'font-normal'
                          }`}
                      >
                        {_opt.menuItem ?  _opt.menuItem : _opt.label}
                      </span>
                      {selected ? (
                        <span
                          className={classNames(
                            'absolute inset-y-0 right-0 flex items-center pr-2',
                            active ? 'text-white' : 'text-primary-600 dark:text-primary-400'
                          )}
                        >
                          <CheckIcon className="h-5 w-5" aria-hidden="true" />
                        </span>
                      ) : null}
                    </>
                  )}
                </ComboboxOption>
              ))
            )}
            {onScrollEnd && <div className="w-full h-4" ref={setOnEndRef} >

            </div>}
          </ComboboxOptions>
          </div>

      </div>
    </Combobox>
    </div>)
}

export const SelectField = forwardRef(SelectFieldCMP) as typeof SelectFieldCMP