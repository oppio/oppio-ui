
import { ChevronDownIcon } from "@heroicons/react/24/outline";
import { ReactElement } from "react";
import { ButtonColor, ButtonProps, ButtonSize, ButtonStyle, buttonClassStyle } from "../Button";
import { classNames } from "../../server-side"
import {Menu, MenuItem, MenuTrigger, Popover, MenuItemProps, Key, Button, PopoverProps, MenuProps} from 'react-aria-components';
type MenuItem = Omit<MenuItemProps, "children"> & {
    children: ReactElement
    key: Key,
    disabled?: boolean
}

export type DropMenuProps = {
    ariaLabel: string
    menuItems: MenuItem[]
    buttonText?: string
    className?: string
    onAction?: (key: Key) => void
    popOverProps?: PopoverProps
} & Pick<ButtonProps, "color" | "buttonSize" | "buttonStyle" | "disabled"> & MenuProps<any>

export function DropMenu({
    menuItems, buttonText, className,
    buttonSize = ButtonSize.MD, buttonStyle = ButtonStyle.STANDARD, color = ButtonColor.PRIMARY,
    disabled = false, ariaLabel, onAction, popOverProps, ...menuProps
}: DropMenuProps) {
    return <MenuTrigger >
    <Button aria-label={ariaLabel} className={buttonClassStyle("", color, buttonSize, buttonStyle, disabled)}>
        <span className="flex items-center gap-1">
        {buttonText}
        <ChevronDownIcon className="w-4 h-4" />
        </span>
    </Button>
    <Popover {...popOverProps}>
    <Menu {...menuProps} disabledKeys={["filter"]}  onAction={onAction} className="w-56 shrink rounded-xl bg-white dark:bg-gray-800 p-4 text-sm font-semibold leading-6 text-gray-900 shadow-lg ring-1 ring-gray-900/5">
      {menuItems.map(({children, ...item}) => {
        return <MenuItem  
            {...item}
            key={item.key}
            isDisabled={item.disabled}
            id={`${ariaLabel}-${item.key}`}
            className={({isHovered, isSelected, isFocused, isFocusVisible, isDisabled}) => {
                return classNames(
                    "p-2 rounded",
                    isDisabled ? "cursor-not-allowed text-gray-300 bg-gray-50 dark:text-gray-100 dark:bg-gray-700 dark:text-gray-500" : "cursor-pointer dark:text-white",
                    (isFocused || isFocusVisible || isHovered) && "bg-primary-50 text-primary-700 dark:bg-gray-600"
                )
            }}
        >
        <div>
            {children}
        </div>
      </MenuItem>
      })}
    </Menu>
    </Popover>
</MenuTrigger>
}