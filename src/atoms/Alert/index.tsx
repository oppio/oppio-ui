import React from "react"
import { AlertType } from "./types"
import { ExclamationTriangleIcon, XCircleIcon, CheckCircleIcon, InformationCircleIcon, EyeIcon } from '@heroicons/react/20/solid'

const alertContainerStyle = (alertType?: AlertType, className?: string) => {
  let style = "rounded-md p-4"
  switch (alertType){
    case AlertType.SUCCESS:
      style += " bg-green-50 dark:bg-gray-800 dark:border dark:border-green-300"
      break
    case AlertType.WARNING:
      style += " bg-yellow-50 dark:bg-gray-800 dark:border dark:border-yellow-300"
      break
    case AlertType.ERROR:
      style += " bg-red-50 dark:bg-gray-800 dark:border dark:border-red-300"
      break
    case AlertType.INFO:
      style += " bg-blue-50 dark:bg-gray-800 dark:border dark:border-blue-300"
      break
    default:
      style += " bg-neutral-50 dark:bg-gray-800 dark:border dark:border-gray-300"
  }
  style += className ? ` ${className}` : ''
  return style
}

const alertIconStyle = (alertType?: AlertType) => {
  let style = "h-5 w-5" //h-5 w-5 text-yellow-400
  switch (alertType){
    case AlertType.SUCCESS:
      style += " text-green-500 dark:text-green-300"
      break
    case AlertType.WARNING:
      style += " text-yellow-400 dark:text-yellow-300"
      break
    case AlertType.ERROR:
      style += " text-red-400 dark:text-red-300"
      break
    case AlertType.INFO:
      style += " text-blue-400 dark:text-blue-300"
      break
    default:
      style += " text-neutral-400 dark:text-gray-300" 
}
  return style
}

const alertTitleStyle = (alertType?: AlertType) => {
  let style = "text-sm font-medium"
  switch (alertType){
    case AlertType.SUCCESS:
      style += " text-green-800 dark:text-green-300"
      break
    case AlertType.WARNING:
      style += " text-yellow-800 dark:text-yellow-300"
      break
    case AlertType.ERROR:
      style += " text-red-800 dark:text-red-300"
      break
    case AlertType.INFO:
      style += " text-blue-800 dark:text-blue-300"
      break
    default:
      style += " text-neutral-800 dark:text-neutral-300" 
}
  return style
}

const alertBodyStyle = (alertType?: AlertType, hasAlertTitle?: boolean) => {
  let style = "text-sm"
  switch (alertType){
    case AlertType.SUCCESS:
      style += " text-green-700  dark:text-green-300"
      break
    case AlertType.WARNING:
      style += " text-yellow-700 dark:text-yellow-300"
      break
    case AlertType.ERROR:
      style += " text-red-700 dark:text-red-300"
      break
    case AlertType.INFO:
      style += " text-blue-700 dark:text-blue-300"
      break
    default:
      style += " text-neutral-700 dark:text-neutral-300" 
}
  return `${style}${hasAlertTitle ? " mt-2" : ""}`
}

export type AlertProps = {
  alertType?: AlertType,
  children: string | React.ReactElement
  className?: string
  alertTitle?: string
}
//
const AlertIcon = ({alertType}: {alertType?: AlertType}) => {
  switch (alertType){
    case AlertType.SUCCESS:
      return <CheckCircleIcon className={alertIconStyle(alertType)} aria-hidden="true" />
    case AlertType.WARNING:
      return <ExclamationTriangleIcon className={alertIconStyle(alertType)} aria-hidden="true" />
    case AlertType.ERROR:
      return <XCircleIcon className={alertIconStyle(alertType)} aria-hidden="true" />
    case AlertType.INFO:
      return <InformationCircleIcon className={alertIconStyle(alertType)} aria-hidden="true" />
    default:
      return <EyeIcon className={alertIconStyle(alertType)} aria-hidden="true" />
}
}

export function Alert({alertType, children, className, alertTitle}: AlertProps){
  
  return <div className={alertContainerStyle(alertType, className)}>
  <div className="flex">
    <div className="flex-shrink-0">
      <AlertIcon alertType={alertType} />
    </div>
    <div className="ml-3">
      {alertTitle &&
        <h3 className={alertTitleStyle(alertType)}>{alertTitle}</h3>
      }
      <div className={alertBodyStyle(alertType, !!alertTitle)}>
        {children}
      </div>
    </div>
  </div>
</div>
}
