import { classNames } from "server-side";

export enum AvatarSize {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

export type AvatarProps = {
  src?: string;
  onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
  firstName?: string
  lastName?: string
  size?: AvatarSize
  className?: string
  style?: React.CSSProperties
  id?: string
};

const getClassName = (size: AvatarSize): string => {
  const style = "rounded-full ";
  switch (size) {
    case AvatarSize.SMALL:
      return style + 'w-6 h-6 text-xs';
    case AvatarSize.MEDIUM:
      return style + 'w-8 h-8 text-sm';
    case AvatarSize.LARGE:
      return style + 'w-12 h-12 text-xl';
    default:
      return style + 'w-8 h-8';
  }
}

export function Avatar({
  src,
  id,
  firstName,
  lastName,
  onClick,
  size = AvatarSize.MEDIUM,
  className,
  style
}: AvatarProps): React.ReactElement {
  const classNameCalc = getClassName(size);
  return src ? <img
  onClick={onClick}
  className={className}
  src={src}
  alt="user avatar"
  style={style}
/>
:
<div id={id} style={style} className={classNames(classNameCalc, "bg-primary-400 dark:bg-primary-700 text-gray-900 text-white font-semibold flex items-center justify-center", className)}>
  {firstName ? firstName[0] :
  ''}{lastName ? lastName[0] : ''}
</div>
}
