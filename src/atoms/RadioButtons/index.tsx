import { ReactElement, useEffect, useState } from 'react'
import { RadioGroup, RadioGroupDescription, RadioGroupLabel, RadioGroupOption } from '@headlessui/react'
import { classNames } from '../../server-side'
import { InputFieldProps } from '../InputField'
import {Typography} from '../Typography'

export type RadioOption = {
  value: string
  name: string | ReactElement
  description?: string
  trailingIcon?: ReactElement
}



export type RadioButtonsProps = {
  options: RadioOption[]
  initial?: string[]
  multiSelect?: boolean
  onChange?: (value: string[]) => void
  error?: string
} & Pick<InputFieldProps, "label" | "hideLabel">


export function RadioButtons({ initial = [], options, multiSelect = false, label, hideLabel, onChange, error }: RadioButtonsProps) {
  const [selected, setSelected] = useState<string[] | null>(initial)

  const handleOnChange = (value: string) => {
    if (value) {
      if (selected?.includes(value)) {
        setSelected(selected?.filter(_sel => _sel !== value))
      } else if (multiSelect && selected !== null) {
        setSelected([...selected, value])
      } else {
        setSelected([value])
      }
    }
  }

  useEffect(() => {setSelected(initial)}, [JSON.stringify(initial)])

  useEffect(() => {
    onChange && selected && onChange(selected)
  }, [selected])

  return (
    <RadioGroup onChange={(val) => handleOnChange(val)}>
      <RadioGroupLabel className={hideLabel ? "sr-only" : "block text-sm font-medium leading-6 text-gray-900 dark:text-white text-left mb-2"}>{label}</RadioGroupLabel>
      {error && <Typography className='text-red-500'>{error}</Typography>}
      <div className="-space-y-px rounded-md bg-white dark:bg-gray-700">
        {options.map((_opt, idx) => {
          const checked = !!selected?.includes(_opt.value)
          return <RadioGroupOption
            key={_opt.value}
            value={_opt.value}
            onClick={() => handleOnChange(_opt.value)}
            className={({ }) =>
              classNames(
                idx === 0 ? 'rounded-tl-md rounded-tr-md' : '',
                idx === options.length - 1 ? 'rounded-bl-md rounded-br-md' : '',
                checked ? 'z-10 border-primary-200 bg-primary-50' : 'border-gray-200',
                'relative flex cursor-pointer border p-4 focus:outline-none'
              )
            }
          >
            {({ active }) => {

              return <>
                <span
                  className={classNames(
                    checked ? 'bg-primary-600 border-transparent' : 'bg-white dark:bg-gray-700 border-gray-300',
                    active ? 'ring-2 ring-offset-2 ring-primary-600' : '',
                    'mt-0.5 h-4 w-4 shrink-0 cursor-pointer rounded-full border flex items-center justify-center'
                  )}
                  aria-hidden="true"
                >
                  <span className="rounded-full bg-white dark:bg-gray-700 w-1.5 h-1.5" />
                </span>
                <span className="ml-3 flex justify-between w-full items-center">
                  <span className="flex flex-col">
                    <RadioGroupLabel
                      as="span"
                      className={classNames(checked ? 'text-primary-900' : 'text-gray-900 dark:text-white', 'block text-sm font-medium')}
                    >
                      {_opt.name}
                    </RadioGroupLabel>
                    <RadioGroupDescription
                      as="span"
                      className={classNames(checked ? 'text-primary-700' : 'text-gray-500 dark:text-gray-400', 'block text-sm')}
                    >
                      {_opt.description}
                    </RadioGroupDescription>
                  </span>
                  {!!_opt.trailingIcon && _opt.trailingIcon}
                </span>
              </>
            }}
          </RadioGroupOption>
        })}
      </div>
    </RadioGroup>
  )
}