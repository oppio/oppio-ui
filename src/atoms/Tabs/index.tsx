import { useRef, useState, useEffect } from "react";
import { classNames } from "../../server-side"
import { Tab, TabGroup, TabList, TabPanel, TabPanelProps, TabPanels } from "@headlessui/react";

export type TabsProps = {
  name: string;
  tabs: {label: string, disabled?: boolean}[];
  children: (React.ReactElement | boolean) | (React.ReactElement | boolean)[];
  initialTab?: number;
  currentTab?: number
  className?: string;
  backgroundActive?: string
  clickNavigation?: boolean
  onTabClick?: (value: number) => void
  tabListStyle?: string
  keepMounted?: boolean
  props?: {
    tabPanel?: TabPanelProps
    tabPanels?: Omit<TabPanelProps, "className"> & {className?: string}
  }
};


export function Tabs({
  name,
  tabs,
  children,
  initialTab = 0,
  className,
  backgroundActive = "bg-white",
  onTabClick,
  currentTab,
  tabListStyle,
  props,
  keepMounted = false
}: TabsProps) {
  const [activeTab, setActiveTab] = useState<number>(initialTab);
  const tabList = useRef<HTMLDivElement>(null)
  const tabUnderline = useRef<HTMLDivElement>(null)

  const setTab = (idx: number) => {
    setActiveTab(idx);
    const child = tabList.current?.children[idx] as HTMLDivElement
    if (tabList.current?.children[idx]) {
      tabUnderline.current?.setAttribute(
        "style", `width: ${tabList.current?.children[idx].clientWidth}px; left: ${child?.offsetLeft}px`
      )
    }
  }

  useEffect(() => {
    const child = tabList.current?.children[initialTab] as HTMLDivElement
    if (child) {
      tabUnderline.current?.setAttribute(
        "style", `width: ${tabList.current?.children[initialTab]?.clientWidth ?? 0}px; left: ${child?.offsetLeft}px`
      )
    }
  },[initialTab])

  useEffect(() => {
    if (currentTab !== undefined) {
      setTab(currentTab)
    }
  }, [currentTab])

  return (
    <TabGroup as="div" className={className} defaultIndex={initialTab} selectedIndex={activeTab}>
      <div className={classNames("border-b border-gray-200", tabListStyle)}>
      <TabList className={classNames("-mb-px flex overflow-x-scroll")} aria-label="Tabs">
        {tabs.map((_tab, idx) => {
          return <Tab
            as="div"
            key={_tab.label}
            onClick={() => {
              onTabClick && onTabClick(idx)
              setActiveTab(idx)
            }}
            className={classNames(
              idx === activeTab
                ? 'border-primary-500 text-primary-600 dark:text-primary-400 dark:border-primary-300'
                : 'border-transparent text-gray-500 dark:text-gray-400 hover:border-gray-300 dark:hover:border-gray-200 hover:text-gray-700 dark:hover:text-gray-300',
              'whitespace-nowrap border-b-2 py-4 px-4 text-sm font-medium focus-visible:outline-0 cursor-pointer'
            )}
            aria-current={idx === activeTab ? 'page' : undefined}
          >
            {_tab.label}
          </Tab>
        })}
      </TabList>
      </div>
      <TabPanels unmount={!keepMounted}  {...(props?.tabPanel ?? {})} className={classNames("pt-4", props?.tabPanels?.className)}>
        {Array.isArray(children) && children.map((_child, idx) => {
          return <TabPanel unmount={!keepMounted}  key={tabs[idx]?.label} {...(props?.tabPanel ?? {})}>
            {_child}
          </TabPanel>
        })}
      </TabPanels>
    </TabGroup>
  );
}
