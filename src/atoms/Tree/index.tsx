import React, { useState, useRef, HTMLProps, forwardRef, ForwardedRef } from "react";
import { DndProvider } from "react-dnd";
import {
  Tree,
  NodeModel,
  TreeMethods,
  MultiBackend,
  getBackendOptions
} from "@minoru/react-dnd-treeview";
import { Button } from "../Button";
import { TextAreaField } from "atoms/TextAreaField";

function CustomNode<T>({node}: {
    node: NodeModel<T>;
    depth: number;
    isOpen: boolean;
    onToggle: () => void;
}) {
    return <div className="ml-4">{node.data.name}</div>
}

const StyledUI = forwardRef((props: HTMLProps<HTMLUListElement>, ref: ForwardedRef<HTMLUListElement>) => {
    return <ul {...props} ref={ref} className="ml-4" />;
})

export default function App<T>({nodes, setNodes}: {nodes: NodeModel<T>[], setNodes: (nodes: NodeModel<T>[]) => void}) {
  const [text, setText] = useState<string>("");
  const ref = useRef<TreeMethods>(null);
  const handleDrop = (newTree: NodeModel<T>[]) => setNodes(newTree);
  const handleChangeText = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value);
  };

  const getIds = (text: string) => {
    let idTexts = text.split(",").map((id) => id.trim());
    idTexts = idTexts.filter((id) => id !== "");
    let ids = idTexts.map((id) => Number(id));
    ids = ids.filter((id) => !isNaN(id));
    return ids;
  };

  const handleOpenAll = () => ref.current?.openAll();
  const handleCloseAll = () => ref.current?.closeAll();
  const handleOpenSpecified = () => ref.current?.open(getIds(text));
  const handleCloseSpecified = () => ref.current?.close(getIds(text));

  return (
    <DndProvider backend={MultiBackend} options={getBackendOptions()}>
          <div className="-ml-8">
          <Tree
            ref={ref}
            tree={nodes}
            listComponent={StyledUI}
            rootId={0}
            render={(
              node: NodeModel<T>,
              { depth, isOpen, onToggle }
            ) => (
              <CustomNode
                node={node}
                depth={depth}
                isOpen={isOpen}
                onToggle={onToggle}
              />
            )}
            onDrop={handleDrop}
          />
          </div>
      </DndProvider>
  );
}
