
import { classNames } from "../../server-side"
import { ReactElement, MouseEventHandler } from "react"


export enum BadgeColor {
    GREY = "GREY",
    RED = "RED",
    YELLOW = "YELLOW",
    GREEN = "GREEN",
    BLUE = "BLUE",
    INDIGO = "INDIGO",
    PURPLE = "PURPLE",
    PINK = "PINK",
}

export enum BadgeStyle {
    DEFAULT = "DEFAULT",
    PILL = "PILL"
}

export type BadgeProps = {
    id?: string;
    children: string | ReactElement
    color?: BadgeColor
    hasDot?: boolean
    badgeStyle?: BadgeStyle
    onRemove?: () => void
    outline?: boolean
    small?: boolean
    className?: string
    onClick?: MouseEventHandler<HTMLSpanElement>
}

const badgeCmpStyle = (badgeColor: BadgeColor, hasDot: boolean, badgeStyle: BadgeStyle, outline: boolean, small: boolean) => {
    let style = "inline-flex items-center text-xs"
    switch (badgeColor) {
        case BadgeColor.GREY:
            style += !hasDot ? " text-gray-600 ring-gray-500/10 bg-gray-50 dark:bg-gray-400/10 dark:text-gray-400 dark:ring-gray-400/20 fill-gray-500" : " fill-gray-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
        case BadgeColor.RED:
            style += !hasDot ? " text-red-600 ring-red-500/10 bg-red-50 dark:bg-red-400/10 dark:text-red-400 dark:ring-red-400/20 fill-red-500" : " fill-red-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
        case BadgeColor.YELLOW:
            style += !hasDot ? " text-yellow-600 ring-yellow-500/10 bg-yellow-50 dark:bg-yellow-400/10 dark:text-yellow-400 dark:ring-yellow-400/20 fill-yellow-500" : " fill-yellow-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
        case BadgeColor.GREEN:
            style += !hasDot ? " text-green-600 ring-green-500/10 bg-green-50 dark:bg-green-400/10 dark:text-green-400 dark:ring-green-400/20 fill-green-500" : " fill-green-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
        case BadgeColor.BLUE:
            style += !hasDot ? " text-blue-600 ring-blue-500/10 bg-blue-50 dark:bg-blue-400/10 dark:text-blue-400 dark:ring-blue-400/20 fill-blue-500" : " fill-blue-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
        case BadgeColor.INDIGO:
            style += !hasDot ? " text-indigo-600 ring-indigo-500/10 bg-indigo-50 dark:bg-indigo-400/10 dark:text-indigo-400 dark:ring-indigo-400/20  fill-indigo-500" : " fill-indigo-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
        case BadgeColor.PURPLE:
            style += !hasDot ? " text-purple-600 ring-purple-500/10 bg-purple-50 dark:bg-purple-400/10 dark:text-purple-400 dark:ring-purple-400/20  fill-purple-500" : " fill-purple-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
        case BadgeColor.PINK:
            style += !hasDot ? " text-pink-600 ring-pink-500/10 bg-pink-50 dark:bg-pink-400/10 dark:text-pink-400 dark:ring-pink-400/20 fill-pink-500" : " fill-pink-500 text-gray-900 ring-gray-200 dark:text-white dark:ring-gray-800"
            break
    }
    switch (badgeStyle) {
        case BadgeStyle.DEFAULT:
            style += " rounded-md"
            break
        case BadgeStyle.PILL:
            style += " rounded-full"
    }
    return classNames(style, outline ? "ring-1 ring-inset gap-x-1.5" : "", small ? "px-1.5 py-0.5 text-[10px]" : "px-2 py-1")
}

const badgeRemoveButtonStyle = (badgeColor: BadgeColor) => {
    let style = "group relative -mr-1 h-3.5 w-3.5 rounded-sm"
    switch (badgeColor) {
        case BadgeColor.GREY:
            style += " hover:bg-gray-500/20 stroke-gray-600/50 group-hover:stroke-gray-600/75"
            break
        case BadgeColor.RED:
            style += " hover:bg-red-500/20 stroke-red-600/50 group-hover:stroke-red-600/75"
            break
        case BadgeColor.YELLOW:
            style += " hover:bg-yellow-500/20 stroke-yellow-600/50 group-hover:stroke-yellow-600/75"
            break
        case BadgeColor.GREEN:
            style += " hover:bg-green-500/20 stroke-green-600/50 group-hover:stroke-green-600/75"
            break
        case BadgeColor.BLUE:
            style += " hover:bg-blue-500/20 stroke-blue-600/50 group-hover:stroke-blue-600/75"
            break
        case BadgeColor.INDIGO:
            style += " hover:bg-indigo-500/20 stroke-indigo-600/50 group-hover:stroke-indigo-600/75"
            break
        case BadgeColor.PURPLE:
            style += " hover:bg-purple-500/20 stroke-purple-600/50 group-hover:stroke-purple-600/75"
            break
        case BadgeColor.PINK:
            style += " hover:bg-pink-500/20 stroke-pink-600/50 group-hover:stroke-pink-600/75"
            break
    }
    return style
}

const dotStyle = (color: BadgeColor) => {
    switch (color) {
        case BadgeColor.GREY:
            return "fill-gray-500"
        case BadgeColor.RED:
            return "fill-red-500"
        case BadgeColor.YELLOW:
            return "fill-yellow-500"
        case BadgeColor.GREEN:
            return "fill-green-500"
        case BadgeColor.BLUE:
            return "fill-blue-500"
        case BadgeColor.INDIGO:
            return "fill-indigo-500"
        case BadgeColor.PURPLE:
            return "fill-purple-500"
        case BadgeColor.PINK:
            return "fill-pink-500"
    }
}

export function Badge({
    color = BadgeColor.GREY, hasDot = false, badgeStyle = BadgeStyle.DEFAULT,
    onRemove, outline = true, small = false, children, className, onClick, id
}: BadgeProps) {
    return <span id={id} onClick={onClick} className={classNames(badgeCmpStyle(color, hasDot, badgeStyle, outline, small), className)}>
        {hasDot &&
            <svg className={classNames("h-1.5 w-1.5", dotStyle(color))} viewBox="0 0 6 6" aria-hidden="true">
                <circle cx={3} cy={3} r={3} />
            </svg>
        }
        {children}
        {onRemove &&
            <button type="button" className={badgeRemoveButtonStyle(color)} onClick={onRemove}>
                <span className="sr-only">Remove</span>
                <svg viewBox="0 0 14 14" className="h-3.5 w-3.5 stroke-gray-600/50 group-hover:stroke-gray-600/75">
                    <path d="M4 4l6 6m0-6l-6 6" />
                </svg>
                <span className="absolute -inset-1" />
            </button>
        }
    </span>
}