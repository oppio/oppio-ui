
import { ForwardedRef, HTMLProps, ReactElement, forwardRef } from "react";
import { ExclamationCircleIcon } from '@heroicons/react/20/solid'
import { classNames } from "../../server-side"
import { PhotoIcon } from "@heroicons/react/24/outline";
import { useDropzone, Accept, DropzoneOptions, DropzoneInputProps } from "react-dropzone-esm";

export enum ImageFieldSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large",

}

export interface ImageFieldProps extends Omit<HTMLProps<HTMLInputElement>, "media" | "size" | "accept" | "value" | "onDrop"> {
  label: string
  hideLabel?: boolean
  error?: string
  media?: ReactElement
  helpText?: string
  trailingIcon?: ReactElement
  leadingIcon?: ReactElement
  inputClassName?: string
  size?: ImageFieldSize
  preview?: string
}

function _ImageField({
  label, error, media, name, helpText, hideLabel, trailingIcon, preview,
  leadingIcon, className, inputClassName, size = ImageFieldSize.MEDIUM, accept, multiple = false,
  onDropAccepted, onDropRejected, onDrop, onError, maxFiles, maxSize, minSize, ...props
}: ImageFieldProps & DropzoneOptions) {
  const {getRootProps, getInputProps, isDragActive, acceptedFiles} = useDropzone(
    {
      onDrop,
      onDropAccepted,
      onDropRejected,
      onError,
      accept,
      maxFiles,
      maxSize,
      minSize
    }
  )

  const id = `id-${label}`
  return (
    <div className={className}>
      <label htmlFor={name} className={hideLabel ? "sr-only" : "block text-sm font-medium leading-6 text-gray-900 dark:text-white text-left"}>
        {label}
      </label>
      <div className={`${(error || trailingIcon || leadingIcon) ? " relative rounded-md shadow-sm" : ""}`}>
        {leadingIcon &&
          <div className={`pointer-events-none absolute inset-y-0${(leadingIcon) ? " left-2 " : ""}flex items-center pl-3`}>
            {leadingIcon}
          </div>
        }
        <div
          {...getRootProps()}
          className={classNames("border-gray-200 border-dashed border-2 rounded-lg flex items-center justify-center hover:border-gray-400 hover:cursor-pointer p-1", size == ImageFieldSize.SMALL && "w-8 h-8", size == ImageFieldSize.MEDIUM && "w-16 h-16", size == ImageFieldSize.LARGE && "w-24 h-24", isDragActive ? "border-primary-600" : "border-gray-300", error ? "border-red-500" : "")}
        >
          {
          (acceptedFiles.length === 0 && !preview) ?
          <PhotoIcon className="h-6 w-6 m-auto text-gray-400" aria-hidden="true" />
          :
          <div className="flex flex-wrap gap-2 rounded">{
          acceptedFiles.length > 0 ?
          acceptedFiles.map((file, index) => <img key={index} src={URL.createObjectURL(file)} alt={file.name} className="w-full h-full rounded" />)
          : <img src={preview} alt="preview" className="w-full h-full rounded"  />
          }
          </div>
          }
        </div>
        <input
          {...getInputProps({refKey: "ref", type: "file", name, id, ...props })}
          className={classNames(`hidden px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-primary-600 dark:bg-gray-700 dark:text-white disabled:cursor-not-allowed disabled:bg-gray-50 disabled:text-gray-500 disabled:ring-gray-200 sm:text-sm sm:leading-6 ${(trailingIcon || error) ? "pr-10" : ((trailingIcon && error) ? "pr-20" : "")} ${(leadingIcon) ? "pl-10" : ""}`, inputClassName)}
        />
        {trailingIcon &&
          <div className={`pointer-events-none absolute inset-y-0 ${(trailingIcon && error) ? "right-4" : "right-0"} flex items-center pr-3`}>
            {trailingIcon}
          </div>
        }
        {error &&
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
            <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
          </div>
        }
      </div>
      {(helpText && !error) &&
        <p className="mt-2 text-sm text-gray-500" id={`${name}-description`}>
          {helpText}
        </p>
      }
      {error &&
        <p className="mt-2 text-sm text-red-600 text-left" id={`${name}-error`}>
          {error}
        </p>
      }
    </div>
  )
}

export const ImageField = _ImageField
