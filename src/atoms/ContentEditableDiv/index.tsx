import { ChangeEvent, useEffect, useState,  KeyboardEvent } from "react"
import { ExclamationCircleIcon } from "@heroicons/react/24/outline"

export type ContentEditableDivProps = { 
  value: string, placeholder: string,
  onChange: (value: string) => void, editable?: boolean,
  error?: string
  className?: string
}

export const ContentEditableDiv = ({ 
    value, onChange, editable = true, placeholder, error, className
  }: ContentEditableDivProps) => {

    const [ref, setRef] = useState<HTMLDivElement | null>(null)
    const [isFocus, setIsFocus] = useState(false)

    useEffect(() => {
      if (ref && value && !isFocus) {
        ref.textContent = value
      }
      // NOTE: WE DO NO PUT VALUE INTO THE DEP ARRAY BECAUSE WE ONLY WANT TO DO THIS IF THE REF IS DEFINED
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ref, value, isFocus])



    const onSpaceBar = (e: KeyboardEvent) => {
      const ele = e.currentTarget as HTMLDivElement | null
      if (ref && e.code === "Space" && ele?.textContent) {
          e.preventDefault()
          e.stopPropagation()
          onChange((ele.textContent || "") + " ")
          let el = ref
          let range = document.createRange()
          let sel = document.getSelection()
          const focusOffset = sel?.focusOffset || 0
          const text = ele.textContent
          const output = focusOffset < text.length ? [text.slice(0, focusOffset),  " ", text.slice(focusOffset)].join('') : text + " "
          ele.textContent = output
          if (sel && focusOffset) {
            range.setStart(el.childNodes[0], focusOffset + 1)
            range.collapse(true)
            sel.removeAllRanges()
            sel.addRange(range)
          }
      }
    }
  
    return <div className={className} onMouseDown={(e) => e.stopPropagation()} onClick={(e) =>  e.stopPropagation()}>
      <div className="relative flex gap-1">
      <div
        ref={setRef}
        contentEditable={editable}
        onKeyDown={onSpaceBar}
        onInput={(e: ChangeEvent<HTMLDivElement>) => {
          if (e.target.textContent) {
            onChange(e.target.textContent !== null ?  e.target.textContent.trimStart() : "")
          }
        }}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        className="min-w-[140px] pr-12 max-w-[162px] relative z-10 overflow-scroll text-nowrap no-scrollbar dark:text-white whitespace-pre break-words"
      />
      {!value && <div className="text-gray-400 absolute top-0 left-1 z-0 min-w-[132px] overflow-hidden truncate dark:text-gray-200">
        {placeholder}
      </div>
      }
      {error &&
            <div className="pointer-events-none inset-y-0 right-0 flex items-center pr-3">
              <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
            </div>
          }
      
    </div>
    { error &&
      <p className="mt-2 text-sm text-red-600" id={`${name}-error`}>
        {error}
      </p>
    }
    </div>
  }
