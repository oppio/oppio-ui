import { classNames } from "server-side"

export enum SkeletonPattern {
    Input = "input",
    Card = "card",
    TextArea = "textarea",
}

export function Skeleton({ pattern, className }: { pattern: SkeletonPattern, className?: string }) {
    return <div className={classNames("w-full rounded-md h-full duration-1000 animate-pulse p-1 overflow-hidden", className)}>
        <SkeletonPatter pattern={pattern} />
    </div>
}

function SkeletonPatter({ pattern }: { pattern: SkeletonPattern }) {
    switch (pattern) {
        case SkeletonPattern.Input:
            return <div className="rounded-md w-full h-full bg-gray-700/10 dark:bg-gray-300/10 " />
        case SkeletonPattern.Card:
            return <div className="flex flex-col space-y-4 p-2">
                <div className="flex gap-4 items-center">
                    <div className="rounded-full bg-gray-700/10 dark:bg-gray-300/10 h-10 w-10"></div>
                    <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded w-full"></div>
                </div>
                <div className="flex-1 space-y-6 py-1">
                    <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded"></div>
                    <div className="space-y-3">
                        <div className="grid grid-cols-3 gap-4">
                            <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded col-span-2"></div>
                            <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded col-span-1"></div>
                        </div>
                        <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded"></div>
                    </div>
                </div>
            </div>
        case SkeletonPattern.TextArea:
            return <div className="flex flex-col gap-1 p-1">
                <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded w-full"></div>
                <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded w-full"></div>
                <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded w-full"></div>
                <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded w-full"></div>
                <div className="h-2 bg-gray-700/10 dark:bg-gray-300/10 rounded w-full"></div>
            </div>
    }
}