import { classNames } from "../../server-side"
import React, { ForwardedRef, HTMLProps, ReactElement, forwardRef } from "react"


export enum ButtonColor {
  PRIMARY = "primary",
  SECONDARY = "secondary",
  SOFT = "soft",
  NONE = "none"
}

export enum ButtonSize {
  XS = "XS",
  SM = "SM",
  MD = "MD",
  LG = "LG",
  XL = "XL",
}

export enum ButtonStyle {
  STANDARD = "STANDARD",
  ROUNDED = "ROUNDED",
  NONE = "NONE"
}

export type ButtonProps = HTMLProps<HTMLButtonElement> & {
  leadingIcon?: ReactElement
  trailingIcon?: ReactElement
  type?: "submit" | "button"
  buttonSize?: ButtonSize
  color?: ButtonColor
  buttonStyle?: ButtonStyle
}


export const buttonClassStyle = (className: string | undefined, color: ButtonColor, buttonSize: ButtonSize, buttonStyle: ButtonStyle, disabled: boolean) => {
let style = ""
  switch (color) {
    case ButtonColor.PRIMARY:
      style += !disabled ? " text-white bg-primary-600 hover:bg-primary-500 focus-visible:outline-primary-600" : "bg-gray-300 text-gray-500" 
      break
    case ButtonColor.SECONDARY:
      style +=  !disabled ? " bg-white dark:bg-white/10 text-gray-900 dark:text-white ring-1 dark:ring-0 ring-inset ring-gray-300 hover:bg-gray-50 dark:hover:bg-white/20" : "bg-gray-300 text-gray-500"
      break
    case ButtonColor.SOFT:
      style += !disabled ? " bg-primary-50 dark:bg-gray-500 text-primary-600 dark:text-primary-100 hover:bg-primary-100 dark:hover:bg-primary-500" : "bg-gray-300 text-gray-500"
      break
  }
  switch (buttonSize) {
    case ButtonSize.XS:
      style += " px-2 py-1 text-xs"
      break
    case ButtonSize.SM:
      style += " px-2 py-1 text-sm"
      break
    case ButtonSize.MD:
      style += " px-2.5 py-1.5 text-sm"
      break
    case ButtonSize.LG:
      style += " px-3 py-2 text-sm"
      break
    case ButtonSize.XL:
      style += " px-3.5 py-2.5 text-sm"
      break
  }
  switch (buttonStyle) {
    case ButtonStyle.STANDARD:
      style += " rounded-md"
      break
    case ButtonStyle.ROUNDED:
      style += " rounded-full"
      break
  }
  return className ? `${style} ${className}`: style;
}

export const Button = forwardRef(({
  children, leadingIcon, trailingIcon, className, color=ButtonColor.PRIMARY, disabled = false,  buttonSize = ButtonSize.MD, type="button",
  buttonStyle = ButtonStyle.STANDARD,
  ...props
}: ButtonProps, ref: ForwardedRef<HTMLButtonElement>) => {
  return <button
  ref={ref}
  type={type}
  {...props}
  {...(disabled ? {disabled: true, onClick: undefined, onMouseDown: undefined, onMouseDownCapture: undefined} : {})}
  className={classNames("inline-flex items-center gap-x-1.5 font-semibold shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2", buttonClassStyle(className, color, buttonSize, buttonStyle, disabled))}
>
  {leadingIcon}
  {children}
  {trailingIcon}
</button>
})
