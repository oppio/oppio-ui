
import { ForwardedRef, HTMLProps, ReactElement, forwardRef, useImperativeHandle } from "react";
import { ExclamationCircleIcon } from '@heroicons/react/20/solid'
import { classNames } from "../../server-side"
import { DocumentIcon, PhotoIcon } from "@heroicons/react/24/outline";
import { useDropzone, Accept, DropzoneOptions, DropzoneInputProps } from "react-dropzone-esm";

export enum FileFieldSize {
  SMALL = "small",
  MEDIUM = "medium",
  LARGE = "large",

}

export interface FileFieldProps extends Omit<HTMLProps<HTMLInputElement>, "media" | "size" | "accept" | "value" | "onDrop"> {
  label: string
  hideLabel?: boolean
  error?: string
  media?: ReactElement
  helpText?: string
  trailingIcon?: ReactElement
  leadingIcon?: ReactElement
  inputClassName?: string
  size?: FileFieldSize
  preview?: string
}

function _FileField({
  label, error, media, name, helpText, hideLabel, trailingIcon, preview,
  leadingIcon, className, inputClassName, size = FileFieldSize.MEDIUM, accept, multiple = false,
  onDropAccepted, onDropRejected, onDrop, onError, maxFiles, maxSize, minSize, ...props
}: FileFieldProps & DropzoneOptions) {
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: accept as Accept,
    multiple,
    onDropAccepted,
    onDropRejected,
    onDrop,
    onError,
    maxFiles,
    maxSize,
    minSize
  })

  const id = `id-${label}`
  return (
    <div className={className}>
      <label htmlFor={name} className={hideLabel ? "sr-only" : "block text-sm font-medium leading-6 text-gray-900 dark:text-white text-left"}>
        {label}
      </label>
      <div className={`${(error || trailingIcon || leadingIcon) ? " relative rounded-md shadow-sm" : ""}`}>
        {leadingIcon &&
          <div className={`pointer-events-none absolute inset-y-0${(leadingIcon) ? " left-2 " : ""}flex items-center pl-3`}>
            {leadingIcon}
          </div>
        }
        <div
          {...getRootProps()}
          className={classNames("border-gray-200 border-dashed border-2 rounded-lg flex items-center justify-center hover:border-gray-400 hover:cursor-pointer p-1", size == FileFieldSize.SMALL && "w-8 h-8", size == FileFieldSize.MEDIUM && "w-16 h-16", size == FileFieldSize.LARGE && "w-24 h-24", isDragActive ? "border-primary-600" : "border-gray-300", error ? "border-red-500" : "")}
        >
          <DocumentIcon className="h-6 w-6 m-auto text-gray-400" aria-hidden="true" />
          <input
            {...getInputProps({ type: "file", name, id, ...props })}
            className={classNames(`hidden px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-primary-600 dark:bg-gray-700 dark:text-white disabled:cursor-not-allowed disabled:bg-gray-50 disabled:text-gray-500 disabled:ring-gray-200 sm:text-sm sm:leading-6 ${(trailingIcon || error) ? "pr-10" : ((trailingIcon && error) ? "pr-20" : "")} ${(leadingIcon) ? "pl-10" : ""}`, inputClassName)}
          />
        </div>

        {trailingIcon &&
          <div className={`pointer-events-none absolute inset-y-0 ${(trailingIcon && error) ? "right-4" : "right-0"} flex items-center pr-3`}>
            {trailingIcon}
          </div>
        }
        {error &&
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
            <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
          </div>
        }
      </div>
      {(helpText && !error) &&
        <p className="mt-2 text-sm text-gray-500" id={`${name}-description`}>
          {helpText}
        </p>
      }
      {error &&
        <p className="mt-2 text-sm text-red-600 text-left" id={`${name}-error`}>
          {error}
        </p>
      }
    </div>
  )
}

export const FileField = _FileField
