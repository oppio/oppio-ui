import { classNames } from "../../server-side";

export type SpinnerProps = {
    size?: "sm" | "md" | "lg"
    color?: "light" | "dark"
}

export const Spinner = ({size = "md", color}: SpinnerProps) => {
    return <svg className={classNames("animate-spin ml-2 mr-2", color === "light" ? "text-primary-300" : "text-primary-500",  size === "lg" && "h-16 w-16", size === "md" && "h-8 w-8", size === "sm" && "h-4 w-4")} xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
    <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
    <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
</svg>
}