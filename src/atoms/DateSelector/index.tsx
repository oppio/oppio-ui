import { useEffect, useMemo, useState } from 'react'
import {
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@heroicons/react/20/solid'
import { Transition, TransitionChild } from '@headlessui/react'
import { DateTime } from 'luxon'
import { XMarkIcon } from '@heroicons/react/24/outline'
import {SelectField} from '../SelectField'
import { classNames } from "../../server-side"



export type DateSelectorProps = {
  value?: DateTime
  onChange?: (value: DateTime) => void
  rangeSelector: false
  startYear?: number
  endYear?: number
  numberOfMonths?: number
  className?: string
  initialDate?: DateTime
} | {
  value?: [DateTime | undefined, DateTime | undefined]
  onChange: (value: [DateTime | undefined, DateTime | undefined]) => void
  rangeSelector: true
  startYear?: number
  endYear?: number
  numberOfMonths?: number
  className?: string
  initialDate?: DateTime
}

type DayState = {
  date: DateTime
  isCurrentMonth: boolean
  isSelected: boolean
  isToday: boolean
  isInternalRange: boolean
}



export function DateSelector({ value, initialDate, onChange, startYear = 1900, endYear,  rangeSelector, className }: DateSelectorProps) {
  const now = useMemo(() => DateTime.now(), [])

  const [yearSeletorOpen, setYearSelectorOpen] = useState(false)
  
  const [year, setYear] = useState<number>((value ? (Array.isArray(value) ? value[0]?.year : (value.isValid ? value.year : (initialDate ? initialDate.year : now.year))) : (initialDate ? initialDate.year : now.year)) ?? now.year)
  const [month, setMonth] = useState<number>((value ? (Array.isArray(value) ? value[0]?.month : (value.isValid ? value.month : (initialDate ? initialDate.month : now.month))) : (initialDate ? initialDate.month : now.month)) ?? now.month)

  const days = useMemo<DayState[]>(() => {
    const start = DateTime.fromObject({ year, month, day: 1 }).toLocal().startOf("week")
    const end = DateTime.fromObject({ year, month, day: 1 }).toLocal().endOf("month").endOf("week")
    const dayCount = Math.round(end.diff(start, "days").days)

    return Array(dayCount).fill(0).map((_, idx) => {
      const date = start.plus({ days: idx })
      return {
        date,
        isCurrentMonth: date.month === month,
        isSelected: !!Array.isArray(value) ? !!value[0]?.hasSame(date, "day") || !!value[1]?.hasSame(date, "day") : !!value?.hasSame(date, "day"),
        isToday: date.hasSame(now, "day"),
        isInternalRange: !!(Array.isArray(value) ? (value[0] && value[1] && date >= value[0] && date <= value[1]) : false)
      }
    })
  }, [month, year, value])

  const yearOptions = useMemo(() => Array(now.year - startYear).fill(0).map((u, idx) => ({
    value: (endYear || now.year) - idx,
    label: String((endYear || now.year) - idx),
    search: String((endYear || now.year) - idx)
  })), [endYear, now.year, startYear])

  const monthOptions = Array(12).fill(0).map((_u, idx) => ({
    value: idx + 1,
    label: DateTime.fromObject({ year: now.year, month: idx + 1, day: 1, hour: 1, minute: 1, second: 1, millisecond: 1 }).monthLong || "",
    search: `DateTime.fromObject({year: currentYear, month: idx + 1, day: 1, hour: 1, minute: 1, second: 1, millisecond: 1}).monthLong ${idx + 1}`
  }))

  const monthString = useMemo<string | null>(() => {
    return DateTime.fromObject({ year, month, day: 1 }).monthLong
  }, [month, year])

  const onDayClick = (date: DateTime) => {
    if (rangeSelector === true && onChange) {
      if (Array.isArray(value)) {
        if (value[0] !== undefined && value[1] !== undefined) {
          onChange([date, undefined])
        } else if (value[0]) {
          if (date > value[0]) {
            onChange([value[0], date])
          } else {
            onChange([date, value[0]])
          }
        } else {
          onChange([date, undefined])
        }
      } else {
        onChange([date, undefined])
      }
    } else if (rangeSelector === false) {
      onChange && onChange(date)
    }
  }

  return (
    <div className={className}>
      <div className="">
        <div className="relative mt-4 text-center lg:col-start-8 lg:col-end-13 lg:row-start-1 lg:mt-9 xl:col-start-9">
          <div className="flex items-center text-gray-900">
            <button
              type="button"
              className="-m-1.5 flex flex-none items-center justify-center p-1.5 text-gray-400 hover:text-gray-500"
              onClick={() => {
                if (month >= 2) { 
                  setMonth(month - 1)
                } else {
                  setMonth(12)
                  setYear(year - 1) }
                } 
              }
            >
              <span className="sr-only">Previous month</span>
              <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
            </button>
            <div className="flex-auto text-sm font-semibold">
              <span className='flex items-center justify-center'>
                <span className='mr-2 hover:text-indigo-500 dark:text-white cursor-pointer' onClick={() => setYearSelectorOpen(true)}>
                  {monthString && monthString} {year}
                </span>
              </span>
            </div>
            <button
              type="button"
              className="-m-1.5 flex flex-none items-center justify-center p-1.5 text-gray-400 hover:text-gray-500"
              onClick={() => {
                if (month <= 11) { 
                  setMonth(month + 1 )
                } else {
                  setMonth(1)
                  setYear(year + 1) }
                } 
              }
            >
              <span className="sr-only">Next month</span>
              <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
            </button>
          </div>
          <div className={classNames('p-1 grid w-full gap-2', (!rangeSelector) ? "grid-cols-3" : "grid-cols-4")}>
            <div className={classNames((!rangeSelector) ? "col-span-3" : "col-span-4", )}>
              <div className="mt-6 grid grid-cols-7 text-xs leading-6 text-gray-500">
                <div>M</div>
                <div>T</div>
                <div>W</div>
                <div>T</div>
                <div>F</div>
                <div>S</div>
                <div>S</div>
              </div>
              <div>
                <div className={classNames("isolate mt-2 grid grid-cols-7 gap-px rounded-lg bg-gray-200 text-sm shadow ring-gray-200", yearSeletorOpen ? "" : "ring-1")}>
                  {days.map((day, dayIdx) => {
                    const dateString = day.date.toISO() || undefined
                    return <button
                      key={dateString}
                      type="button"
                      className={classNames(
                        'py-1.5 hover:bg-gray-100 dark:hover:bg-gray-500 focus:z-10',
                        day.isCurrentMonth ? 'bg-white dark:bg-gray-600' : 'bg-gray-50 dark:bg-gray-700',
                        (day.isSelected || day.isToday || day.isInternalRange) && 'font-semibold',
                        (day.isSelected || day.isInternalRange) && 'text-white',
                        !day.isSelected && day.isCurrentMonth && !day.isToday && 'text-gray-900 dark:text-gray-100',
                        !day.isSelected && !day.isCurrentMonth && !day.isToday && 'text-gray-400',
                        day.isToday && !day.isSelected && 'text-indigo-600 dark:text-indigo-400',
                        dayIdx === 0 && 'rounded-tl-lg',
                        dayIdx === 6 && 'rounded-tr-lg',
                        days.length - dayIdx <= 7 && day.date.weekday === 1 && 'rounded-bl-lg',
                        dayIdx === days.length - 1 && 'rounded-br-lg'
                      )}
                      onClick={() => onDayClick(day.date)}
                    >
                      <time
                        dateTime={dateString}
                        className={classNames(
                          'mx-auto flex h-7 w-7 items-center justify-center rounded-full',
                          (day.isSelected || day.isInternalRange) && day.isToday && 'bg-indigo-600',
                          (day.isSelected || day.isInternalRange) && !day.isToday && 'bg-gray-900'
                        )}
                      >
                        {day.date.day}
                      </time>
                    </button>
                  }
                  )}
                </div>

              </div>
            </div>
          </div>
          
          <Transition
            as="div"
            show={yearSeletorOpen}
            enter="transition-transform duration-150"
            enterFrom="translate-y-[-100%]"
            enterTo="translate-y-0"
            leave="transition-transform duration-150"
            leaveFrom="translate-y-0"
            leaveTo="translate-y-[-100%]"
            className="absolute w-full h-full top-0 left-0 bg-white/95 flex flex-col dark:bg-gray-900 rounded-lg px-4 gap-2"
          >
            <TransitionChild>
              <div className='flex pt-4 w-full flex-row-reverse'>
                <XMarkIcon className='w-4 h-4 cursor-pointer dark:text-white' onClick={() => setYearSelectorOpen(false)} />
              </div>
              <div className='flex justify-center gap-2'>
                <SelectField<number>
                  label="Month"
                  value={month}
                  isMulti={false}
                  useFixed={false}
                  creatable={false}
                  options={monthOptions}
                  onChange={(val) => Array.isArray(val) ? setMonth(val[0]) : val && setMonth(val)}
                />
                <SelectField<number>
                  isMulti={false}
                  creatable={false}
                  label="Year"
                  value={year}
                  options={yearOptions}
                  useFixed={false}
                  onChange={(val) => Array.isArray(val) ? setYear(val[0]) : val && setYear(val)}
                />
              </div>
            </TransitionChild>
          </Transition>
        </div>
      </div>
    </div>
  )
}