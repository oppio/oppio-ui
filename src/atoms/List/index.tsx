import React, { HTMLProps, ReactElement } from "react"

export type ListProps = Omit<HTMLProps<HTMLUListElement>, "children"> & {
  children: ReactElement | ReactElement[]
}


export function List({children, ...props}: ListProps) {
  
  const items = Array.isArray(children) ? children : [children]
  
  return <ul role="list" className="divide-y divide-gray-100">
  {items.map((item) => (
    <li key={item.key} className="flex justify-between gap-x-6 py-5">
      {item}
    </li>
  ))}
</ul>
}