"use-client";
import { classNames } from "../../server-side"
import { ReactElement } from "react";
import { OverlayArrow, TooltipTrigger, Tooltip as AriaTooltip, Button } from "react-aria-components";
import type {TooltipProps as AriaTooltipProps} from 'react-aria-components';
import {  ButtonColor } from "../Button";

interface MyTooltipProps extends Omit<AriaTooltipProps, 'children'> {
    children: React.ReactNode;
    color?: ButtonColor
}

export type TooltipProps = {
    tooltipText: string | ReactElement
    color?: ButtonColor
} & MyTooltipProps

export function Tooltip({children, tooltipText, offset = 0, ...props}: TooltipProps) {
    return <TooltipTrigger>
    <Button className="focus-visible:outline-0 text-sm">
    {children}
    </Button>
    <MyTooltip {...props} offset={offset}>{tooltipText}</MyTooltip>
  </TooltipTrigger>
}





function MyTooltip({ children, color = ButtonColor.PRIMARY, placement = "top", ...props }: MyTooltipProps) {
  return (
    <AriaTooltip {...props} placement={placement}>
        <span 
            className={classNames(
                "p-1 px-2 rounded bottom-[100%] -translate-y-full",
                color === ButtonColor.PRIMARY && "bg-primary-500 text-white",
                color === ButtonColor.SOFT && "bg-primary-50 text-primary-700",
                color === ButtonColor.SECONDARY && "bg-gray-50 dark:bg-gray-700 dark:text-white"
            )}
        >
        <OverlayArrow
            
        >
            <svg 
                width={12}
                height={12}
                viewBox="0 0 8 8"
                className={classNames(
                    color === ButtonColor.PRIMARY && "fill-primary-500 text-white",
                    color === ButtonColor.SOFT && "fill-primary-50 text-primary-700",
                    color === ButtonColor.SECONDARY && "fill-gray-50 dark:fill-gray-700 dark:text-white",
                    placement === "bottom" && "rotate-180",
                    placement === "left" && "-rotate-90",
                    placement === "right" && "rotate-90"
                )}
            
            >
            <path d="M0 0 L4 4 L8 0" />
            </svg>
        </OverlayArrow>
        <span className="text-xs text-gray-900 dark:text-white">
            {children}
        </span>
      </span>
    </AriaTooltip>
  );
}
