import { DateTime } from "luxon";

export const monthDayYearTime = (value: DateTime): string => {
    return value.toLocaleString(DateTime.DATETIME_SHORT)
}

export const monthDayYear = (value: DateTime): string => {
    return value.isValid ? value.toLocaleString(DateTime.DATE_SHORT) : ""
}

export const fromMonthDayYear = (value: string): DateTime => {
    return DateTime.fromFormat(value, "MM/d/yyyy")
}

export function classNames(...classes: (string | boolean | undefined)[]) {
    return classes.filter(Boolean).join(' ')
}