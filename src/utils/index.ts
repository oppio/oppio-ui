import { DateTime } from "luxon";
import { Breakpoint, mobileBreakpoints } from "../hooks";
export {
  monthDayYearTime,
  monthDayYear,
  fromMonthDayYear,
  classNames
} from "../server-side"



export const isMobile = (breakpoint: Breakpoint) => {
    return mobileBreakpoints.includes(breakpoint)
  }