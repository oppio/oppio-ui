const fs = require('fs');
const path = require('path');

let idx = 0
function exportFiles(dir, ftype) {
    currentIdx = idx.toString()
    idx += 1
    const children = fs.readdirSync(dir, {})
    const childRes = children.map((child) => {
        const filePath = path.join(dir, child)
        const stats = fs.statSync(filePath)
        if (stats.isDirectory()) {
            return exportFiles(filePath, ftype)
            
        } else if (stats.isFile() && child.startsWith("index.ts") || child.startsWith("utils.ts" || child.startsWith("hooks.tsx"))) {
            return [filePath]
        }
        return []
    })
    return childRes.filter((x) => x != undefined).reduce((acc, val) => acc.concat(val), [])
    
}

module.exports = {exportFiles}