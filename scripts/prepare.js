const packageJSON = require('../package.json');
const fs = require('fs');

packageJSON["main"] = "index.mjs";
packageJSON["module"] = "index.mjs";
packageJSON["types"] = "index.d.ts";

fs.writeFileSync('./package.json', JSON.stringify(packageJSON, null, 2));

