const commonjs = require("@rollup/plugin-commonjs");
const resolve= require("@rollup/plugin-node-resolve");
const typescript = require("@rollup/plugin-typescript");

const packageJson = require("./package.json");
const external = require('rollup-plugin-peer-deps-external');
const postcss = require('rollup-plugin-postcss');

module.exports = {
    input: "src/atoms/Alert/index.tsx",
    output: [
        {
            dir: "./dist",
            format: "cjs",
            exports: "auto",
            sourcemap: true,
            inlineDynamicImports: true
        },
        {
            dir: "./dist",
            format: "esm",
            exports: "auto",
            sourcemap: true,
            inlineDynamicImports: true
        }
    ],
    plugins: [
        external(),
        resolve(),
        commonjs(),
        typescript({ tsconfig: './tsconfig.json', rootDir: "./src", }),
        postcss(),
    ]
};