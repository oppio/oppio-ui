const commonjs = require("@rollup/plugin-commonjs");
const resolve= require("@rollup/plugin-node-resolve");
const typescript = require("@rollup/plugin-typescript");
const buble = require('@rollup/plugin-buble');
const external = require('rollup-plugin-peer-deps-external');
const postcss = require('rollup-plugin-postcss');
var fs = require('fs');
var path = require('path');
const { rollup } = require("rollup");

const config = (path) => ({
    input: path,
    output: [
        {
            format: "esm",
            exports: "auto",
            sourcemap: true,
            entryFileNames: '[name].mjs',
            inlineDynamicImports: false,
            dir: "./dist"
        }
    ],
    plugins: [
        external(),
        resolve(),
        typescript({ tsconfig: './tsconfig.json', rootDir: "./src", }),
        postcss(),
        commonjs(),
    ]
})

const buildFileList = (dir, done) => {
    let results = [];
    fs.readdir(dir, function(err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach((file) => {
            file = path.resolve(dir, file);
            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    buildFileList(file, function(err, res) {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    if (file.endsWith(".ts") || file.endsWith(".tsx")) {
                        results.push(file)
                    }
                if (!--pending) done(null, results);
                }
            });
        });
    });
}
buildFileList("./src", async (error, res) => {
    let input = Object.fromEntries(res.map((filePath) => {
        let relativePath = filePath.split("src/")[1]
        relativePath = relativePath.split(".")[0]
        return [relativePath, filePath]
    }))
    const configData = config("./")
    const r = await rollup({
        input: input,
        plugins: configData.plugins
    })
    configData.output.forEach(async (options) => {
        r.write({...options})
    })
})

